$(document).on('click','.show-side-btn',function(){
    $('aside.side-nav').toggleClass('show-side-nav');
    // console.log($('aside.side-nav').attr("class"))
    if($('aside.side-nav').hasClass("show-side-nav")){
        $(this).addClass("fa-remove").removeClass("fa-bars");
    }else{
        $(this).addClass("fa-bars").removeClass("fa-remove");
    }
    $('section.content-toggle-margin, div#contents').toggleClass('margin');
})

$(document).on('click','.closeBtn',function(){
    if($( window ).width() <= 767){
        $('aside.side-nav').toggleClass('show-side-nav');
        if($('aside.side-nav').hasClass("show-side-nav")){ $(".show-side-btn").addClass("fa-bars").removeClass("fa-remove");
            $('#show-side-navigation1').removeClass('show-side-nav')
        }else{
            $(".show-side-btn").addClass("fa-remove").removeClass("fa-bars");
        }
        $('section.content-toggle-margin, div#contents').toggleClass('margin');
    }
})