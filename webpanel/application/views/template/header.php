<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Aptech Montana Preschool</title>
	<meta name="description" content="Web Panel Dashboard">	
	<!-- end: Meta -->	
	<!-- start: Mobile Specific -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->	
	<!-- start: CSS -->	
	<link id="base-style-responsive" href="<?PHP echo base_url();?>css/main.css" rel="stylesheet">			
	<link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/jquery.dataTables.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/jquery.noty.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/noty_theme_default.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/adminLTE.min.css" rel="stylesheet">
    <link href="<?PHP echo base_url();?>css/ionicons.min.css" rel="stylesheet">
   
    
    <link rel="manifest" href="<?PHP echo base_url();?>css/manifest.json">
    <link href="<?PHP echo base_url();?>css/select2.css" type="text/css" rel="stylesheet" />
	<!-- end: CSS -->	
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="<?PHP echo base_url();?>css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="<?PHP echo base_url();?>css/ie9.css" rel="stylesheet">
	<![endif]-->
	<!-- start: JavaScript -->	
	<script src="<?PHP echo base_url();?>js/jquery-2.1.4.min.js"></script>
	<script src="<?PHP echo base_url();?>js/plugins/jquery-ui.custom.min.js"></script>
	<script src="<?PHP echo base_url();?>js/essential-plugins.js"></script>
	<script src="<?PHP echo base_url();?>js/bootstrap.min.v3.3.6.js"></script>
	<script src="<?PHP echo base_url();?>js/plugins/pace.min.js"></script>	
	<script src="<?PHP echo base_url();?>js/jquery.form.js"></script>
	<script src="<?PHP echo base_url();?>js/jquery.validate.js"></script>
	<script src="<?PHP echo base_url();?>js/additional-methods.js"></script>
	<script src="<?PHP echo base_url();?>js/plugins/bootstrap-datepicker.min.js"></script>
	<!--<script src="<?PHP echo base_url();?>js/bootstrap-clockpicker.min.js"></script>-->
	
	<!-- Datatable plugin-->
	<script src='<?PHP echo base_url();?>js/jquery.dataTables.min.js'></script>
	<script src='<?PHP echo base_url();?>js/datatable.js'></script>
	<script src="<?PHP echo base_url();?>js/jquery.noty.js"></script>
	<!-- Datatable plugin-->
	
	<!-- CK Editor plugins -->
	<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>	  
	<!-- CK Editor plugins -->
	
	<script type="text/javascript" src="<?PHP echo base_url();?>js/moment.js"></script>
	<script type="text/javascript" src="<?PHP echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?PHP echo base_url();?>css/bootstrap-datetimepicker.min.css"/>

	<script type="text/javascript" src="<?PHP echo base_url();?>js/bootstrap-timepicker.js"></script>
	<!--timepicker css-->
	<link type="text/css" rel="stylesheet" href="<?PHP echo base_url();?>css/bootstrap-timepicker.css"/>
    
	<!-- Start: Select2-->
		
	<script type="text/javascript" src="<?PHP echo base_url();?>js/select2.min.js"></script>
    
	<!-- end: Select2-->
	<!-- end: JavaScript -->
    
	<link href="<?PHP echo base_url();?>css/bootstrap-glyphicons.css" rel="stylesheet">
    
	<!-- selectpicker -->
	<link rel="stylesheet" href="https://cdn.rawgit.com/djibe/bootstrap-select/v1.13.0-dev/dist/css/bootstrap-select-daemonite.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
	<?php //echo "<pre>";print_r($_SESSION["webadmin"][0]);exit;?>
	<script>
		function setTabIndex(){
			var tabindex = 1;
			$('input,select,textarea,.icon-plus,.icon-minus,button,a').each(function() {
				if (this.type != "hidden") {
					var $input = $(this);
					$input.attr("tabindex", tabindex);
					tabindex++;
				}
			});
		}
		
		$(function(){
			setTabIndex();
			$(".select2").each(function(){
				$(this).select2({
					placeholder: "Select",
					allowClear: true
				});
				$("#s2id_"+$(this).attr("id")).removeClass("searchInput");
			});
			$(".dataTables_filter input.hasDatepicker").change( function () {				
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $(".searchInput").index(this) ) );
			});			
			window.scrollTo(0,0);
		});
		
		function displayMsg(type,msg)
		{
			
			$.noty({
				text:msg,
				layout:"topRight",
				type:type
			});
		}
	</script>	
    <!-- Old CSS & JS Files-->
</head>

<body class="sidebar-mini fixed">	     
<?php //echo "<pre>";print_r($_SESSION);exit;?>
<div class="wrapper"><!-- Wrapper Start -->	
<!-- Navbar-->
     <header class="main-header hidden-print">
		<a class="logo" href="<?php echo base_url();?>home">
      		<!--<h3 style="color:black;">Montana - CRM</h3>-->
			<img src="<?PHP echo base_url();?>images/aptech-montana-logo.jpg" alt="Aptech Montana" style="width:110px;"/>
		</a>
		<nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
          <!-- Navbar Right Menu-->
			<div class="navbar-custom-menu">
				<ul class="top-nav">
				<!-- User Menu-->
					<li class="dropdown">
						<a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-user fa-lg"></i> <?php echo $_SESSION["webadmin"][0]->first_name." ".$_SESSION["webadmin"][0]->last_name; ?>
						</a>
						<ul class="dropdown-menu settings-menu">
							<!-- <li><a href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>-->
							<?php  //if($_SESSION["webadmin"][0]->role_id == 1 ){?>
								<li><a href="<?PHP echo base_url();?>changepassword/addEdit"><i class="fa fa-user fa-lg"></i> Change Password</a></li>
							<?php //}?>	
							<li><a href="<?PHP echo base_url();?>home/logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
        </nav>
     </header>
     <!-- Side-Nav-->     
	 <aside class="main-sidebar hidden-print">
		<section class="sidebar">          
		<!-- Sidebar Menu-->
          <ul class="sidebar-menu">
			<li class="active"><a href="<?php echo base_url();?>home"><i class="fa fa-dashboard"></i><span style="font-size: 100%;">Dashboard</span></a></li>
			<?php  if($_SESSION["webadmin"][0]->user_type == 1 ){?>
				<?php 
					if ($this->privilegeduser->hasPrivilege("InquiryList") || $this->privilegeduser->hasPrivilege("InquiryExport")) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>inquiries"><i class="fa fa-question"></i><span style="font-size: 100%;">Enquiry</span></a></li>
				<?php }?>


				<?php 
					if ($this->privilegeduser->hasPrivilege("AdmissionList") || $this->privilegeduser->hasPrivilege("AdmissionList")) {
				?>
				   <li class="treeview"><a href="<?php echo base_url();?>admission"><i class="fa fa-graduation-cap"></i><span style="font-size: 100%;">Admission </span></a></li>
				<?php } ?>
					<li class="treeview"><a href="<?php echo base_url();?>assigngrouptostudent"><i class="fa fa-users"></i><span style="font-size: 100%;">Assign To Groups</span></a></li>
					<li class="treeview"><a href="#"><i class="fa fa-money"></i><span style="font-size: 100%;"> Fees</span><i class="fa fa-angle-right"></i></a>
				<ul class="treeview-menu">
					<?php 
						if ($this->privilegeduser->hasPrivilege("FeesLevelList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>feeslevelmaster"><i class="fa fa-money"></i><span style="font-size: 100%;">Create Fee Levels</span></a></li> 
					<?php }?>	
					
					<?php 
						if ($this->privilegeduser->hasPrivilege("FeesComponentList") ) {
					?>
	           		<li class="treeview"><a href="<?php echo base_url();?>feescomponentmaster"><i class="fa fa-money"></i><span style="font-size: 100%;">Fee Components</span></a></li>
					<?php }?>
					
					<?php 
						if ($this->privilegeduser->hasPrivilege("FeesMasterList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>feesmaster"><i class="fa fa-money"></i><span style="font-size: 100%;">Fee Config</span></a></li>  
					<?php }?>	
					<?php 
					if ($this->privilegeduser->hasPrivilege("AssignFeesList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>assignfees"><i class="fa fa-money"></i><span style="font-size: 100%;">Assign Fees</span></a></li>   
						<li class="treeview"><a href="<?php echo base_url();?>assignfeestobatch"><i class="fa fa-list"></i><span style="font-size: 100%;">Assign Fees to Batches</span></i></a></li> 
					<?php }?>		
						<li class="treeview"><a href="<?php echo base_url();?>childactivityfeesmonth"><i class="fa fa-money"></i><span style="font-size: 100%;">Group Payment Frequency Master</span></a></li>    
						<li class="treeview"><a href="<?php echo base_url();?>feesdiscountmaster"><i class="fa fa-money"></i><span style="font-size: 100%;">Create Group Payment Frequency</span></i></a></li> 
						<li class="treeview"><a href="<?php echo base_url();?>gstmaster"><i class="fa fa-money"></i><span style="font-size: 100%;">GST Master</span></a></li>            
					</ul>
				</li>
				<?php 
					if ($this->privilegeduser->hasPrivilege("RemainingFeesPayment") || $this->privilegeduser->hasPrivilege("RemainingFeesPayment")) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>remainingfees"><i class="fa fa-money"></i><span style="font-size: 100%;">Fees Payment </span></a></li>
				<?php } ?>
	   			<li class="treeview"><a href="#"><i class="fa fa-calendar"></i><span style="font-size: 100%;"> Timetable</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					
					<?php 
						if ($this->privilegeduser->hasPrivilege("ThemeAddEdit") || $this->privilegeduser->hasPrivilege("ThemeList")) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>themesmaster"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Create Theme </span></a></li>
					<?php }?>	
					
						
					<?php 
						if ($this->privilegeduser->hasPrivilege("TimeTableAddEdit") || $this->privilegeduser->hasPrivilege("TimeTableList")) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>timetablemaster"><i class="fa fa-calendar"></i><span style="font-size: 100%;">	Create Timetable </span></a></li>
					<?php }?>
					<?php 
						if ($this->privilegeduser->hasPrivilege("CenterTimetableList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>centertimetables"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Time Tables </span><i class="fa fa-angle-right"></i></a></li>
					<?php }?>
					
					<?php 
						if ($this->privilegeduser->hasPrivilege("AssignTimeTableAddEdit") || $this->privilegeduser->hasPrivilege("AssignTimeTableList")) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>assigntimetable"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Assign Timetable </span></a></li>
					<?php }?>			
						              
					</ul>
				</li>
	   			<li class="treeview"><a href="#"><i class="fa fa-folder"></i><span style="font-size: 100%;"> Documents</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					
					
					<?php 
						if ($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit") || $this->privilegeduser->hasPrivilege("DocumentFolderList")) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>documentfoldermaster"><i class="fa fa-folder"></i><span style="font-size: 100%;">Add/Map Folder</span></a></li>
					<?php }?>	
					
						
					
					<?php 
						if ($this->privilegeduser->hasPrivilege("CenterDocumentFolderList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>documentfolders"><i class="fa fa-folder"></i><span style="font-size: 100%;">View Documents </span></a></li>
					<?php }?>			
						              
					</ul>
				</li>
	   			<li class="treeview"><a href="#"><i class="fa fa-camera"></i><span style="font-size: 100%;"> Camera</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					<?php 
						if ($this->privilegeduser->hasPrivilege("CenterCameraSettingsList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>camerasettings"><i class="fa fa-camera"></i><span style="font-size: 100%;">Centre Camera Settings</span></a></li>
					<?php }?>	
					<?php 
						if ($this->privilegeduser->hasPrivilege("CCTVCumulativeSettingsList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>cumulativesettings"><i class="fa fa-camera"></i><span style="font-size: 100%;">Cumulative Time Settings</span></a></li>
					<?php }?>	
					<?php 
						if ($this->privilegeduser->hasPrivilege("CameraList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>camera"><i class="fa fa-camera"></i><span style="font-size: 100%;">Add Camera</span></i></a></li> 
					<?php }?>		
					<?php 
						if ($this->privilegeduser->hasPrivilege("AssignCameraList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>assigncamera"><i class="fa fa-camera"></i><span style="font-size: 100%;">Assign Camera</span></a></li> 
					<?php }?>             
					</ul>
				</li>
	   			<li class="treeview"><a href="#"><i class="fa fa-newspaper-o"></i><span style="font-size: 100%;"> Newsletters</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					<?php 
						if ($this->privilegeduser->hasPrivilege("NewsLettersList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>newsletters"><i class="fa fa-newspaper-o"></i><span style="font-size: 100%;">Create Newsletter</span></a></li>
					<?php }?>	
					<?php 
						if ($this->privilegeduser->hasPrivilege("AssignNewsletterList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>assignnewsletter"><i class="fa fa-newspaper-o"></i><span style="font-size: 100%;">Assign Newsletters</span></a></li>
					<?php }?>	            
					</ul>
				</li>
	   			<li class="treeview"><a href="#"><i class="fa fa-users"></i><span style="font-size: 100%;"> Communication</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					<?php 
						if ($this->privilegeduser->hasPrivilege("CommunicationCategoriesList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>communicationcategories"><i class="fa fa-users"></i><span style="font-size: 100%;">Communication Category</span></a></li>
					<?php }?>	
						<li class="treeview"><a href="<?php echo base_url();?>communication"><i class="fa fa-users"></i><span style="font-size: 100%;">Add Communication</span></a></li>	            
					</ul>
				</li>
				<li class="treeview"><a href="#"><i class="fa fa-users"></i><span style="font-size: 100%;"> User Management</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					<?php 
						if ($this->privilegeduser->hasPrivilege("PermissionAddEdit") || $this->privilegeduser->hasPrivilege("PermissionList")) {
					?>
						<li><a href="<?php echo base_url();?>permission"><i class="fa fa-lock"></i> Permissions</a></li>					
					<?php }?>
					<?php 
						if ($this->privilegeduser->hasPrivilege("RoleAddEdit") || $this->privilegeduser->hasPrivilege("RoleList")) {
					?>
						<li><a href="<?php echo base_url();?>roles"><i class="fa fa-user"></i> Roles</a></li>					
					<?php }?>
					<?php 
						if ($this->privilegeduser->hasPrivilege("UserAddEdit") || $this->privilegeduser->hasPrivilege("UserList")) {
					?>
						<li><a href="<?php echo base_url();?>users"><i class="fa fa-user"></i> Admin Users</a></li>
					<?php }?>	
					
					<?php 
						if ($this->privilegeduser->hasPrivilege("CenterUserAddEdit") || $this->privilegeduser->hasPrivilege("CenterUserList")) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>centerusers"><i class="fa fa-user"></i><span style="font-size: 100%;">Center Users </span></a></li>
					<?php }?>	 

					</ul>
				</li>
				<li class="treeview"><a href="#"><i class="fa fa-laptop"></i><span style="font-size: 100%;"> Masters</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
						
				<?php 
					if ($this->privilegeduser->hasPrivilege("AcademicYearList") ) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>academicyear"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Academic Year Master</span></a></li>
				<?php }?>	
					<?php 
					if ($this->privilegeduser->hasPrivilege("CategoryAddEdit") || $this->privilegeduser->hasPrivilege("CategoryList")) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>categories"><i class="fa fa-laptop"></i><span style="font-size: 100%;">Category Master</span></a></li>
				<?php }?>	
				<?php 
					if ($this->privilegeduser->hasPrivilege("CourseAddEdit") || $this->privilegeduser->hasPrivilege("CourseList")) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>coursesmaster"><i class="fa fa-laptop"></i><span style="font-size: 100%;">Course Master</span></a></li>
					<?php }?> 
						  <?php 
						if ($this->privilegeduser->hasPrivilege("ZoneAddEdit") || $this->privilegeduser->hasPrivilege("ZoneList")) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>zonemaster"><i class="fa fa-building"></i><span style="font-size: 100%;">Zone Master </span></a></li>
					<?php }?>  
					<?php 
						if ($this->privilegeduser->hasPrivilege("CenterAddEdit") || $this->privilegeduser->hasPrivilege("CenterList")) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>centermaster"><i class="fa fa-building"></i><span style="font-size: 100%;">Center Master </span></a></li>
					<?php }?> 
					<?php 
						if ($this->privilegeduser->hasPrivilege("BatchList") ) {
					?>
						<li class="treeview"><a href="<?php echo base_url();?>batchnamemaster"><i class="fa fa-users"></i><span style="font-size: 100%;">Batch Name Master</span></a></li>
						<li class="treeview"><a href="<?php echo base_url();?>batchmaster"><i class="fa fa-users"></i><span style="font-size: 100%;">Batch Master</span></a></li>
					<?php }?> 
					<?php 
						if ($this->privilegeduser->hasPrivilege("GroupList") ) {
					?>
	           		<li class="treeview"><a href="<?php echo base_url();?>groupmaster"><i class="fa fa-users"></i><span style="font-size: 100%;">Group Master</span></a></li>
					<?php } ?>       
	           		
					</ul>
				</li>
				<?php 
					if ($this->privilegeduser->hasPrivilege("FeedbackList") || $this->privilegeduser->hasPrivilege("FeedbackExport")) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>feedbacks"><i class="fa fa-comment"></i><span style="font-size: 100%;">Feedbacks</span></a></li>
				<?php }?>
	   			<li class="treeview"><a href="#"><i class="fa fa-file-image-o"></i><span style="font-size: 100%;"> Album</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					
           		
					<li class="treeview"><a href="<?php echo base_url();?>album"><i class="fa fa-file-image-o"></i><span style="font-size: 100%;">Album </span></a></li>

					<li class="treeview"><a href="<?php echo base_url();?>assignalbum"><i class="fa fa-file-image-o"></i><span style="font-size: 100%;">Assign Album </span></a></li>		            
					</ul>
				</li>
	   			<li class="treeview"><a href="#"><i class="fa fa fa-clock-o"></i><span style="font-size: 100%;"> School Diary</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					
           		
									
				<?php 
					if ($this->privilegeduser->hasPrivilege("SubroutinesList") ) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>subroutines"><i class="fa fa-clock-o"></i><span style="font-size: 100%;">Subroutines</span></a></li>
				<?php }?>	
				<?php 
					if ($this->privilegeduser->hasPrivilege("RoutineActionList") ) {
				?>
					<li class="treeview"><a href="<?php echo base_url();?>routinesactions"><i class="fa fa-clock-o"></i><span style="font-size: 100%;">Routine Actions</span></a></li>
				<?php }?>		            
					</ul>
				</li>

						
				<?php 
					if ($this->privilegeduser->hasPrivilege("FolderDocumentAddEdit") || $this->privilegeduser->hasPrivilege("FolderDocumentList")) {
				?>
					<!--<li class="treeview"><a href="<?php echo base_url();?>folderdocumentsmaster"><i class="fa fa-laptop"></i><span style="font-size: 100%;">Folder Documents </span><i class="fa fa-angle-right"></i></a></li>-->
				<?php }?>					
			
			<?php }else{?>
				<li class="treeview"><a href="<?php echo base_url();?>centertimetables"><i class="fa fa-calendar"></i><span style="font-size: 100%;">Time Tables </span><i class="fa fa-angle-right"></i></a></li>
				
				<li class="treeview"><a href="<?php echo base_url();?>documentfolders"><i class="fa fa-folder"></i><span style="font-size: 100%;">View Documents </span><i class="fa fa-angle-right"></i></a></li>
				
			<?php }?>
			<li class="treeview"><a href="<?php echo base_url();?>appointment"><i class="fa fa-folder"></i><span style="font-size: 100%;">Appointment</span><i class="fa fa-angle-right"></i></a></li>
			
          </ul>
		</section>
     </aside>	
 		
<noscript>
	<div class="alert alert-block span10">
		<h4 class="alert-heading">Warning!</h4>
		<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
	</div>
</noscript>
