<!-- <?php 
error_reporting(0);
if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	//echo $_GET['text'];exit;
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
?> -->

<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Fee Payment Process</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>feepayment/add">Fee Payment Process</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
			<div class="col-sm-8 col-md-12">
				<form class="" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Fees Selection Type</span></label>
							<div class="controls">
								<input type="radio" class="form-control selection_type" name="selection_type" value="Class" <?php echo (!empty($details[0]->selection_type) &&  ($details[0]->selection_type== 'Class')) ? "checked" : ""?> checked="checked">Class
							</div>
						</div>

						<div class="col-md-6 control-group form-group">
							<label class="control-label">Select Student*</label>
							<div class="controls">
								<select id="student_id" name="student_id" class="form-control select2"  onchange="getStudentPaymentDetails(this.value);" >
									<option value="">Select Student</option>
									<?php 
										if(isset($student_details) && !empty($student_details)){
											foreach($student_details as $cdrow){
									?>
										<option value="<?php echo $cdrow['student_id'];?>"><?php echo $cdrow['enrollment_no'] .' ( '.$cdrow['student_first_name'].' '.$cdrow['student_last_name'].' )';?></option>
									<?php }}?>
								</select>
							</div>
						</div>
					</div>

					<div class="row installment">
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Fees Type</span></label>
							<div class="controls">
								<input type="radio" class="form-control is_instalment" name="is_instalment" value="No" <?php echo (!empty($details[0]->is_instalment) &&  ($details[0]->is_instalment== 'No')) ? "checked" : ""?> disabled="disabled">Lumsum
								<input type="radio" class="form-control is_instalment" name="is_instalment" value="Yes" <?php echo (!empty($details[0]->is_instalment) &&  ($details[0]->is_instalment== 'Yes')) ? "checked" : ""?>  checked="checked">Installment
							</div>
						</div>
						<div class="col-md-6 control-group form-group lumsum">
							<label class="control-label"><span>Total Of Fees</span></label>
							<div class="controls">
								<input type="text" class="form-control" name="fees_amount_collected" value="<?php if(!empty($details[0]->fees_amount_collected)){echo $details[0]->fees_amount_collected;}else{ echo 0;}?>" readonly="readonly" id="fees_amount_collected">
							</div>
						</div>
					</div>
					
					<div class="row installment">
						<div class="col-md-6 control-group form-group lumsum">
							<label class="control-label"><span>Total Of Fees Paid</span></label>
							<div class="controls">
								<input type="text" class="form-control" name="fees_remaining_amount" value="<?php if(!empty($details[0]->fees_remaining_amount)){echo $details[0]->fees_remaining_amount;}?>" readonly="readonly" id="fees_remaining_amount">
							</div>
						</div>
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Total Of Pending Fees</span></label>
							<div class="controls">
								<input type="text" class="form-control" name="max_installment" value="" readonly="readonly" id="max_installment">
							</div>
						</div>
					</div>
					
					<div class="table-responsive installment">
						<table class="table table-striped" id="fees_table">
							<thead>
								<tr>
									<td>Payment Due Date</td>
									<td>Insatllment Amount</td>
									<td>Paying Amount</td>
									<td>Remaining Amount</td>
								</tr>
							</thead>
							<tbody class="f_table">
							</tbody>
						</table>
					</div>

					<div class="row">
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Fees Remark*</span></label>
							<div class="controls">
								<textarea class="form-control" name="fees_remark" value="" placeholder="Fees Remark" id="fees_remark"><?php if(!empty($details[0]->fees_remark)){echo $details[0]->fees_remark;}?></textarea>
							</div>
						</div>
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Fees Accepted By</span></label>
							<div class="controls">
								<input type="text" class="form-control" name="fees_approved_accepted_by_name" value="<?php echo $login_name;?>" readonly="readonly" id="total_amount">
							</div>
						</div>
					</div>

					<h3>Fill Payment Details</h3>

					<div class="row">
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Payment Mode</span></label>
							<div class="controls">
								<input type="radio" class="form-control payment_mode" name="payment_mode" value="Cheque" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cheque')) ? "checked" : ""?>>Cheque
								<input type="radio" class="form-control payment_mode" name="payment_mode" value="Cash" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cash')) ? "checked" : ""?> checked="checked" >Cash
								<input type="radio" class="form-control payment_mode" name="payment_mode" value="Netbanking" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Netbanking')) ? "checked" : ""?> >Netbanking
							</div>
						</div>
						<div class="col-md-6 control-group form-group both">
							<label class="control-label"><span>Bank Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control" name="bank_name" value="<?php if(!empty($details[0]->bank_name)){echo $details[0]->bank_name;}?>" id="bank_name">
							</div>
						</div>
					</div>

					<div class="row cheque">
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Cheque No*</span></label>
							<div class="controls">
								<input type="number" class="form-control" name="cheque_no" value="<?php if(!empty($details[0]->cheque_no)){echo $details[0]->cheque_no;}?>" id="cheque_no" min="0">
							</div>
						</div>
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Cheque Date*</span></label>
							<div class="controls">
								<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="cheque_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
							</div>
						</div>
					</div>

					<div class="row transaction">
						<div class="col-md-6 control-group form-group cheque transaction">
							<label class="control-label"><span>Transaction No*</span></label>
							<div class="controls">
								<input type="number" class="form-control" name="transaction_id" value="<?php if(!empty($details[0]->transaction_id)){echo $details[0]->transaction_id;}?>" id="transaction_id" min="0">
							</div>
						</div>
						<div class="col-md-6 control-group form-group">
							<label class="control-label"><span>Transaction Date*</span></label>
							<div class="controls">
								<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="transaction_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
							</div>
						</div>
					</div>

					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
					</div>
				</form>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY',

	});
	installmentprocess()
});
function getStudentPaymentDetails(){
	console.log(22222)
}


function installmentprocess(){
	$('.cheque').hide();
	$('.transaction').hide();
	$('.both').hide();
	$('.payment_mode').change(function(){
		var payment_mode = $(this).val();
		if(payment_mode == 'Cheque'){
			$('.cheque').show();
			$('.transaction').hide();
			$('.both').show();
		}
		else if(payment_mode == 'Netbanking'){
			$('.cheque').hide();
			$('.transaction').show();
			$('.both').show();
		}
		else{
			$('.cheque').hide();
			$('.transaction').hide();
			$('.both').hide();
		}
	})
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY',

	});
	$('.installment').hide();
	$('.is_instalment').change(function(){
		var is_instalment = $(this).val();
		if(is_instalment == 'Yes'){
			$('.installment').show();
			$('.lumsum').hide();
		}
		else{
			$('.installment').hide();
			$('.lumsum').show();
		}
	})
	$('#discount_amount').keyup(function(){
		var discount_amount = $(this).val();
		var fees_total_amount = $('#fees_total_amount').val();
		var amount_after_discount = Number(fees_total_amount) - Number(discount_amount);
		$('#total_amount').val(amount_after_discount)
		let installmentNo = $('.no_of_installments').val()
		let installment_amount = (amount_after_discount/installmentNo).toFixed(2)
		$('.instalment_amount').val(installment_amount)
		$('.instalment_collected_amount').val(0)
		$('.instalment_remaining_amount').val(installment_amount)
	})

	$('.no_of_installments').keyup(function(){
		var installmentNo = $(this).val()
		var max_installment = $('#max_installment').val()
		if(installmentNo == ''){
			$(".f_table").children().remove();
		}
		else{
			if(installmentNo > max_installment){
				$('.installment_error').text('Installment No should not greater than Max Installment') 
			}
			else{
				$('.installment_error').text('') 
				for(let i=0;i<installmentNo;i++){
					let installment_amount = ($('#fees_total_amount').val()/installmentNo).toFixed(2)
					$(".f_table").append("<tr>");
					$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control monthPicker" placeholder="Select Date" id="installmentdate_'+i+'" name="instalment_due_date[]"></div></td>');
					$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_amount"  name="instalment_amount" readonly="readonly"></div></td>');
					$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_collected_amount" id="instalmentcollectedamount_'+i+'" name="instalment_collected_amount[]"></div></td>');
					$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_remaining_amount"  id="instalmentremainingamount_'+i+'"name="instalment_remaining_amount[]" readonly="readonly"></div></td>');
					$(".f_table").append("</tr>");
					$(".f_table").append("</br>");
					$('.instalment_amount').val(installment_amount)
					$('.instalment_collected_amount').val(0)
					$('.instalment_remaining_amount').val(installment_amount)
				}
				$(".monthPicker").datepicker({ 
					format: 'mm/yyyy',
					startView: "months", 
		    		minViewMode: "months",
				});
				$('.instalment_collected_amount').keyup(function(){
					let collected_amount = $(this).val()
					let installment_amount = $('.instalment_amount').val()

					let remaining_amount_input = $(this).parent().parent().next().find('.instalment_remaining_amount')
					if(parseInt(collected_amount) > parseInt(installment_amount)){
						remaining_amount_input.val(installment_amount);
					}
					else{				
						let remaining_amount = ($('.instalment_amount').val() - collected_amount).toFixed(2)
						remaining_amount_input.val(remaining_amount);
					}
				})
			}
		}
	})
}



function getFees(fees_level_id,fees_id = null)
{
	//alert("Val: "+val);return false;
	if(fees_level_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getFees",
			data:{fees_level_id:fees_level_id, fees_id:fees_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#fees_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#fees_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#fees_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getFessDetails(fees_id){
	if(fees_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getFessDetails",
			data:{ fees_id:fees_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				console.log(res)
				if(res['status']=="success")
				{
					$('#fees_total_amount').val(res['result'][0]['amount'])
					$('#fees_remaining_amount').val(res['result'][0]['amount'])
					$('#total_amount').val(res['result'][0]['amount'])
					$('#max_installment').val(res['result'][0]['installment_no'])
				}
				else
				{	
					$('#fees_total_amount').val(0)
					$('#fees_remaining_amount').val(0)
					$('#total_amount').val(0)
					$('#max_installment').val(0)
				}
			}
		});
	}
}

var vRules = {
	student_id : {required:true},
	fees_remark : {required:true},
	
};
var vMessages = {
	student_id : {required:"Please select student."},
	fees_remark : {required:"Please enter fees remark."},
};

$("#feesInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitfeesInfoForm";
		$("#feesInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						let admission_fees_id = res['payfeesdataresult']
						$.ajax({
							url:"<?php echo base_url();?>admission/getPayemtDetails",
							data:{admission_fees_id:admission_fees_id},
							dataType: 'json',
							method:'post',
							success: function(data)
							{
								if(data['status_code'] == 200){
									window.open(response['body'], '_blank');
									window.location = "<?php echo base_url();?>admission";
								}
							}
						});
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "Add - Fee Payment Process";

 
</script>					
