<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Assign Fees</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>assignfees">Assign Fees</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<input type="hidden" id="assign_fees_id" name="assign_fees_id" value="<?php if(!empty($details[0]->assign_fees_id)){echo $details[0]->assign_fees_id;}?>" />
					
					<div class="control-group form-group">
						<label class="control-label">Academic Year*</label>
						<div class="controls">
							<select id="academic_year_id" name="academic_year_id" class="form-control">
								<option value="">Select Academic Year</option>
								<?php 
									if(isset($academic_year) && !empty($academic_year)){
										foreach($academic_year as $cdrow){
											$sel = ($cdrow->academic_year_master_id == $details[0]->academic_year_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->academic_year_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->academic_year_master_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label"><span>Select Class / Group*</span></label>
						<div class="controls">
							<label for="" style="margin-right: 60px;margin-top: 20px;">
								<input type="radio" class="fees_selection_type classname" name="fees_selection_type" value="Class" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Class')) ? "checked" : ""?> checked="checked">Class
							</label>
							<label for="" style="margin-right: 60px;margin-top: 20px;">
								<input type="radio" class="fees_selection_type groupname" name="fees_selection_type" value="Group" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Group')) ? "checked" : ""?> >Group
							</label>
						</div>
					</div>

					<div class="control-group form-group class_div">
						<label class="control-label" for="category_id">Category*</label>
						<div class="controls">
							<select id="category_id" name="category_id" class="form-control"  >
								<!-- <option value="">Select Category</option> -->
								<?php 
								if(isset($categories) && !empty($categories)){
									foreach($categories as $cdrow){
										if(strtolower($cdrow->categoy_name) == "preschool"){
										// $sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
										$sel = "selected";
								?>
									<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
								<?php }
									}
								}?>
							</select>
						</div>
					</div>
					<div class="control-group form-group">
						<label class="control-label" for="category_id">Fees Level*</label>
						<div class="controls">
							<select id="fees_level_id" name="fees_level_id" class="form-control"  >
								<option value="">Select Fees Level</option>
								<?php 
									if(isset($fees_level) && !empty($fees_level)){
										foreach($fees_level as $cdrow){
											$sel = ($cdrow->fees_level_id == $details[0]->fees_level_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->fees_level_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_level_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					<div class="control-group form-group">
						<label class="control-label">Fees Type*</label>
						<div class="controls">
							<select id="fees_type" name="fees_type" class="form-control" onchange="getFees(this.value);">
								<option value="">Select Fees Type</option>
								<?php 
									if(isset($fees_type_details) && !empty($fees_type_details)){
										foreach($fees_type_details as $key=>$val){
											$sel = ($val == $details[0]->fees_type) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $val;?>" <?php echo $sel; ?>><?php echo $val;?></option>
								<?php }}?>
							</select>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label"><span>Fees*</span></label> 
						<div class="controls">
							<select id="fees_id" name="fees_id" class="form-control" onchange="getCourses(this.value);getGroups(this.value);" >
								<option value="">Select Fees</option>
							</select>
						</div>
					</div>
					<div class="control-group form-group class_div">
						<label class="control-label"><span>Course*</span></label> 
						<div class="controls">
							<select id="course_id" name="course_id[]" class="form-control selectpicker required" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ...">
							</select>
						</div>
					</div>

					<div class="control-group form-group group_div">
						<label class="control-label" for="category_id">Group*</label>
						<div class="controls">
							<select id="group_id" name="group_id[]" class="form-control selectpicker required" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..."  >
							</select>
						</div>
					</div>

					
					<div class="control-group form-group">
						<label class="control-label" for="zone_id">Zone*</label>
						<div class="controls">
							<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);" >
								<option value="">Select Zone</option>
								<?php 
									if(isset($zones) && !empty($zones)){
										foreach($zones as $cdrow){
											$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					<div class="control-group form-group">
						<label class="control-label"><span>Center*</span></label> 
						<div class="controls">
							<select id="center_id" name="center_id[]" class="form-control center_id selectpicker" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..."></select>
						</div>
					</div>
					<div class="input-daterange">
						<div class="control-group form-group ">
							<label class="control-label"><span>Open for Admission FROM*</span></label>
							<div class="controls">
								<input type="text" class="form-control required text-left" placeholder="Fee Applicable FROM Batch Start Date" id="open_from_date" name="open_from_date" value="<?php if(!empty($details[0]->open_from_date)){echo date("d-m-Y",strtotime($details[0]->open_from_date));}else{echo "";}?>" style="text-align:left;">
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label"><span>Open for Admission TO*</span></label>
							<div class="controls">
								<input type="text" class="form-control required text-left" placeholder="Fee Applicable FROM Batch Start Date" id="open_to_date" name="open_to_date" value="<?php if(!empty($details[0]->open_to_date)){echo date("d-m-Y",strtotime($details[0]->open_to_date));}else{echo "";}?>" style="text-align:left;">
							</div>
						</div>
					</div>
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>assignfees" class="btn btn-primary">Cancel</a>
					</div>
				</form>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>
$(document).ready(function(){
	$('.selectpicker').selectpicker('refresh');

	// date range validations
	$("#open_from_date").datepicker({
		// startDate: new Date(),
		autoclose: true,
		todayHighlight: true,
		format:"dd-mm-yyyy",
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#open_to_date').datepicker('setStartDate', startDate);
    });
    $("#open_to_date").datepicker({
		autoclose: true,
		format:"dd-mm-yyyy",
	}).on('changeDate', function (selected) {
		var endDate = new Date(selected.date.valueOf());
		$('#open_from_date').datepicker('setEndDate', endDate);
	});
})

 
$( document ).ready(function() {
	$('.group_div').hide();
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#center_id > option").prop("selected","selected");
			$("#center_id").trigger("change");
		}else{
			$("#center_id > option").removeAttr("selected");
			$("#center_id").trigger("change");
		}
	});
	$("#selectall").click(function(){
		$("#center_id > option").prop("selected","selected");
		$("#center_id").trigger("change");
	})
	getCourses($("#fees_id").val());
	getGroups($("#fees_id").val());
	<?php 
		if(!empty($details[0]->assign_fees_id)){
	?>
		getCourses('<?php echo $details[0]->fees_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getGroups('<?php echo $details[0]->fees_id; ?>','<?php echo $details[0]->group_id; ?>');
		getFees('<?php echo $details[0]->fees_level_id; ?>','<?php echo $details[0]->fees_type; ?>' ,'<?php echo $details[0]->fees_id; ?>','<?php echo $details[0]->fees_selection_type; ?>');
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php 
		if($details[0]->fees_selection_type == 'Class'){ ?>
			$('.class_div').show();
			$('.group_div').hide();
		<?php
		}
		else{ ?>
			$('.class_div').hide();
			$('.group_div').show();
		<?php
		} 
	}?>

	$('.fees_selection_type').change(function(){
		var fees_selection_type = $(this).val();
		if(fees_selection_type == 'Class'){
			$('.class_div').show();
			$('.group_div').hide();
		}
		else{
			$('.class_div').hide();
			$('.group_div').show();
		}
		getFees(fees_level_id = null,fees_type = null,fees_id = null,type=null)
	})
	
});


function getGroups(fees_id,group_id = null){
	var assign_fees_id = $("#assign_fees_id").val();
	if(fees_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>assignfees/getGroups",
			data:{fees_id:fees_id, group_id:group_id,assign_fees_id:assign_fees_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#group_id").html(res['option']);
					}
					else
					{
						$("#group_id").html("");
					}
				}
				else
				{	
					$("#group_id").html("");
				}
				$("#group_id").selectpicker("refresh");
			}
		});
	}
}

function getCourses(fees_id,course_id = null)
{
	var assign_fees_id = $("#assign_fees_id").val();
	if(fees_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>assignfees/getCourses",
			data:{fees_id:fees_id, course_id:course_id,assign_fees_id:assign_fees_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#course_id").html(res['option']);
					}
					else
					{
						$("#course_id").html("");
					}
				}
				else
				{	
					$("#course_id").html("");
				}
				$("#course_id").selectpicker("refresh");
			}
		});
	}
}
// function getCourses(category_id,course_id = null)
// {
// 	//alert("Val: "+val);return false;
// 	if(category_id != "" )
// 	{
// 		$.ajax({
// 			url:"<?php echo base_url();?>assignfees/getCourses",
// 			data:{category_id:category_id, course_id:course_id},
// 			dataType: 'json',
// 			method:'post',
// 			success: function(res)
// 			{
// 				if(res['status']=="success")
// 				{
// 					if(res['option'] != '')
// 					{
// 						$("#course_id").html("<option value=''>Select</option>"+res['option']);
// 					}
// 					else
// 					{
// 						$("#course_id").html("<option value=''>Select</option>");
// 					}
// 				}
// 				else
// 				{	
// 					$("#course_id").html("<option value=''>Select</option>");
// 				}
// 			}
// 		});
// 	}
// }

function getFees(fees_level_id,fees_type,fees_id = null)
{
	//alert("Val: "+val);return false;
	var fees_level_id = $('#fees_level_id').val()
	var fees_type = $('#fees_type').val()
	if($('.classname').is(':checked') == true){
		var type = 'Class';
	}
	else{
		var type = 'Group';
	}
	if(fees_level_id != "" && fees_type != "")
	{
		$.ajax({
			url:"<?php echo base_url();?>assignfees/getFees",
			data:{fees_level_id:fees_level_id,fees_type:fees_type, fees_id:fees_id,type:type},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#fees_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#fees_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#fees_id").html("<option value=''>Select</option>");
				}
			}
		});
	}else{
		$("#fees_id").html("<option value=''>Select</option>");
	}
}

function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	var assign_fees_id = $("#assign_fees_id").val();
	var fees_level_id = $('#fees_level_id').val()
	if(zone_id != "" && fees_level_id !="")
	{
		$.ajax({
			url:"<?php echo base_url();?>assignfees/getCenters",
			data:{zone_id:zone_id, center_id:center_id,assign_fees_id:assign_fees_id,fees_level_id:fees_level_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						// $("#center_id").html("<option value='' disabled='disabled'>Select</option>"+res['option']);
						$("#center_id").html(res['option']);
						if($("#center_id").hasClass("selectpicker")){
							$('#center_id').selectpicker('refresh');
						}
					}
					else
					{
						$("#center_id").html("<option value='' disabled='disabled'>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value='' disabled='disabled'>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	"center_id[]":{required:true},
	fees_level_id:{required:true},
	fees_type:{required:true},
	fees_id:{required:true},
	zone_id:{required:true},
	open_from_date:{required:true},
	open_to_date:{required:true},
};
var vMessages = {
	category_id:{required:"Please select category."},
	"center_id[]":{required:"Please select centers"},
	fees_level_id:{required:"Please select fees level."},
	fees_type:{required:"Please select fees type."},
	fees_id:{required:"Please select fees."},
	zone_id:{required:"Please select zone."},
	open_from_date:{required:"Please select From date"},
	open_to_date:{required:"Please select To date"},
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>assignfees/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>assignfees";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Assign Fees";

 
</script>					
