<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Assignfees extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('assignfeesmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('assignfees/index');
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			$result = [];
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				$result['details'] = $this->assignfeesmodel->getFormdata($record_id);
			}
			$result['academic_year'] = $this->assignfeesmodel->getDropdown("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			$result['categories'] = $this->assignfeesmodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['groups'] = $this->assignfeesmodel->getDropdown("tbl_group_master","group_master_id,group_master_name");
			$result['fees_level'] = $this->assignfeesmodel->getDropdown("tbl_fees_level_master","fees_level_id,fees_level_name");
			$result['zones'] = $this->assignfeesmodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['fees_type_details'] = $this->assignfeesmodel->enum_select("tbl_assign_fees","fees_type");

			$this->load->view('template/header.php');
			$this->load->view('assignfees/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		// $result = $this->assignfeesmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		$result = $this->assignfeesmodel->getMappedCourses($_REQUEST['fees_id']);
		
		$option = '';
		$course_ids = array();

		//get selected course_ids
		if(!empty($_REQUEST['assign_fees_id'])){
			$selected_course_data = $this->assignfeesmodel->getdata("tbl_assign_fees_course_mapping","assign_fees_id='".$_REQUEST['assign_fees_id']."' ");
			if(!empty($selected_course_data)){
				$course_ids = array_column($selected_course_data,"course_id");
			}
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel="";
				if(!empty($course_ids) && in_array($result[$i]->course_id,$course_ids)){
					$sel="selected";
				}
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getGroups(){
		$result = $this->assignfeesmodel->getMappedGroups($_REQUEST['fees_id']);
		
		$option = '';
		$group_ids = array();

		//get selected group_ids
		if(!empty($_REQUEST['assign_fees_id'])){
			$selected_group_data = $this->assignfeesmodel->getdata("tbl_assign_fees_group_mapping","assign_fees_id='".$_REQUEST['assign_fees_id']."' ");
			if(!empty($selected_group_data)){
				$group_ids = array_column($selected_group_data,"group_master_id");
			}
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel="";
				if(!empty($group_ids) && in_array($result[$i]->group_master_id,$group_ids)){
					$sel="selected";
				}
				$option .= '<option value="'.$result[$i]->group_master_id.'" '.$sel.' >'.$result[$i]->group_master_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}




	public function getFees(){
		// print_r($_REQUEST);exit();
		// $result = $this->assignfeesmodel->getOptionFees("tbl_fees_master",$_REQUEST['fees_level_id'],"fees_level_id",$_REQUEST['type'],"fees_selection_type");
		$condition = " fees_level_id='".$_REQUEST['fees_level_id']."' AND fees_selection_type = '".$_REQUEST['type']."' AND fees_type ='".$_REQUEST['fees_type']."' ";
		$result = $this->assignfeesmodel->getdata("tbl_fees_master",$condition);
		
		$option = '';
		$fees_id = '';
		
		if(isset($_REQUEST['fees_id']) && !empty($_REQUEST['fees_id'])){
			$fees_id = $_REQUEST['fees_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['fees_id'] == $fees_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['fees_id'].'" '.$sel.' >'.$result[$i]['fees_name'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		// $result = $this->assignfeesmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		// get mapped centers for fee level
		$result = $this->assignfeesmodel->getFeeLevelCenters($_REQUEST['fees_level_id'],$_REQUEST['zone_id']);
		//get selected centers
		$selected_centers = array();
		if(!empty($_REQUEST['assign_fees_id'])){
			$selected_centers_result = $this->assignfeesmodel->getdata("tbl_assign_fees_center_mapping","assign_fees_id='".$_REQUEST['assign_fees_id']."' ");
			if(!empty($selected_centers_result)){
				$selected_centers = array_column($selected_centers_result,"center_id");
			}
		}
		
		$option = '';
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = "";
				if(!empty($selected_centers) && in_array($result[$i]->center_id,$selected_centers)){
					$sel = "selected";
				}
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['center_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select center!'));
				exit;
			}
			if(!empty($_POST['center_id'])){
				foreach($_POST['center_id'] as $key=>$val){
					$condition = "academic_year_id='".$_POST['academic_year_id']."' && category_id='".$_POST['category_id']."' &&  fees_level_id='".$_POST['fees_level_id']."' && zone_id= '".$_POST['zone_id']."' && fees_id='".$_POST['fees_id']."' ";
					
					if(isset($_POST['assign_fees_id']) && $_POST['assign_fees_id'] > 0){
						$condition .= " &&  assign_fees_id != ".$_POST['assign_fees_id'];
					}
					
					$check_name = $this->assignfeesmodel->getdata("tbl_assign_fees",$condition);
					
					if(!empty($check_name)){
						//check mapped
						$center_exist = $this->assignfeesmodel->getdata("tbl_assign_fees_center_mapping"," assign_fees_id !='".$check_name[0]['assign_fees_id']."' && center_id='".$val."' ");
						if(!empty($center_exist)){
							$getCenterName = $this->assignfeesmodel->getdata("tbl_centers", "center_id='".$val."'");
							echo json_encode(array("success"=>"0",'msg'=>' '.$getCenterName[0]['center_name'].' Already assign to selected center!'));
							exit;
						}
					}
				}
			}
			$assign_fees_id =  "";
			if (!empty($_POST['assign_fees_id'])) {
				$condition = "academic_year_id='".$_POST['academic_year_id']."' && category_id='".$_POST['category_id']."' &&  fees_level_id='".$_POST['fees_level_id']."' && zone_id= '".$_POST['zone_id']."' && fees_id='".$_POST['fees_id']."' ";
				if(isset($_POST['assign_fees_id']) && $_POST['assign_fees_id'] > 0){
					$condition .= " &&  assign_fees_id != ".$_POST['assign_fees_id'];
				}
				
				$check_name = $this->assignfeesmodel->getdata("tbl_assign_fees",$condition);
				
				// if(!empty($check_name)){
				// 	$getCenterName = $this->assignfeesmodel->getdata("tbl_centers", "center_id='".$_POST['center_id']."' ");
				// 	echo json_encode(array("success"=>"0",'msg'=>' '.$getCenterName[0]['center_name'].' Already assign to selected center!'));
				// 	exit;
				// }
				if(empty($_POST['center_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Please select center!'));
					exit;
				}
				$data_array = array();			
				$assign_fees_id = $_POST['assign_fees_id'];
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				// $data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				// $data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['fees_level_id'] = (!empty($_POST['fees_level_id'])) ? $_POST['fees_level_id'] : '';
				$data_array['fees_type'] = (!empty($_POST['fees_type'])) ? $_POST['fees_type'] : '';
				$data_array['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				//from and to date
				$data_array['open_from_date'] = (!empty($_POST['open_from_date'])) ? date("Y-m-d", strtotime($_POST['open_from_date'])) : '';
				$data_array['open_to_date'] = (!empty($_POST['open_to_date'])) ? date("Y-m-d", strtotime($_POST['open_to_date'])) : '';


				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->assignfeesmodel->updateRecord('tbl_assign_fees', $data_array,'assign_fees_id',$assign_fees_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				// $data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				// $data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['fees_level_id'] = (!empty($_POST['fees_level_id'])) ? $_POST['fees_level_id'] : '';
				$data_array['fees_type'] = (!empty($_POST['fees_type'])) ? $_POST['fees_type'] : '';
				$data_array['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				
				//from and to date
				$data_array['open_from_date'] = (!empty($_POST['open_from_date'])) ? date("Y-m-d", strtotime($_POST['open_from_date'])) : '';
				$data_array['open_to_date'] = (!empty($_POST['open_to_date'])) ? date("Y-m-d", strtotime($_POST['open_to_date'])) : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				$result = $this->assignfeesmodel->insertData('tbl_assign_fees', $data_array, '1');
				$assign_fees_id = $result;
			}
		
			if(!empty($_POST['center_id']) && $assign_fees_id != ""){
				//delete existing mapping
				$this->assignfeesmodel->deletedata("tbl_assign_fees_center_mapping","assign_fees_id",$assign_fees_id);
				foreach($_POST['center_id'] as $key=>$val){
					$mapping_center_data = array();
					$mapping_center_data['assign_fees_id'] = $assign_fees_id;
					$mapping_center_data['center_id'] = $val;
					$this->assignfeesmodel->insertData('tbl_assign_fees_center_mapping', $mapping_center_data, '1');
				}
			}

			//for course insertion mapping
			if(!empty($_POST['course_id']) && $assign_fees_id != ""){
				//delete existing mapping
				$this->assignfeesmodel->deletedata("tbl_assign_fees_group_mapping","assign_fees_id",$assign_fees_id);
				$this->assignfeesmodel->deletedata("tbl_assign_fees_course_mapping","assign_fees_id",$assign_fees_id);

				foreach($_POST['course_id'] as $key=>$val){
					$mapping_center_data = array();
					$mapping_center_data['assign_fees_id'] = $assign_fees_id;
					$mapping_center_data['course_id'] = $val;
					$this->assignfeesmodel->insertData('tbl_assign_fees_course_mapping', $mapping_center_data, '1');
				}
			}

			//for group insertion mapping
			if(!empty($_POST['group_id']) && $assign_fees_id != ""){
				//delete existing mapping
				$this->assignfeesmodel->deletedata("tbl_assign_fees_group_mapping","assign_fees_id",$assign_fees_id);
				$this->assignfeesmodel->deletedata("tbl_assign_fees_course_mapping","assign_fees_id",$assign_fees_id);

				foreach($_POST['group_id'] as $key=>$val){
					$mapping_center_data = array();
					$mapping_center_data['assign_fees_id'] = $assign_fees_id;
					$mapping_center_data['group_master_id'] = $val;
					$this->assignfeesmodel->insertData('tbl_assign_fees_group_mapping', $mapping_center_data, '1');
				}
			}
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->assignfeesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->fees_selection_type);
				array_push($temp, $get_result['query_result'][$i]->fees_level_name);
				array_push($temp, $get_result['query_result'][$i]->fees_name);
				// array_push($temp, $get_result['query_result'][$i]->course_name);
				// array_push($temp, $get_result['query_result'][$i]->group_master_name);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("AssignFeesAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->assign_fees_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("AssignFeesAddEdit")){
					$actionCol1.= '<a href="assignfees/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->assign_fees_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->assignfeesmodel->delrecord12("tbl_assign_fees","assign_fees_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	
}

?>
