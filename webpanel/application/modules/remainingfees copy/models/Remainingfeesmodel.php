<?PHP
class Remainingfeesmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}

	function getDropdown1($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status = "Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getStudentClassWiseList($condition){
		
		$this -> db -> select('s.student_id,s.inquiry_master_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name,s.dob, d.admission_date, d.academic_year_id, d.category_id, d.course_id, d.batch_id ');
		$this -> db -> from('tbl_student_details as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('s.student_first_name', 'asc');
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}

	function getdata($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getdata1($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }

	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 

	function getPayemtDetails($condition){
		
		$this -> db -> select('i.*, s.student_id, s.student_first_name, s.student_last_name,s.enrollment_no, s.father_name, s.present_address, s.father_mobile_contact_no, s.father_email_id, ct.categoy_name, cr.course_name, b.batch_name, g.group_master_name, a.academic_year_master_name, cn.center_name, cn.center_address, cn.center_contact_no, cn.center_email_id, cn.center_code,fp.*');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id = g.group_master_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'i.center_id = cn.center_id', 'left');
		$this -> db -> join('tbl_fees_payment_details as fp', 'i.admission_fees_id = fp.admission_fees_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getFeesComponents($condition){
		
		$this -> db -> select('i.*,f.fees_component_master_id, f.fees_component_master_name');
		$this -> db -> from('tbl_fees_component_data as i');
		$this -> db -> join('tbl_fees_component_master as f', 'i.fees_component_id  = f.fees_component_master_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getStudentGroupWiseList($condition){
		
		$this -> db -> select('s.student_id,s.inquiry_master_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name,s.dob, d.academic_year_id');
		$this -> db -> from('tbl_student_group as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('s.student_first_name', 'asc');
		$this->db->group_by('s.student_id', 'asc');
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}

	/* ----------------------------------receipt functions ------------------------------------ */
	function getRecords($get){
		$table = "tbl_fees_payment_receipt as i";
		$table_id = 'i.fees_payment_receipt_id';
		$default_sort_column = 'i.fees_payment_receipt_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		// if($_SESSION["webadmin"][0]->role_id != 1){
		// 	$condition .= "  center_id='".$_SESSION["webadmin"][0]->center_id."' ";
		// }else{
			$condition = "  1=1 ";
		// }
		$colArray = array('i.created_on','i.receipt_no','i.receipt_amount','i.payment_mode','sm.student_first_name');
		$searchArray = array('i.created_on','i.receipt_no','i.receipt_amount','i.payment_mode','sm.student_first_name');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;
		
		$this -> db -> select('i.*,CONCAT(sm.student_first_name," ",sm.student_last_name) as student_name');
		$this -> db -> from($table);
		$this -> db -> join("tbl_admission_fees as af","af.admission_fees_id = i.admission_fees_id");
		$this -> db -> join("tbl_student_master as sm","sm.student_id = af.student_id");
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$this->db->group_by('i.receipt_no');		
		$query = $this -> db -> get();
		
		// echo $this->db->last_query();
		$this -> db -> select('i.*,CONCAT(sm.student_first_name," ",sm.student_last_name) as student_name');
		$this -> db -> from($table);
		$this -> db -> join("tbl_admission_fees as af","af.admission_fees_id = i.admission_fees_id");
		$this -> db -> join("tbl_student_master as sm","sm.student_id = af.student_id");
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);	
		$this->db->group_by('i.receipt_no');	
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1){
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}else{
			return array("totalRecords"=>0);
		}
		//exit;
	}
}
?>
