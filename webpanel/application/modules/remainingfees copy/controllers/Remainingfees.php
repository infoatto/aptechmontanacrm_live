<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Remainingfees extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('remainingfeesmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$result['academicyear'] = $this->remainingfeesmodel->getDropdown1("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			$result['zones'] = $this->remainingfeesmodel->getDropdown1("tbl_zones","zone_id,zone_name");
			$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
			$result['groups'] = $this->remainingfeesmodel->getDropdown1("tbl_group_master", "group_master_id, group_master_name");

			$this->load->view('template/header.php');
			$this->load->view('remainingfees/addEdit',$result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	public function getCenters(){
		$result = $this->remainingfeesmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getCenterStudent(){
		$condition ="  d.academic_year_id='".$_REQUEST['academic_year_id']."' && s.zone_id='".$_REQUEST['zone_id']."' && s.center_id='".$_REQUEST['center_id']."' ";
						// print_r($condition);exit();
		$result = $this->remainingfeesmodel->getStudentClassWiseList($condition);
		
		$option = '';
		$student_id = '';
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->student_id == $student_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->student_id.'" '.$sel.' >'.$result[$i]->enrollment_no.' ( '.$result[$i]->student_first_name.' '.$result[$i]->student_last_name.' )</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getGroupStudent(){
		$condition ="  d.academic_year_id='".$_REQUEST['academic_year_id']."' && d.group_id='".$_REQUEST['group_id']."' && d.zone_id='".$_REQUEST['zone_id']."' && d.center_id='".$_REQUEST['center_id']."' ";
		$result = $this->remainingfeesmodel->getStudentGroupWiseList($condition);
		// echo $this->db->last_query();
		// echo "<pre>";
		// print_r($result);exit();
		$option = '';
		$student_id = '';
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->student_id == $student_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->student_id.'" '.$sel.' >'.$result[$i]->enrollment_no.' ( '.$result[$i]->student_first_name.' '.$result[$i]->student_last_name.' )</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getStudentDetails(){
		// $condition = "student_id='".$_REQUEST['student_id']."' && academic_year_id='".$_REQUEST['academic_year_id']."' && zone_id='".$_REQUEST['zone_id']."' && center_id='".$_REQUEST['center_id']."' && center_id='".$_REQUEST['center_id']."' && fees_type='".$_REQUEST['type']."'"; 
		$condition = "student_id='".$_REQUEST['student_id']."' && academic_year_id='".$_REQUEST['academic_year_id']."' && zone_id='".$_REQUEST['zone_id']."' && center_id='".$_REQUEST['center_id']."' && center_id='".$_REQUEST['center_id']."' "; 
		$getDetails = $this->remainingfeesmodel->getdata("tbl_admission_fees", "admission_fees_id, academic_year_id, student_id, fees_id, fees_type, zone_id, center_id, category_id, course_id, group_id, is_instalment, no_of_installments, fees_total_amount, fees_amount_collected, fees_remaining_amount", $condition);
		echo "<pre>";print_r($getDetails);exit;

		if (!empty($getDetails)) {
			$all_installments = array();
			foreach($getDetails as $key=>$val){
				$all_installments = $this->remainingfeesmodel->getdata("tbl_admission_fees_instalments", "admission_fees_instalment_id, admission_fees_id, instalment_due_date, instalment_amount, instalment_collected_amount, instalment_remaining_amount, instalment_status, created_on, updated_on", "academic_year_id='".$_REQUEST['academic_year_id']."' && student_id='".$_REQUEST['student_id']."' && admission_fees_id='".$getDetails[0]['admission_fees_id']."' ");
				if(!empty($all_installments)){
					for($i=0; $i < sizeof($all_installments); $i++){
						$all_installments[$i]['instalment_due_date'] = date("d M Y", strtotime($all_installments[$i]['instalment_due_date']));
						if($getDetails[$key]['fees_type'] == 'Class'){
							$getCategoryDetails = $this->remainingfeesmodel->getdata("tbl_categories", "categoy_name", "category_id='".$getDetails[$key]['category_id']."' ");
							$getCourseDetails = $this->remainingfeesmodel->getdata("tbl_courses", "course_name", "course_id='".$getDetails[$key]['course_id']."' ");
							$all_installments[$i]['categoy_name'] = (!empty($getCategoryDetails[0]['categoy_name'])) ? $getCategoryDetails[0]['categoy_name'] : '';
							$all_installments[$i]['course_name'] = (!empty($getCourseDetails[0]['course_name'])) ? $getCourseDetails[0]['course_name'] : '';
						}else{
							$getGroupDetails = $this->remainingfeesmodel->getdata("tbl_group_master", "group_master_name", "group_master_id='".$getDetails[$key]['group_id']."' ");
							$all_installments[$i]['group_name'] = (!empty($getGroupDetails[0]['group_master_name'])) ? $getGroupDetails[0]['group_master_name'] : '';
						}
					}
				}
				if(!empty($all_installments)){
					$getDetails[$key]['instalment_details'] = $all_installments;
				}else{
					$getDetails[$key]['instalment_details'] = NULL;
				}
			}
		}
		
		echo "<pre>"; print_r($getDetails);exit;
		echo json_encode(array("status"=>"success","option"=>$getDetails));
		exit;
	}

	function submitForm(){
		// echo "<pre>";
		// print_r($_POST);exit();
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(isset($instalment_collected_amount)){
				if(empty($instalment_collected_amount[0])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Installment Collected Amount!'));
					exit;
				}
			}

			$total_amount = 0;
			$already_collected_amount = 0;
			$total_remaining_amount = 0;
			for ($i=0; $i < sizeof($_POST['admission_fees_instalment_id']); $i++) { 
				$total_amount = $total_amount + $_POST['installment_collected_amount'][$i];
				$already_collected_amount = $already_collected_amount + $_POST['instalment_collected_amount'][$i];
				$total_remaining_amount = $total_remaining_amount + $_POST['installment_remaining_amount'][$i];
			}
			// print_r($total_remaining_amount);exit();
			$total_collected_amount = $total_amount + $already_collected_amount;
			$feesdata['fees_amount_collected'] = (!empty($total_collected_amount)) ? $total_collected_amount : 0;
			$feesdata['fees_remaining_amount'] = (!empty($total_remaining_amount)) ? $total_remaining_amount : 0;
			$feesdata['updated_on'] = date("Y-m-d H:m:i");
			$feesdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			$this->remainingfeesmodel->updateRecord('tbl_admission_fees', $feesdata, "admission_fees_id",$_POST['admission_fees_id']);


			for ($ins=0; $ins <sizeof($_POST['admission_fees_instalment_id']) ; $ins++) {
				$collected_amount = $_POST['instalment_collected_amount'][$ins] + $_POST['installment_collected_amount'][$ins];
				$installmentdata['instalment_collected_amount'] = (!empty($collected_amount) ? $collected_amount : 0);
				$installmentdata['instalment_remaining_amount'] = (!empty($_POST['installment_remaining_amount'][$ins]) ? $_POST['installment_remaining_amount'][$ins] : $_POST['instalment_amount']);
				if($collected_amount == '0'){
					$installmentdata['instalment_status'] = 'Pending';
				}else if($_POST['installment_remaining_amount'][$ins] == '0'){
					$installmentdata['instalment_status'] = 'Paid';
				}else if($_POST['instalment_amount'][$ins] > $collected_amount){
					$installmentdata['instalment_status'] = 'Partial';
				}
				$installmentdata['updated_on'] = date("Y-m-d H:m:i");
				$installmentdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				$result = $this->remainingfeesmodel->updateRecord('tbl_admission_fees_instalments', $installmentdata, "admission_fees_instalment_id",$_POST['admission_fees_instalment_id'][$ins]);
			}
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'admission_fees_id' => $_POST['admission_fees_id']
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}
		else {
			return false;
		}

	}

	function getPayemtDetails(){
		$getPayemtDetails = $this->remainingfeesmodel->getPayemtDetails("i.admission_fees_id='".$_REQUEST['admission_fees_id']."' ");

		$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
			$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
			$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
			$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
			$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
			$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
			$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
			$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
			$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
			$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
			$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
			
			$intallment_yes_no = (!empty($getPayemtDetails[0]['is_instalment'])) ? $getPayemtDetails[0]['is_instalment'] : '';
			$installment_nos = (!empty($getPayemtDetails[0]['no_of_installments'])) ? $getPayemtDetails[0]['no_of_installments'] : '';
			$discount_amount = (!empty($getPayemtDetails[0]['discount_amount'])) ? $getPayemtDetails[0]['discount_amount'] : '';
			$gst_amount = (!empty($getPayemtDetails[0]['gst_amount'])) ? $getPayemtDetails[0]['gst_amount'] : '';
			$total_fees = (!empty($getPayemtDetails[0]['fees_total_amount'])) ? $getPayemtDetails[0]['fees_total_amount'] : '';
			$total_amount = (!empty($getPayemtDetails[0]['total_amount'])) ? $getPayemtDetails[0]['total_amount'] : '';
			$collected_fees = (!empty($getPayemtDetails[0]['fees_amount_collected'])) ? $getPayemtDetails[0]['fees_amount_collected'] : '';
			$remaining_fees = (!empty($getPayemtDetails[0]['fees_remaining_amount'])) ? $getPayemtDetails[0]['fees_remaining_amount'] : '';
			
			$getRecieptNo = $this->remainingfeesmodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "1=1", "order by fees_payment_receipt_id desc");
			
			$reciept_id = (!empty($getRecieptNo)) ? ($getRecieptNo[0]['fees_payment_receipt_id'] + 1) : 1;
			
			$reciept_no = "";
			if(!empty($center_code)){
				$reciept_no .= $center_code."/";
			}
			if(!empty($academic_year)){
				$reciept_no .= $academic_year."/";
			}
			
			$reciept_no .= $reciept_id;
			
			$_POST['isadmission'] = 'No';
			if(!empty($getPayemtDetails[0]['father_email_id']) && $_POST['isadmission'] == 'Yes'){
			
				ini_set('memory_limit','100M');

				$date_v = date("d_m_Y_H_i_s");
				$pdf_filename1 = "receipt_".$date_v.".pdf";
				$receipt_pdfFilePath = DOC_ROOT_FRONT_API."/images/payment_invoices/$pdf_filename1";
				$file_path = FRONT_API_URL."/images/payment_invoices/$pdf_filename1";
				
				$message = '';
				$message  = '<html><body>';	
				$message .= '<div style="width:750px;padding:0 30px;">
									<div style="margin:10px;">
									<img src="https://www.aptechmontanapreschool.com/wp-content/uploads/2018/06/aptech-logo-1.png" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
									</div>';
									$message .= '<div>
										<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$center_name.'</h1>
										<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;">'.$center_address.'</p>
										<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p>
									</div>';
									
									$message .= '<div style="height:1px;background:#000;margin:10px 0">&nbsp;</div>
									<h4 style="text-align: center;font-size:20px;margin:10px 0;padding:0">Booking Confirmation</h4>
									<table cellpadding="0" cellspacing="0" border="0" style="border:0px none;width:700px;">
										<tr>
											<td style="width:350px;padding:10px;text-align:left;">
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Name:</strong>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Address:</strong>'.$getPayemtDetails[0]['present_address'].'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Student ID:</strong>'.$getPayemtDetails[0]['enrollment_no'].'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Program Family:</strong>'.$getPayemtDetails[0]['categoy_name'].'</p>
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Program Name:</strong>'.$getPayemtDetails[0]['course_name'].'</p>
											</td>
											<td style="width:350px;padding:10px;text-align:left;">
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Number:</strong>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Date:</strong>'.date("d-M-Y").'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Email ID:</strong>'.$getPayemtDetails[0]['father_email_id'].'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Currency:</strong>INR</p>
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Number: '.$reciept_no.'</strong></p>
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Date: '.date("d-M-Y").'</strong></p>
											</td>
										</tr>
									</table>
									<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
										<tr>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component	</th>
											<th style="padding:10px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
										</tr>';
										//get fess components
										$component_condition = "f.status='Active' && i.fees_master_id='".$getPayemtDetails[0]['fees_id']."' ";
										$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);
										if(!empty($getFeesComponents)){
											for($i=0; $i < sizeof($getFeesComponents); $i++){
												$message .= '<tr>						
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['fees_component_master_name'].'</strong></td>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['component_fees'].'</strong></td>					
												</tr>';
											}
										}
											
										
										$message .= '<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Fees Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_fees.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_amount.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Collected Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$collected_fees.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Remaining Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$remaining_fees.' /-</strong></td>						
										</tr>
										
									</table>';
									if($intallment_yes_no == 'Yes'){
									$message .= '
										
										<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:750px;margin:20px auto;">
												<tr>
													<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Number</th>
													<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Planned Installment Date</th>
													<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Amount</th>
													<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Paid Amount</th>		
													<th style="padding:5px;text-align:center;font-size:14px;border-bottom:1px solid #000">Remaining Amount</th>
												</tr>
												<tr>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
													<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">(In INR)</td>
												</tr>';
											//get installment details
											$getPayemtInstallmentDetails = $this->admissionmodel->getdata("tbl_admission_fees_instalments", "admission_fees_id='".$_REQUEST['admission_fees_id']."' " );	
											if(!empty($getPayemtInstallmentDetails)){
												$count = 1;
												for($i=0; $i < sizeof($getPayemtInstallmentDetails); $i++){
													
													$message .= '<tr>
														<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$count.'</td>
														<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.date("M Y", strtotime($getPayemtInstallmentDetails[$i]['instalment_due_date'])).'</td>
														<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_amount'].'</td>	
														<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_collected_amount'].'</td>		
														<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_remaining_amount'].'</td>
													</tr>';
													$count++;
												}
											}
											
											$message .= '</table>';
									}
									
									$message .= '<p style="font-size:15px;margin:4px 0;">*CHEQUES SUBJECT TO REALISATION</p>
									<p style="font-size:15px;margin:4px 0;">*NO CHANGE OR CASH CAN BE TAKEN IN EXCHANGE OF GIFT VOUCHER</p>
									<p style="font-size:15px;margin:4px 0;">THIS RECEIPT MUST BE PRODUCED WHEN DEMANDED</p>
									<p style="font-size:15px;margin:4px 0;">FEES ONCE PAID ARE NOT REFUNDABLE</p>
									<p style="font-size:15px;margin:4px 0;">SUBJECT TO TERMS AND CONDITION PRINTED OVERLEAF THE BOOKING CONFIRMATION</p>
									<p style="font-size:15px;margin:4px 0;">1) This Booking Confirmation provides provisional admission to the child.</p>
									<p style="font-size:15px;margin:4px 0;">2) Ensure timely payments as mentioned in the Booking confirmation.</p>
									<p style="font-size:15px;margin:4px 0;">3) Taxes, as applicable, at the time of payment will be charged extra.</p>
									<p style="font-size:15px;margin:4px 0;">4) Parents are required to inform the school if there is any change in their address and telephone numbers.</p>
									<p style="font-size:15px;margin:4px 0;">5) The parents are responsible for timely drop and pick up of child.</p>
									<p style="font-size:15px;margin:4px 0;">6) Please refer detailed code of conduct and policy Guidelines sent on your mail.</p>
									<p style="font-size:15px;margin:4px 0 50px 0;">I have read and understood the code of conduct and payment terms / installment plan mentioned above and agree to abide by them and also the terms and conditions printed overleaf.</p>
									<div style="width:200px;float:right;text-align:center">
										<p style="font-size:15px;margin:4px 0;">The Montana <br/>International Preschool<br/> Pvt Ltd </p>
									</div>
									<div style="clear:both;margin-bottom:80px">&nbsp;</div>
									<div style="width:200px;float:left;text-align:center">
										<p style="font-size:15px;margin:4px 0;"><strong>Signature of Parent / Guardian </strong></p>
									</div>
									<div style="width:200px;float:right;text-align:center">
										<p style="font-size:15px;margin:4px 0 ;"><strong>AUTHORISED <br/>SIGNATORY</strong></p>
									</div>
									<div style="clear:both;">&nbsp;</div>
										<p style="font-size:15px;margin:15px 0;">For any feedback or suggestions please write to us at <a href="mailto:feedback@montanapreschool.com" style="color:#000;text-decoration:none;">feedback@montanapreschool.com</a></p>
										<p style="font-size:15px;margin:15px 0;">Registered Office : 402, Sagar Tech Plaza, B wing, Andheri-Kurla Road, Sakinaka, Andheri East, Mumbai - 400 072 Maharashtra, India, </p>
										<p style="font-size:15px;margin:15px 0;"><a href="https://www.facebook.com/montanapreschool/" target="_blank" style="color:#000;text-decoration:none;">https://www.facebook.com/montanapreschool/</a>
										<p style="font-size:15px;margin:4px 0;">Disclaimer : Taxes will be charged extra, as applicable, on the date of payment.</p>
										</p>
									</div>';
				$message .=  '</body></html>';


				$this->load->library('M_pdf');
				//echo "here...";exit;
				$pdf = $this->m_pdf->load();
				//$pdf = new mPDF('utf-8',array($fwidth_mm,$fheight_mm),0,0,0,0,0,0);
				$pdf = new mPDF('utf-8');
				
				$pdf->WriteHTML($message); // write the HTML into the PDF
				$pdf->Output($receipt_pdfFilePath, 'F');
				
				$this->email->clear();
				$this->email->from("info@attoinfotech.in"); // change it to yours
				$subject = "";
				$subject = "Aptech Montana - Admission Confirmation";
				$this->email->to($getPayemtDetails[0]['father_email_id']);
				//$this->email->to("danish.akhtar@attoinfotech.com");
				$this->email->subject($subject);
				$this->email->message($message);
				$bfilePath = DOC_ROOT."/images/M_attachment.pdf";
				$this->email->attach($bfilePath);
				$checkemail = $this->email->send();
				$this->email->clear(TRUE);
			}

			//genrate invoice pdf
			ini_set('memory_limit','100M');
			//echo "here";exit;
			//$this->load->library('m_pdf');
			//echo "here1";exit;
			$date_v = date("d_m_Y_H_i_s");
			$pdf_filename = "paymentinvoice_".$date_v.".pdf";
			$invoice_pdfFilePath = DOC_ROOT_FRONT_API."/images/payment_invoices/$pdf_filename";
			$file_path = FRONT_API_URL."/images/payment_invoices/$pdf_filename";

			if($getPayemtDetails[0]['payment_mode'] == 'Cheque'){
				$transaction_no = $getPayemtDetails[0]['cheque_no'];
			}
			elseif($getPayemtDetails[0]['payment_mode'] == 'Nerbanking'){
				$transaction_no = $getPayemtDetails[0]['transaction_id'];
			}
			else{
				$transaction_no = '';
			}
			
			$html_invoice = '';
			$html_invoice = '<div style="width:750px;padding:0 30px;">
								<div style="margin:10px;">
									<img src="https://www.aptechmontanapreschool.com/wp-content/uploads/2018/06/aptech-logo-1.png" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
								</div>
								<div>';
								//get center information
								
									$html_invoice .= '<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$center_name.'</h1>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;">'.$center_address.' Helpline: '.$center_contact_no.'</p>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p>';
									
								$html_invoice .= '</div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
										<tr>
											<td>
												<table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
													<tr>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Date</th>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Enrol No</th>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Name</th>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Class/Group</th>
														<th style="width:150px;padding:10px;border-bottom:1px solid #000">Section(Batch)</th>
													</tr>
													<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>'.date("d-M-Y").'</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getPayemtDetails[0]['enrollment_no'].'</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$student_name.'</strong></td>';
													if($getPayemtDetails[0]['fees_type'] == 'Class'){		
														$html_invoice .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$course_name.'</strong></td>
														<td style="padding:10px;font-size:14px"><strong>'.$batch_name.'</strong></td>';
													}else{
														$html_invoice .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$group_name.'</strong></td>
														<td style="padding:10px;font-size:14px"><strong>-</strong></td>';
													}
														
													$html_invoice .= '</tr>
													<tr>
														<td colspan="2" style="padding:10px;text-align:left;font-size:14px">
															<p style="color:#000;font-size:14px;margin:3px 0;">Father Name: <strong>'.$getPayemtDetails[0]['father_name'].'</strong></p>
															<p style="color:#000;font-size:14px;margin:3px 0;">Address: '.$getPayemtDetails[0]['present_address'].'</p>
														</td>						
														<td colspan="2"  style="padding:10px;text-align:left;font-size:14px"> 
															<p style="color:#000;font-size:14px;margin:3px 0;">Payment Mode: <strong>'.$getPayemtDetails[0]['payment_mode'].'</strong></p>
														</td>					
														<td style="padding:10px;text-align:left;font-size:14px"> 
															<p style="color:#000;font-size:14px;margin:3px 0;">Receipt No:<strong>'.$reciept_no.'</strong></p>
														</td>
													</tr>					
												</table>
											</td>
										</tr>
									</table>

									
									
									<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;" >
										<tr>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component	</th>
											<th style="padding:10px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
										</tr>';
										//get fess components
										$component_condition = "f.status='Active' && i.fees_master_id='".$getPayemtDetails[0]['fees_id']."' ";
										$getFeesComponents = $this->remainingfeesmodel->getFeesComponents($component_condition);
										if(!empty($getFeesComponents)){
											for($i=0; $i < sizeof($getFeesComponents); $i++){
												$html_invoice .= '<tr>						
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['fees_component_master_name'].'</strong></td>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['component_fees'].'</strong></td>					
												</tr>';
											}
										}
											
										
										$html_invoice .= '<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Fees Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_fees.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Discount Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$discount_amount.' /-</strong></td>						
										</tr>

										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>GST Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$gst_amount.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_amount.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Collected Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$collected_fees.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Remaining Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$remaining_fees.' /-</strong></td>						
										</tr>
										
									</table>
									
									<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
																		<tr>						
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>Bank Name: '.$getPayemtDetails[0]['bank_name'].'</strong></td>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>Payment Mode: '.$getPayemtDetails[0]['payment_mode'].'</strong></td>
																		</tr>';
																	if($getPayemtDetails[0]['payment_mode'] == 'Cheque'){	
																		$html_invoice .= '<tr>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque No: '.$getPayemtDetails[0]['cheque_no'].'</strong></td>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: '.$getPayemtDetails[0]['transaction_date'].'</strong></td>
																		</tr>';
																	}else if($getPayemtDetails[0]['payment_mode'] == 'Netbanking'){	
																		$html_invoice .= '<tr>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction No: '.$getPayemtDetails[0]['transaction_id'].'</strong></td>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: '.$getPayemtDetails[0]['transaction_date'].'</strong></td>
																		</tr>';
																	}else{
																		$html_invoice .= '<tr>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
																		</tr>';
																	}
																		
												$html_invoice .= '</table>';
									if($intallment_yes_no == 'Yes'){				
									
									'<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:750px;margin:20px auto;">
											<tr>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Number</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Planned Installment Date</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Amount</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Paid Amount</th>		
												<th style="padding:5px;text-align:center;font-size:14px;border-bottom:1px solid #000">Remaining Amount</th>
											</tr>
											<tr>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
												<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">(In INR)</td>
											</tr>';
										//get installment details
										$getPayemtInstallmentDetails = $this->remainingfeesmodel->getdata1("tbl_admission_fees_instalments", "admission_fees_id='".$_REQUEST['admission_fees_id']."' " );
										// echo "<pre>";
										// print_r($getPayemtInstallmentDetails);exit();	
										if(!empty($getPayemtInstallmentDetails)){
											$count = 1;
											for($i=0; $i < sizeof($getPayemtInstallmentDetails); $i++){
												
												$html_invoice .= '<tr>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$count.'</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.date("M Y", strtotime($getPayemtInstallmentDetails[$i]['instalment_due_date'])).'</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_amount'].'</td>	
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_collected_amount'].'</td>		
													<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_remaining_amount'].'</td>
												</tr>';
												$count++;
											}
										}
										
										$html_invoice .= '</table>';
								}
									'<p style="font-size:15px;text-align:center;"><strong>*CHEQUES SUBJECT TO REALISATION. *NO CHANGE OR CASH CAN BE TAKEN IN EXCHANGE OF GIFT VOUCHER. THIS RECEIPT MUST BE PRODUCED WHEN DEMANDED. FEES ONCE PAID ARE NONREFUNDABLE AND NON-TRANSFERABLE.</strong></p>
								</div>';
			//echo $html_invoice;exit;
			
			
			$this->load->library('M_pdf');
			//echo "here...";exit;
			$pdf = $this->m_pdf->load();
			//$pdf = new mPDF('utf-8',array($fwidth_mm,$fheight_mm),0,0,0,0,0,0);
			$pdf = new mPDF('utf-8');
			
			$pdf->WriteHTML($html_invoice); // write the HTML into the PDF
			$pdf->Output($invoice_pdfFilePath, 'F'); // save to file because we can	
			
			
			
			//insert invoice file
			$invoice_file = array();
			$invoice_file['admission_fees_id'] = $_REQUEST['admission_fees_id'];
			$invoice_file['receipt_no'] = $reciept_no;
			$invoice_file['receipt_file'] = $pdf_filename;
			$invoice_file['receipt_file_withoutgst'] = $pdf_filename1;
			
			$invoice_file['created_on'] = date("Y-m-d H:m:i");
			$invoice_file['created_by'] = $_SESSION["webadmin"][0]->user_id;
			$invoice_file['updated_on'] = date("Y-m-d H:m:i");
			$invoice_file['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			
			$this->remainingfeesmodel->insertData('tbl_fees_payment_receipt', $invoice_file, 1);
			
				
			echo json_encode(array("status_code" => "200", "msg" => "Information saved successfully.", "body" => $file_path));
			exit;
	}
	
	/* -----------------------------receipt funcitons -------------------------------- */
	function fetch(){
		$get_result = $this->remainingfeesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				if(!empty($get_result['query_result'][$i])){
					array_push($temp, date("d-M-Y",strtotime($get_result['query_result'][$i]->created_on)));
					array_push($temp, $get_result['query_result'][$i]->receipt_no);
					array_push($temp, $get_result['query_result'][$i]->receipt_amount);
					array_push($temp, $get_result['query_result'][$i]->payment_mode);
					array_push($temp, $get_result['query_result'][$i]->student_name);
					// array_push($temp, $get_result['query_result'][$i]->fees_payment_receipt_id);
					// array_push($temp, $get_result['query_result'][$i]->fees_payment_receipt_id);
					array_push($items, $temp);
				}
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	/* -----------------------------receipt funcitons -------------------------------- */
	
}

?>
