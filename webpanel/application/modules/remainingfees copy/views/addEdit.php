<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Fee Payment Process</h1>            
          </div>
          <!-- <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>camera">Camera</a></li>
            </ul>
          </div> -->
    </div>
    <div class="card"> 
	    <div id='loadingmessage' style='display:none'>
	        <img src='<?php echo FRONT_URL; ?>/images/loading.gif?>'/>
	    </div>      
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label class="control-label">Academic Year*</label>
								<div class="controls">
									<select id="academic_year_id" name="academic_year_id" class="form-control" onchange="getCenterStudent(this.value)">
										<option value="">Select Academic Year</option>
										<?php 
											if(isset($academicyear) && !empty($academicyear)){
												foreach($academicyear as $cdrow){
										?>
											<option value="<?php echo $cdrow->academic_year_master_id;?>"><?php echo $cdrow->academic_year_master_name;?></option>
										<?php }}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 control-group" sty>
								<label class="control-label" for="zone_id">Zone*</label>
								<div class="controls">
									<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);getCenterStudent(this.value)" >
										<option value="">Select Zone</option>
										<?php 
											if(isset($zones) && !empty($zones)){
												foreach($zones as $cdrow){
										?>
											<option value="<?php echo $cdrow->zone_id;?>"><?php echo $cdrow->zone_name;?></option>
										<?php }}?>
									</select>
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Center*</span></label> 
								<div class="controls">
									<select id="center_id" name="center_id" class="form-control" onchange="getCenterStudent(this.value)" >
										<option value="">Select Center</option>
									</select>
								</div>
							</div>
							<!-- <div class="col-md-6 control-group">
								<label class="control-label"><span>Fees Selection Type</span></label>
								<div class="controls">
									<input type="radio" class="form-control feestypeClass feestype" name="feestype" value="No" <?php echo (!empty($details[0]->feestype) &&  ($details[0]->feestype== 'No')) ? "checked" : ""?> checked="checked">Class
									<input type="radio" class="form-control feestypeGroup feestype" name="feestype" value="Yes" <?php echo (!empty($details[0]->feestype) &&  ($details[0]->feestype== 'Yes')) ? "checked" : ""?> >Group
								</div>
								<input type="hidden" name="type" id="type">
							</div> -->
							<!-- <div class="col-md-6 control-group group_show">
								<label class="control-label">Group*</label>
								<div class="controls">
									<select id="group_id" name="group_id" class="form-control" onchange="getGroupStudent(this.value)">
										<option value="">Select Group</option>
										<?php 
											if(isset($groups) && !empty($groups)){
												foreach($groups as $cdrow){
										?>
											<option value="<?php echo $cdrow->group_master_id;?>"><?php echo $cdrow->group_master_name;?></option>
										<?php }}?>
									</select>
								</div>
							</div> -->
							<div class="col-md-6 control-group">
								<label class="control-label">Select Student*</label>
								<div class="controls">
									<select id="stdntId" name="stdntId" class="form-control select2"  onchange="getStudentDetails(this.value);" >
										<option value="">Select Student</option>
									</select>
								</div>
							</div>

							<!-- <div class="row form-group">

								<div class="control-group col-md-6">
									<label class="text_green"> Group<span class="text-danger">*</span> </label>
									<select class="form-control" name="groupid" onchange="getCenterUserGroupBatches(this.value);getCenterFees(this.value)" id="groupid">
						                <option value="" selected="selected" disabled="disabled">Select Group</option>
						                <?php 
											if(isset($groups) && !empty($groups)){
												foreach($groups as $cdrow){
										?>
											<option value="<?php echo $cdrow->group_id;?>"><?php echo $cdrow->group_master_name;?></option>
										<?php }}?>
					              	</select>
								</div>
							</div> -->
						</div>
						<h3>Fill Payment Details</h3>
						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Total Of Fees</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="fees_total_amount" value="" readonly="readonly" id="fees_total_amount">
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Total Of Fees Paid</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="fees_amount_collected" value="" readonly="readonly" id="fees_amount_collected">
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Total Of Pending Fees</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="fees_remaining_amount" value="" readonly="readonly" id="fees_remaining_amount">
								</div>
							</div>
						</div>

						<table class="table table-striped payment_detail_show" style="border: 1px">
							<!-- <thead> -->
								<tr>
									<th>Category Name</th>
				        			<th>Course Name / Group Name</th>
				        			<th>Insatllment Due Date</th>
				        			<th>Installment Fees</th>
				        			<th>Collected Amount</th>
				        			<th>Fee Payment<span class="text-danger">*</span></th>
				        			<th>Remaining Installment Fees</th>
								</tr>
							<!-- </thead> -->
							<tbody class="c_table">
							</tbody>
						</table>

						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Fees Remark*</span></label>
								<div class="controls">
									<textarea class="form-control" name="fees_remark" value="" placeholder="Fees Remark" id="fees_remark"><?php if(!empty($details[0]->fees_remark)){echo $details[0]->fees_remark;}?></textarea>
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Fees Accepted By</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="fees_approved_accepted_by_name" value="<?php echo $login_name;?>" readonly="readonly">
								</div>
							</div>
							<input type="hidden" name="admission_fees_id" id="admission_fees_id">
						</div>

						<div class="row form-group">
						</div>
						
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions">
							<button type="submit" class="btn btn-primary save">Save</button>
							<a href="<?php echo base_url();?>remainingfees" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            	<div class="clearfix"></div>
            </div>
		</div>
	</div>      
	<!-- receipt listing start-->
	<div class="card"> 										
		<h2>Receipts</h2>
		<div class="col-sm-12">
         	<div class="box-content form-horizontal product-filter">            	
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Name/Receipt No.</label>
						<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
					</div>
				</div>
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="box-content">
			<div class="table-responsive scroll-table">
				<table class="dynamicTable display table table-bordered non-bootstrap">
					<thead>
						<tr>
							<th>Date</th>
							<th>Receipt No</th>
							<th>Amount</th>
							<th>Mode</th>
							<th>Student Name</th>
							<!-- <th>Status</th>
							<th data-bSortable="false">Actions</th> -->
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot></tfoot>
				</table>        
			</div>
		</div>											
	</div>
	<!-- receipt listing end-->
</div><!-- end: Content -->			
				
<script>

 
$( document ).ready(function() {
	$('.payment_detail_show').hide()
	$('.group_show').hide()
	$('.feestype').change(function(){
		$("#stdntId").html("<option value=''>Select</option>");
		$("#select2-chosen-1").text("");
		$(".c_table").children().remove();
		if($('.feestypeGroup').is(':checked') == true){
			$('.group_show').show()
			$('.class_show').hide()
		}
		else{
			$('.group_show').hide()
			$('.class_show').show()
			$("#group_id").val("");
			let academic_year_id = $('#academic_year_id').val()
			let zone_id = $('#zone_id').val()
			let center_id = $('#center_id').val()
			getCenterStudent(academic_year_id,zone_id,center_id)
		}
	})
});

$(document).on('keypress','.number_decimal_only',function(e){
    if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
        return false;
    }
})


function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	if(zone_id != ""  || zone_id != undefined)
	{
		$.ajax({
			url:"<?php echo base_url();?>remainingfees/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCenterStudent(academic_year_id,zone_id,center_id)
{
	academic_year_id = $('#academic_year_id').val()
	zone_id = $('#zone_id').val()
	center_id = $('#center_id').val()
	if($('.feestypeGroup').is(':checked') == true){
		group_id = $('#group_id').val()
		getGroupStudent(academic_year_id,zone_id,center_id,group_id)
	}
	else{
	
	//alert("Val: "+val);return false;
		if(zone_id != ""  || zone_id != undefined || center_id != ""  || center_id != undefined || academic_year_id != ""  || academic_year_id != undefined)
		{
			$.ajax({
				url:"<?php echo base_url();?>remainingfees/getCenterStudent",
				data:{zone_id:zone_id, center_id:center_id,academic_year_id:academic_year_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != "")
						{
							$("#stdntId").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#stdntId").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#stdntId").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}
}

function getGroupStudent(academic_year_id,zone_id,center_id,group_id)
{
	academic_year_id = $('#academic_year_id').val()
	zone_id = $('#zone_id').val()
	center_id = $('#center_id').val()
	group_id = $('#group_id').val()
	//alert("Val: "+val);return false;
	if(zone_id != ""  || zone_id != undefined || center_id != ""  || center_id != undefined || academic_year_id != ""  || academic_year_id != undefined || group_id != ""  || group_id != undefined)
	{
		$.ajax({
			url:"<?php echo base_url();?>remainingfees/getGroupStudent",
			data:{zone_id:zone_id, center_id:center_id,academic_year_id:academic_year_id,group_id:group_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#stdntId").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#stdntId").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#stdntId").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getStudentDetails(academic_year_id,zone_id,center_id,student_id)
{
	$(".c_table").html("");
	academic_year_id = $('#academic_year_id').val()
	zone_id = $('#zone_id').val()
	center_id = $('#center_id').val()
	student_id = $('#stdntId').val()
	if($('.feestypeGroup').is(':checked') == true){
		console.log(1)
		$('#type').val('Group')
	}
	else{
		console.log(2)
		$('#type').val('Class')
	}
	let type = $('#type').val()
	console.log(type)
	if(zone_id != ""  || zone_id != undefined || center_id != ""  || center_id != undefined || academic_year_id != ""  || academeic_year_id != undefined || student_id != ""  || student_id != undefined)
	{
		
		$.ajax({
			url:"<?php echo base_url();?>remainingfees/getStudentDetails",
			data:{zone_id:zone_id, center_id:center_id,academic_year_id:academic_year_id,student_id:student_id,type:type},
			dataType: 'json',
			method:'post',
			success: function(res){
				console.log(res)
				if(res['status']=="success"){
					$('.payment_detail_show').show()
					for(let i=0;i<res['option'].length;i++){
						// if(res['option'][i]['fees_type'] == 'Class'){ 
							$('#admission_fees_id').val(res['option'][i]['admission_fees_id'])
							$('#fees_total_amount').val(res['option'][i]['fees_total_amount'])
							$('#fees_amount_collected').val(res['option'][i]['fees_amount_collected'])
							$('#fees_remaining_amount').val(res['option'][i]['fees_remaining_amount'])
							if(res['option'][i]['is_instalment'] == 'Yes'){
								console.log(1)
								$('.feepaymenttype').prop('checked',true)
								$('.feepaymenttypel').prop('disabled',true)
							}
							else{
								console.log(2)
								$('.feepaymenttype').prop('disabled',true)
								$('.feepaymenttypel').prop('checked',true)
							}
							$tr_html = "";
							for(let c=0;c<res['option'][i]['instalment_details'].length;c++){
								$tr_html ='<tr>'+
											'<td>'+res['option'][i]['instalment_details'][c]['categoy_name']+'</td>'+
											'<td>'+((res['option'][i]['instalment_details'][c]['course_name']!="")?res['option'][i]['instalment_details'][c]['course_name']:res['option'][i]['instalment_details'][c]['group_name'])+'</td>'+
											'<td>'+res['option'][i]['instalment_details'][c]['instalment_due_date']+'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control instalment_amount"  id="installment_amnt_'+c+'"name="instalment_amount[]" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_amount']+'">'+
												'</div>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control instalment_amount"  id="collected_amount_'+c+'" name="instalment_collected_amount[]" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_collected_amount']+'">'+
												'</div>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control number_decimal_only installment_collected_amount"  id="id_'+c+'" name="installment_collected_amount[]" value="">'+
												'</div>'+
												'<span class="text-danger" id="amounterror_'+c+'"></span>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control installment_remaining_amount"  id="remaining_id_'+c+'" name="installment_remaining_amount[]" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_remaining_amount']+'">'+
												'</div>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="hidden" class="form-control"  id="admission_fees_instalment_id_'+c+'" name="admission_fees_instalment_id[]" value="'+res['option'][i]['instalment_details'][c]['admission_fees_instalment_id']+'">'+
												'</div>'+
											'</td>'+
										'</tr>';
								$(".c_table").append($tr_html);
								$('#id_'+c).keyup(function(){
									let collected_amount = Number($('#collected_amount_'+c).val())
									let paying_amount = Number($('#id_'+c).val() )
									let amount = Number($('#installment_amnt_'+c).val())
									$('#id_'+c).val(paying_amount)
									let remaining_amount = $('#remaining_id_'+c).val()
									let installment_amnt = $('#installment_amnt_'+c).val()
									let total_minus_amount = parseFloat(collected_amount) + parseFloat(paying_amount)
									if(Number(total_minus_amount) > Number(installment_amnt)){
										// $('#amounterror_'+c).text('Paying Amount grater than insatllment amount')
										let installment_remaining_amount = (amount - collected_amount)
										$('#remaining_id_'+c).val(installment_remaining_amount)
										$('#id_'+c).val('')

									}
									else{
										if(paying_amount == 0){
											$('#id_'+c).val('')
										}
										else{
											console.log(13)
										}
										let installment_remaining_amount = (amount - total_minus_amount)
										$('#remaining_id_'+c).val(installment_remaining_amount.toFixed(2))
										$('#amounterror_'+c).text('')

									}
								})
							}
						/* }else if(res['option'][i]['fees_type'] != 'Group'){ 
							$('#admission_fees_id').val(res['option'][i]['admission_fees_id'])
							$('#fees_total_amount').val(res['option'][i]['fees_total_amount'])
							$('#fees_amount_collected').val(res['option'][i]['fees_amount_collected'])
							$('#fees_remaining_amount').val(res['option'][i]['fees_remaining_amount'])
							
							$('.feepaymenttype').prop('disabled',true)
							$('.feepaymenttypel').prop('checked',true)
							console.log(res['option'][i])
							for(let c=0;c<res['option'][i]['instalment_details'].length;c++){
								$(".c_table").append("<tr>");
								$(".c_table").append('<td>'+res['option'][i]['instalment_details'][c]['group_name']+'</td>');
								$(".c_table").append('<td>'+res['option'][i]['instalment_details'][c]['instalment_due_date']+'</td>');
								$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_amount"  id="installment_amnt_'+c+'"name="instalment_amount[]" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_amount']+'"></div></td>');
								$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_amount"  id="collected_amount_'+c+'" name="instalment_collected_amount[]" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_collected_amount']+'"></div></td>');
								$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control number_decimal_only installment_collected_amount"  id="id_'+c+'" name="installment_collected_amount[]" value=""></div><span class="text-danger" id="amounterror_'+c+'"></span></td>');
								$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control installment_remaining_amount"  id="remaining_id_'+c+'" name="installment_remaining_amount[]" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_remaining_amount']+'"></div></td>');
								$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control"  id="admission_fees_instalment_id_'+c+'" name="admission_fees_instalment_id[]" value="'+res['option'][i]['instalment_details'][c]['admission_fees_instalment_id']+'"></div></td>');
								$(".c_table").append("</tr>");
								$(".c_table").append("</br>");


								$('#id_'+c).keyup(function(){
									let collected_amount = Number($('#collected_amount_'+c).val())
									let paying_amount = Number($('#id_'+c).val() )
									let amount = Number($('#installment_amnt_'+c).val())
									$('#id_'+c).val(paying_amount)
									let remaining_amount = $('#remaining_id_'+c).val()
									let installment_amnt = $('#installment_amnt_'+c).val()
									let total_minus_amount = parseFloat(collected_amount) + parseFloat(paying_amount)
									if(Number(total_minus_amount) > Number(installment_amnt)){
										// $('#amounterror_'+c).text('Paying Amount grater than insatllment amount')
										let installment_remaining_amount = (amount - collected_amount)
										$('#remaining_id_'+c).val(installment_remaining_amount)
										$('#id_'+c).val('')

									}
									else{
										if(paying_amount == 0){
											$('#id_'+c).val('')
										}
										else{
											console.log(13)
										}
										let installment_remaining_amount = (amount - total_minus_amount)
										$('#remaining_id_'+c).val(installment_remaining_amount.toFixed(2))
										$('#amounterror_'+c).text('')

									}
								})
							}
						} */
					}
				}else{
					$('.payment_detail_show').hide()
				}
			}
		});
	}
}
var vRules = {
	academic_year_id : {required:true},
	zone_id :  {required:true},
	center_id :  {required:true},
	group_id :  {required:true},
	stdntId:{required:true},
	fees_remark:{required:true}
	
};

var vMessages = {
	academic_year_id:{required:"Please select academic year."},
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	group_id:{required:"Please select group."},
	stdntId:{required:"Please select student."},
	fees_remark:{required:"Please enter fees remark."}
};

$("#feesInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		$('#loadingmessage').show();
		var act = "<?php echo base_url();?>remainingfees/submitForm";
		$("#feesInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				let admission_fees_id = res['admission_fees_id']
				$.ajax({
					url:"<?php echo base_url();?>remainingfees/getPayemtDetails",
					data:{admission_fees_id:admission_fees_id},
					dataType: 'json',
					method:'post',
					success: function(data)
					{
						if(data['status_code'] == 200){
							
							window.open(data['body'], '_blank');
							$('#loadingmessage').hide();
							window.location = "<?php echo base_url();?>home";
						}
						else{
							$('#loadingmessage').hide();
						}
					}
				});
			}
		});
	}
});


document.title = "Fees Payment Process";

 
</script>					
