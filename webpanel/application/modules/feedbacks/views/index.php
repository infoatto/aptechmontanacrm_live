<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($roles);exit;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Feedbacks</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>feedbacks">Feedbacks</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
		<?php 
					if ($this->privilegeduser->hasPrivilege("FeedbackExport")) {
				?>
			<a style="margin-left:10px;" class="btn btn-primary icon-btn export_contactus"><i class="fa fa-plus"></i>Export</a>
		<?php }?>	
         </div>     
		
        <div class="col-sm-12" style="clear: both">
			<form name="filter_form" method="post" id="filter_form">
				<div class="box-content form-horizontal product-filter">            	
				
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Zone</label>
							<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Center</label>
							<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
						</div>
					</div>

					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Email ID" class="control-label">Name</label>
							<input id="sSearch_3" name="sSearch_3" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Email ID" class="control-label">Email ID</label>
							<input id="sSearch_4" name="sSearch_4" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12" style="clear: left;float: left;">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Mobile" class="control-label">Contact Number</label>
							<input id="sSearch_5" name="sSearch_5" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="control-group clearFilter" style="clear: right;float:left;">
						<div class="controls">
							<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
						</div>
					</div>
					
				</div>
			</form>
        </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                   <table class="dynamicTable display table table-bordered non-bootstrap">
                        <thead>
                          <tr>
							<th>Zone</th>
							<th>Center</th>
							<th>Name</th>
							<th>Email ID</th>
							<th>Contact Number</th>
							<th>Message</th>
							<th>Contacted On</th>
							<!--<th>Action</th>-->
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	
	$( document ).ready(function() {
		clearSearchFilters();
		$(".datepicker").datepicker({
			//format: 'YYYY-MM-DD'
		});
	});	

	
	// $("#sSearch_3").datepicker({format: 'yyyy-dd-mm '});
	function changestatus(id){
		$.ajax({
			url: "<?php echo base_url(); ?>feedbacks/changestatus/"+id,
			dataType:'json',
			beforeSend:function(){
			},
			success: function(res){
				if(res['success']){
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feedbacks";
					},2000);

				}
				else{
					if(res['msg']=='redirect'){
						displayMsg("error",'Session has expired.');
						setTimeout(function(){
							window.location = "<?php echo base_url();?>login";
						},2000)
					}
					else{
						displayMsg("error",res['msg']);
						return false;
					}
				}
			},
			error: function(){
				displayMsg("error",'Error Occured,Please reload the page.');
				return false;
			}
		});
	}
	
	$(".export_contactus").on("click", function()
	{
		$('.export_contactus').attr("disabled","disabled");
		var act = "<?php  echo base_url();?>feedbacks/export";
		$("#filter_form").attr("action",act);
		$("#filter_form").submit();
	});
	
	$("#filter_form").on("submit", function()
	{
		$('.export_contactus').removeAttr("disabled");
		
		$('.export_contactus_sizes').removeAttr("disabled");
		
		setTimeout("location.reload(true);",1000);
	});
	
	<?php
	if(isset($_SESSION['contactus_export_success']))
	{
		?>
		displayMsg("<?php echo $_SESSION['contactus_export_success']; ?>", "<?php echo $_SESSION['contactus_export_msg']; ?>");
		<?php
		unset($_SESSION['contactus_export_success']);
		unset($_SESSION['contactus_export_msg']);
	}
	?>
	
	document.title = "Feedbacks";
</script>