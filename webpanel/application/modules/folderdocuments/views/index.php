<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($roles);exit;

if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	//echo $_GET['text'];exit;
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
//echo $record_id;exit;
?>


<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Folder Documents</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>documentfolders">Document Folder</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
            	
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12" style="clear: both">
         	<div class="box-content form-horizontal product-filter">            	
            	
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Name</label>
						<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
				
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" callfunction="<?php echo base_url();?>folderdocuments/fetch/<?php echo $record_id;?>">
                        <thead>
                          <tr>
							
							<th>Name</th>
							<th>Document/Video Title</th>
							<th data-bSortable="false">Document / Video</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
	    
		<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
			   <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
			   <div class="modal-content">
				  <div class="modal-body">
					 <img src="" alt="" />
				  </div>
			   </div>
		    </div>
		</div>
	    
	    
    </div>
</div><!-- end: Content -->
			
<script>

function openwindow(imgname){
	//alert("here...");
	window.open("<?php echo FRONT_URL; ?>/images/teacher_album_images/"+imgname, "Change Product Approval", "width=500,height=500");
}

$( document ).ready(function() {
	//alert("hhh");
	//$(".colorbbb").colorbox({rel:'group1'});
	
	var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
		//alert("src: "+src);
		
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    alert("src: "+src);
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
	
	
});	
	function deleteData(id,status)
	{
		var sta="";
		var sta1=" ";
		
		if(status=='Active')
		{
		sta1="dis-approve";
		sta="In-active";
			
		}
		else{
			sta1="approve";
			sta="Active";
		}
		
		
		
    	var r=confirm("Are you sure to " +sta1);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delRecord/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	
	
	function deleteData1(id,status)
	{
		var sta="";
		var sta1=" ";
		
		if(status=='Active')
		{
		
			sta="In-active";
			
		}
		else{
			
			sta="Active";
		}
		
		
		
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord12/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	
	document.title = "Folder Documents";
</script>