<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Payment Process</h1>            
          </div>
          <!-- <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>camera">Camera</a></li>
            </ul>
          </div> -->
    </div>
    <div class="card">    
    <div id='loadingmessage' style='display:none'>
        <img src='<?php echo FRONT_URL; ?>/images/loading.gif?>'/>
    </div>   
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($student_id)){echo $student_id;}?>" />
						<?php if(isset($paymentinfo['installmentcomponent'])){?>
							<table class="table table-striped" style="border: 1px">
								<thead>
									<tr>
										<td>Payment Due Date</td>
					        			<td>Installment Amount</td>
					        			<td>Paying Amount<span class="text-danger">*</span></td>
					        			<td>Remaining Amount</td>
									</tr>
								</thead>
								<tbody>
									<?php for($i=0;$i<sizeof($paymentinfo['installmentcomponent'][0]);$i++){?>
										<tr>
											<td><?php echo $paymentinfo['installmentcomponent'][0][$i]['instalment_due_date']?></td>
											<td><input type="text" class="form-control instalment_amount"  name="instalment_amount" readonly="readonly" value="<?php echo $paymentinfo['installmentcomponent'][0][$i]['instalment_amount']?>"></td>
											<td><input type="text" class="form-control instalment_collected_amount number_decimal_only" id="instalmentcollectedamount_<?php echo $i;?>" name="instalment_collected_amount[]"></td>
											<td><input type="text" class="form-control instalment_remaining_amount"  id="instalmentremainingamount_<?php echo $i;?>" name="instalment_remaining_amount[]" readonly="readonly"></td>

											<input type="hidden" name="admission_fees_instalment_id[]" value="<?php echo $paymentinfo['installmentcomponent'][0][$i]['admission_fees_instalment_id']?>">

											<input type="hidden" name="admission_fees_id" value="<?php echo $paymentinfo['installment'][0]['admission_fees_id']?>">
										<tr>
									<?php }?>
								</tbody>
							</table>
						<?php }?>

						<?php if(isset($paymentinfo['lumpsum'])){?>
							<table class="table table-striped" style="border: 1px">
								<thead>
									<tr>
										<th>Fees Total Amount</th>
					        			<th>Discount Amount</th>
					        			<th>GST Amount</th>
					        			<th>Total Amount</th>
					        			<th>Collected Amount</th>
									</tr>
								</thead>
								<tbody>
									<?php for($i=0;$i<sizeof($paymentinfo['lumpsum']);$i++){?>
										<tr>
											<td><?php echo $paymentinfo['lumpsum'][$i]['fees_total_amount']?></td>
											<td><?php echo $paymentinfo['lumpsum'][$i]['discount_amount']?></td>
											<td><?php echo $paymentinfo['lumpsum'][$i]['gst_amount']?></td>
											<td><?php echo $paymentinfo['lumpsum'][$i]['total_amount']?></td>
											<td><?php echo $paymentinfo['lumpsum'][$i]['fees_amount_collected']?></td>

											<input type="hidden" name="fees_id[]" value="<?php echo $paymentinfo['lumpsum'][$i]['fees_id']?>">
										<tr>
									<?php }?>
								</tbody>
							</table>
						<?php }?>

						<h3>Fill Payment Details</h3>

						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Payment Mode</span></label>
								<div class="controls">
									<input type="radio" class="form-control payment_mode" name="payment_mode" value="Cheque" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cheque')) ? "checked" : ""?>>Cheque
									<input type="radio" class="form-control payment_mode" name="payment_mode" value="Cash" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cash')) ? "checked" : ""?> checked="checked" >Cash
									<input type="radio" class="form-control payment_mode" name="payment_mode" value="Netbanking" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Netbanking')) ? "checked" : ""?> >Netbanking
								</div>
							</div>
							<div class="col-md-6 control-group both">
								<label class="control-label"><span>Bank Name*</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="bank_name" value="<?php if(!empty($details[0]->bank_name)){echo $details[0]->bank_name;}?>" id="bank_name">
								</div>
							</div>
						</div>

						<div class="row form-group cheque">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Cheque No*</span></label>
								<div class="controls">
									<input type="number" class="form-control" name="cheque_no" value="<?php if(!empty($details[0]->cheque_no)){echo $details[0]->cheque_no;}?>" id="cheque_no" min="0">
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Cheque Date*</span></label>
								<div class="controls">
									<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="cheque_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
								</div>
							</div>
						</div>

						<div class="row form-group transaction">
							<div class="col-md-6 control-group cheque transaction">
								<label class="control-label"><span>Transaction No*</span></label>
								<div class="controls">
									<input type="number" class="form-control" name="transaction_id" value="<?php if(!empty($details[0]->transaction_id)){echo $details[0]->transaction_id;}?>" id="transaction_id" min="0">
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Transaction Date*</span></label>
								<div class="controls">
									<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="transaction_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
								</div>
							</div>
						</div>

						
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions">
							<button type="submit" class="btn btn-primary save">Save</button>
							<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$('.cheque').hide();
	$('.transaction').hide();
	$('.both').hide();
});
$(document).on('keypress','.number_decimal_only',function(e){
    if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
        return false;
    }
})

$('.instalment_collected_amount').keyup(function(){
	let collected_amount = $(this).val()
	let installment_amount = $('.instalment_amount').val()

	let installmentid = $(this).attr('id').split('_')
	let remaining_amount_input = $(this).parent().parent().next().find('.instalment_remaining_amount')
	if(parseInt(collected_amount) > parseInt(installment_amount)){
		remaining_amount_input.val(installment_amount);
	}
	else{			
		let remaining_amount = ($('.instalment_amount').val() - collected_amount).toFixed(2)
		$('#instalmentremainingamount_'+installmentid[1]).val(remaining_amount);
	}
})
$('.payment_mode').change(function(){
	var payment_mode = $(this).val();
	if(payment_mode == 'Cheque'){
		$('.cheque').show();
		$('.transaction').hide();
		$('.both').show();
	}
	else if(payment_mode == 'Netbanking'){
		$('.cheque').hide();
		$('.transaction').show();
		$('.both').show();
	}
	else{
		$('.cheque').hide();
		$('.transaction').hide();
		$('.both').hide();
	}
})
$(".datepicker").datetimepicker({
	format: 'DD-MM-YYYY',

});


$("#feesInfoform-validate").validate({
	submitHandler: function(form) 
	{
		$('#loadingmessage').show();
		var act = "<?php echo base_url();?>admission/admissionGroupFees";
		$("#feesInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				console.log(res)
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
						setTimeout(function(){
							let student_id = res['student_id']
							$.ajax({
								url:"<?php echo base_url();?>admission/getPayemtDetails1",
								data:{student_id:student_id},
								dataType: 'json',
								method:'post',
								success: function(data)
								{
									if(data['status_code'] == 200){
										let student_id = btoa(data['body']);
										$('#loadingmessage').hide();
										window.location = "<?php echo base_url();?>admission/viewAllInvoice?text="+student_id;
									}
									else{
										$('#loadingmessage').hide();
									}
								}
							});
						},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "Add - PaymentProcess";

 
</script>					
