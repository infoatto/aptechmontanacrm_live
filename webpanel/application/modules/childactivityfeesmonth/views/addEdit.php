<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Group Payment Frequency Master</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>childactivityfeesmonth">Group Payment Frequency Master</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="month_id" name="month_id" value="<?php if(!empty($details[0]->month_id)){echo $details[0]->month_id;}?>" />
						
						<div class="control-group form-group">
							<label class="control-label"><span>Month/Week/Frequency*</span></label>
							<div class="controls">
								<input type="text" class="form-control required " placeholder="Enter Month Like 1,2,..." name="month" value="<?php if(!empty($details[0]->month)){echo $details[0]->month;}?>"  maxlength="50">
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label"><span>Days*</span></label>
							<div class="controls">
								<input type="number" class="form-control required " placeholder="Enter Days " name="days" value="<?php if(!empty($details[0]->days)){echo $details[0]->days;}?>"  >
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label"><span>Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required " placeholder="Enter Name " name="name" value="<?php if(!empty($details[0]->name)){echo $details[0]->name;}?>"  >
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label" for="status">Status*</label>
							<div class="controls">
								<select name="status" id="status" class="form-control">
									<option value="Active" <?php if(!empty($details[0]->status) && $details[0]->status == "Active"){?> selected <?php }?>>Active</option>
									<option value="In-active" <?php if(!empty($details[0]->status) && $details[0]->status == "In-active"){?> selected <?php }?>>In-active</option>
								</select>
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>childactivityfeesmonth" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>
$('document').ready(function(){
	$(".number_only").keypress(function (e) {
    	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      	return false;
    	}
  	});
})
var vRules = {
	month:{required:true},
	days:{required:true},
	name:{required:true}	
};
var vMessages = {
	month:{required:"Please enter Month/Week/Frequency."},
	days:{required:"Please enter days."},
	name:{required:"Please enter name of Month/Week/Frequency."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>childactivityfeesmonth/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>childactivityfeesmonth";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Child Activity Fees Month";

 
</script>					
