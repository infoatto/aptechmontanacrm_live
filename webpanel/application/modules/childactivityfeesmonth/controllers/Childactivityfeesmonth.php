<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Childactivityfeesmonth extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('childactivityfeesmonthmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('childactivityfeesmonth/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['details'] = $this->childactivityfeesmonthmodel->getFormdata($record_id);
			// print_r($result);exit();
			$this->load->view('template/header.php');
			$this->load->view('childactivityfeesmonth/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
		


	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			$condition = "month_name='".$_POST['month'].$_POST['name']."' ";
			if(isset($_POST['month_id']) && $_POST['month_id'] > 0)
			{
				$condition .= " &&  month_id != ".$_POST['month_id'];
			}
			
			$check_name = $this->childactivityfeesmonthmodel->getdata("tbl_childactivity_fees_month",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			
			if (!empty($_POST['month_id'])) {
				$data_array = array();			
				$month_id = $_POST['month_id'];
		 		$data_array['month_name'] = (!empty($_POST['month']) && !empty($_POST['name'])) ? $_POST['month'].$_POST['name'] : '';
		 		$data_array['month'] = (!empty($_POST['month'])) ? $_POST['month']: 0;
		 		$data_array['name'] = (!empty($_POST['name'])) ? $_POST['name']: 0;
		 		$data_array['days'] = (!empty($_POST['days'])) ? $_POST['days']: 0;
				$data_array['status'] = (!empty($_POST['status'])) ? $_POST['status'] : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->childactivityfeesmonthmodel->updateRecord('tbl_childactivity_fees_month', $data_array,'month_id',$month_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['month_name'] = (!empty($_POST['month']) && !empty($_POST['name'])) ? $_POST['month'].$_POST['name'] : '';
				$data_array['month'] = (!empty($_POST['month'])) ? $_POST['month']: 0;
				$data_array['name'] = (!empty($_POST['name'])) ? $_POST['name']: 0;
				$data_array['days'] = (!empty($_POST['days'])) ? $_POST['days']: 0;				
				$data_array['status'] = (!empty($_POST['status'])) ? $_POST['status'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->childactivityfeesmonthmodel->insertData('tbl_childactivity_fees_month', $data_array, '1');
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	// /*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->childactivityfeesmonthmodel->getRecords($_GET);

		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->month);
				array_push($temp, $get_result['query_result'][$i]->days);
				array_push($temp, $get_result['query_result'][$i]->name);
				
				$actionCol21="";
				$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->month_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				
				$actionCol1 = "";
				$actionCol1.= '<a href="childactivityfeesmonth/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->month_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord12()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->childactivityfeesmonthmodel->delrecord12("tbl_childactivity_fees_month","month_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
}

?>
