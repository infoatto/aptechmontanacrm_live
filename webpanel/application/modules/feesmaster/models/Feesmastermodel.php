<?PHP
class Feesmastermodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	 
	function getRecords($get=null){
		$table = "tbl_fees_master";
		$table_id = 'f.fees_id';
		$default_sort_column = 'f.fees_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('a.academic_year_master_name','f.fees_selection_type','fe.fees_level_name','f.fees_name','c.categoy_name','fd.status');
		$searchArray = array('a.academic_year_master_name','f.fees_selection_type','fe.fees_level_name','f.fees_name','c.categoy_name','fd.status');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<7;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		
		$this -> db -> select('f.*,academic_year_master_name,fe.fees_level_name,c.categoy_name');
		$this -> db -> from('tbl_fees_master as f');
		$this -> db -> join('tbl_academic_year_master as a', 'f.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_fees_level_master as fe', 'f.fees_level_id  = fe.fees_level_id', 'left');
		$this -> db -> join('tbl_categories as c', 'f.category_id  = c.category_id', 'left');
		// $this -> db -> join('tbl_fees_component_master as fc', 'fd.fees_component_id  = fc.fees_component_master_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		// print_r($query);
		// exit();
		$this -> db -> select('f.*,academic_year_master_name,fe.fees_level_name,c.categoy_name');
		$this -> db -> from('tbl_fees_master as f');
		$this -> db -> join('tbl_academic_year_master as a', 'f.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_fees_level_master as fe', 'f.fees_level_id  = fe.fees_level_id', 'left');
		$this -> db -> join('tbl_categories as c', 'f.category_id  = c.category_id', 'left');
		// $this -> db -> join('tbl_fees_component_data as fd', 'f.fees_id  = fd.fees_master_id', 'left');
		// $this -> db -> join('tbl_fees_component_master as fc', 'fd.fees_component_id  = fc.fees_component_master_id', 'left');


		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		// echo "<pre>";
		// print_r($query->result());exit();
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			return array("totalRecords"=>0);
		}
		
		exit;
	}
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getFormdata($ID){

		$this -> db -> select('*');
		$this -> db -> from('tbl_fees_master');
		$this->db->where('fees_id', $ID);
	
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}

	function getFormdata1($ID){
		
		$this -> db -> select('*');
		$this -> db -> from('tbl_fees_component_data');
		$this->db->where('fees_master_id', $ID);
	
		$query = $this -> db -> get();
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions1($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where_in($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
		
		$query = $this -> db -> get();
		// print_r($query);
		// exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getdata($table,$condition = '1=1'){
		$sql = $this->db->query("Select * from $table where $condition");

        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	
	function deletedata($tableName, $column, $value){
		$this->db->where("$column", $value);
		$this->db->delete($tableName);
		return true;
	}

	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function enum_select($table, $field){
		$query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
		$row = $this->db->query(" SHOW COLUMNS FROM `$table` LIKE '$field' ")->row()->Type;
		$regex = "/'(.*?)'/";
		preg_match_all( $regex , $row, $enum_array );
		$enum_fields = $enum_array[1];
		return( $enum_fields );
	}
	

	function getMonths($table, $fields, $condition = '1=1'){
		$sql = $this->db->query("Select $fields from $table where $condition order by cast(month_name as unsigned)");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getAllFrequency(){
		$condition = array("cfm.status"=>"Active","m.status"=>"Active");
		$this -> db -> select('m.*,cfm.*');
		$this -> db -> from('tbl_fees_discount_master as m');
		$this -> db -> join("tbl_childactivity_fees_month as cfm", "cfm.month_id  = m.month_id", "left");
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
}
?>
