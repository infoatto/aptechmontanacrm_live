<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Camera</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>camera">Camera</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="camera_id" name="camera_id" value="<?php if(!empty($details[0]->camera_id)){echo $details[0]->camera_id;}?>" />
					
						<div class="control-group form-group">
							<label class="control-label"><span>Camera Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Name" id="camera_name" name="camera_name" value="<?php if(!empty($details[0]->camera_name)){echo $details[0]->camera_name;}?>" maxlength="100" >
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Camera Url*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Url" id="url" name="url" value="<?php if(!empty($details[0]->url)){echo $details[0]->url;}?>" maxlength="100" >
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Start Time*</span></label>
							<div class="controls">
								<input type="text" class="form-control required bootstrap-timepicker timepicker" placeholder="Select start time" id="start_time" name="start_time" value="<?php if(!empty($details[0]->start_time)){echo date("h:i:s a", strtotime($details[0]->start_time));}?>" >
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>End Time*</span></label>
							<div class="controls">
								<input type="text" class="form-control required bootstrap-timepicker timepicker" placeholder="Select start time" id="end_time" name="end_time" value="<?php if(!empty($details[0]->end_time)){echo date("h:i:s a", strtotime($details[0]->end_time));}?>" >
							</div>
						</div>
						
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>camera" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$(".timepicker").timepicker({});
});


var vRules = {
	camera_name:{required:true},
	url:{required:true}
	
};
var vMessages = {
	camera_name:{required:"Please enter camera name."},
	url:{required:"Please enter url."}

	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>camera/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>camera";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Camera";

 
</script>					
