<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Permission extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('permissionmodel','',TRUE); 
   if(!$this->privilegeduser->hasPrivilege("PermissionList")){
     redirect('home');
   }
 }
 
 function index()
 {
   if(!empty($_SESSION["webadmin"]))
   {
		$this->load->view('template/header.php');
		$this->load->view('permission/index');
		$this->load->view('template/footer.php');
   }
   else
   {
		//If no session, redirect to login page
		redirect('login', 'refresh');
   }
 }
 
 function fetch(){
	//print_r($_GET);
	$get_result = $this->permissionmodel->getRecords($_GET);
	
	//print_r($get_result['query_result']);
	//echo "Count: ".$get_result['totalRecords'];
	
	$result = array();
	$result["sEcho"]= $_GET['sEcho'];
	
	$result["iTotalRecords"] = $get_result['totalRecords'];									//	iTotalRecords get no of total recors
	$result["iTotalDisplayRecords"]= $get_result['totalRecords'];							//  iTotalDisplayRecords for display the no of records in data table.
	
	$items = array();
	if(!empty($get_result['query_result'])){
		for($i=0;$i<sizeof($get_result['query_result']);$i++){
			$temp = array();
			array_push($temp, $get_result['query_result'][$i]->perm_desc );
			
			$actionCol = "";
			if($this->privilegeduser->hasPrivilege("PermissionAddEdit")){
				$actionCol .='<a href="permission/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->perm_id), '+/', '-_'), '=').'" title="Edit"><i class="fa fa-edit"></i></a>';
			}
			//if($this->privilegeduser->hasPrivilege("PermissionDelete")){
				//$actionCol .='&nbsp;&nbsp;<a href="javascript:void(0);" onclick="deleteData(\''.$get_result['query_result'][$i]->perm_id .'\');" title="Delete"><i class="icon-remove-sign"></i></a>';
			//}
			
	//		array_push($temp, $actionCol);
			array_push($items, $temp);
		}
	}	
	
	$result["aaData"] = $items;
	echo json_encode($result);
	exit;
	
 }
 
function addEdit($id=NULL)
 {
   if(!empty($_SESSION["webadmin"]))
	{
     
		$record_id = "";
		//print_r($_GET);
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$record_id = $url_prams['id'];
		}
		
		//echo $user_id;
		$result = "";
		$result['users'] = $this->permissionmodel->getFormdata($record_id);
		
		$this->load->view('template/header.php');
		$this->load->view('permission/addEdit',$result);
		$this->load->view('template/footer.php');
	}
	else
	{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
 }
 
	function submitForm(){
		/*print_r($_POST);
		exit;*/
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			
			if(!empty($_POST['perm_id'])){
				//update
				/*echo "in update";
				print_r($_POST);
				exit;*/
				
				$data = array();
				$data['perm_desc'] = $_POST['perm_desc'];				
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->permissionmodel->updateRecord($data,$_POST['perm_id']);
					
				if(!empty($result)){
					echo json_encode(array('success'=>'1','msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>'0','msg'=>'Problem in data update.'));
					exit;
				}
				
				
			}else{
				//add
				/*echo "in add";
				print_r($_POST);
				exit;*/
				
				//print_r($_SESSION["webadmin"]);
				//echo $_SESSION["webadmin"][0]->user_id;
				//exit;
				
				$data = array();
				$data['perm_desc'] = $_POST['perm_desc'];
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->permissionmodel->insertData('permissions',$data,'1');
				
				if(!empty($result)){
					echo json_encode(array("success"=>"1",'msg'=>'Record inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array("success"=>"0",'msg'=>'Problem in data insert.'));
					exit;
				}
				
			}
			 
			
		}else{
			return false;
		}
	}
 
//For Delete

function delRecord($id)
 {
	 $appdResult = $this->permissionmodel->delrecord("permissions","perm_id",$id);
	 
	if($appdResult)
	{
		echo "1";
	}
	else
	{
		echo "2";	
			 
	}	
 }	
	
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('auth/login', 'refresh');
 }

}

?>