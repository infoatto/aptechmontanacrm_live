<?php 
//session_start();

//print_r($_SESSION["webadmin"]);

//print_r($users);
//echo "Name: ".$users[0]->first_name;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
    <div class="page-title">
      <div>
        <h1>Add Roles</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Add Roles</a></li>
        </ul>
      </div>
    </div>
    <div class="card">
    	<div class="card-body">
        	<div class="box-content">
            	
                	<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                    	<input type="hidden" id="role_id" name="role_id" value="<?php if(!empty($users[0]->role_id)){echo $users[0]->role_id;}?>" />
                        <div class="control-group">
                            <label class="control-label" for="role_name">Role Name*</label>
                            <div class="controls">
                                <input class="input-xlarge form-control" id="role_name" name="role_name" type="text" value="<?php if(!empty($users[0]->role_name)){echo $users[0]->role_name;}?>">
                            </div>
                        </div>
               
                      
                        <div class="control-group">
                            <div class="">
                                <fieldset id="fieldUserRoles">
                                    <div class="page-header">
                                        <h4>User Roles</h4>
                                    </div>
                                    <?php
                                        $selectedCheckArray = array();
                                        if(!empty($permissions) && isset($permissions)){
                                            for($i=0;$i<sizeof($permissions);$i++){
                                                if(!empty($selpermissions) && isset($selpermissions)){
                                                    for($j=0;$j<sizeof($selpermissions);$j++){
                                                        $selectedCheckArray[] = $selpermissions[$j]->perm_id;
                                                    }
                                                }
                                    ?>
                                       <span style="float:left;width:300px;overflow-wrap: break-word;" class="width33">
                                            <input <?php if(in_array($permissions[$i]->perm_id,$selectedCheckArray)){ echo "checked=checked";} ?> class="chk" type="checkbox" id="<?php echo $permissions[$i]->perm_id; ?>" name="perm_id[]" value="<?php echo $permissions[$i]->perm_id; ?>"/>
								    <?php echo $permissions[$i]->perm_desc; ?>
                                        </span>
                                    <?php }}?>
                                    <div class="clearfix" style="height: 10px; width: 100%; float: left; display: inline;">&nbsp;</div>
                                    <button id="chkAll" style="float:left;" class="btn btn-info marginR10 checkall" type="button" >Check All</button>
                                    <div class="clearfix" style="height: 10px; width: 100%; float: left; display: inline;">&nbsp;</div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?php echo base_url();?>roles"><button class="btn" type="button">Cancel</button></a>
                        </div>
                    </form>
                </div>
            	<div class="clearfix"></div>
            </div>
        </div>
    </div>
<!-- end: Content -->
			
<script>

var clicked = false;
$(".checkall").on("click", function() {
  $(".chk").prop("checked", !clicked);
  clicked = !clicked;
});

$( document ).ready(function() {
});

var vRules = {
	role_name:{required:true}
	
};
var vMessages = {
	role_name:{required:"Please enter role name."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>roles/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>roles";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

/*
function checkUncheck(obj)
{
    if($(obj).text() == "Check All")
    {
		$(".width33").each(function(){
			$(this).find("span").addClass("checked");
			$(this).find("input").attr("checked","checked");
		});
		$(".both").each(function(){
			$(this).find("span").addClass("checked");
			$(this).find("input").attr("checked","checked");
		});
        $(obj).text("Uncheck All");
    }
    else
    {
    	$(".width33").each(function(){
			$(this).find("span").removeClass("checked");
			$(this).find("input").removeAttr("checked");
		});
		$(".both").each(function(){
			$(this).find("span").removeClass("checked");
			$(this).find("input").removeAttr("checked");
		});
    	$(obj).text("Check All");
    }
}
*/

document.title = "Add/Edit Roles";
</script>