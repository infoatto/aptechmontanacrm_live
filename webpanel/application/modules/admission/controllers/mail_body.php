$message_body ='';
					$message_body  = '<html><body>';	
					$message_body .= '<div style="width:750px;padding:0 30px;">';
										$message_body .= '<div>
											<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Below is the email and attachment which has to go to each parent at the time of BC generation. Apart from this the BC must be added in pdf format.</b></p><br><br>

											<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Dear Parent,</b></p><br>

											<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Welcome to Aptech Montana International Preschool!

											Your child’s journey of learning and fun begins with us and as your child embarks on this journey, here are few essential guidelines that will help you.</p><br>

											<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Let’s begin!</p>
										
										<br>';
										
										$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">PROGRAM AND PAYMENT INFORMATION</h4>';
										$message_body .='<table width="30%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Enrollment number</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">000110</td>
											</tr>
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Parent Manual</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	
												PM_1087702</td>
											</tr>
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Program name</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	
												Kindergarten 1</td>
											</tr>
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Program Fee</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">40000.00</td>
											</tr>
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Centre Name</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Madhapur</td>
											</tr>';
											$message_body .='</table><br>';

											$message_body .='<table width="30%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">E-Mail ID</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">priyankasarkar178@gmail.com</td>
											</tr>
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Mobile Number</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	
												9064253165</td>
											</tr>
											<tr>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Total Amount Received till now towards Course</th>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	
												40000.00</td>
											</tr>';
											$message_body .='</table></div>';
											$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">IMPORTANT COMMUNICATION:</h4>';

											$message_body .= '
										<p style="font-size:13px;margin:4px 0;">1)   A copy of the Booking Confirmation is attached with this mail. It contains the details of the program in which your child is enrolled, program fees, payment details and instalment payment schedule. You are requested to please check the details mentioned in the Booking Confirmation carefully.</p>
										<p style="font-size:13px;margin:4px 0;">2) We request parents to pay the school fees either by cheque, DD or by net banking. Payment to be done in favour of “The Montana International Preschool Pvt Ltd ”. </p>
										<p style="font-size:13px;margin:4px 0;">3) Parents can also pay the fees using payment gateway attached to the mobile app.</p>
										<p style="font-size:13px;margin:4px 0;">4) Please insist on computerized receipt on payment of fees. Aptech Montana International Preschool is not responsible/ nor recognizes any manual receipt issued by the school.</p>
										<p style="font-size:13px;margin:4px 0;">5) On completion of the Academic Year , program completion certificate will be issued by Aptech Montana International Preschool. Any other certificate issued is invalid.</p>
										<p style="font-size:13px;margin:4px 0;"> 6)In case of any discrepancies, reach out to our centre or our customer care <a href="mailto:feedback@montanapreschool.com" style="color:#000;text-decoration:none;">feedback@montanapreschool.com</a>  with your centre name, enrollment number, name and contact details.</p>';

										$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Montana Parent Connect Mobile App</h4><br>
										<p style="font-size:13px;margin:4px 0;">You are requested to kindly download the ‘Montana Parent Connect App’ from the Google Play Store or App Store!</p><br>
										
										<p style="font-size:13px;margin:4px 0;">The Montana Parent Connect App comes with some amazing features like LIVE CCTV feed, child attendance history, updates on school activities through notifications, picture gallery to view photos and videos of events, newsletters etc.</p><br>
										
										<p style="font-size:13px;margin:4px 0;"><b>Username:</b> 6 digit enrollment number</p>
										<p style="font-size:13px;margin:4px 0;"><b>Default Password:</b> password </p><br>

										<p style="font-size:13px;margin:4px 0;">On securing admission of your at Aptech Montana International Preschool, you will receive a child kit as per the program enrolled, which contains the following items.</p><br>';

										$message_body .='<table width="40%" cellpadding="0" cellspacing="0" border="2" style="border:1px solid #000;width:700px;margin:0 auto;>';
										$message_body .='<tr>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000"></th>
										
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Sr. No.</th>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">List of items</th>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Quantity</th>
												<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Program</th>
												
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	T shirt (Common)</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
												Short/Skirt</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	3</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Cap</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, Child Care, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	4</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Apron</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, Child Care, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	5</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Shoe</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1 pair</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	6</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Bag</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	7</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Socks</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2 pair</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
												Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	8</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	School Diary</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	9</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Assessment Card</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	10</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Bag ID Card</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	11</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Family ID Card</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	12</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Student ID Card</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	13</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
												ID holder with Lanyard</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	14</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	PN/NU Workbooks</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">1 set</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery & Nursery</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	15</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	K1/K2 book kit</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
												1 set</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">K1 & K2</td>
											</tr>
											<tr>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	16</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Child Care Diary</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
												<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Child Care</td>
											</tr>';
											$message_body .='</table>';

											
											$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Parent Manual</h4>
										<p style="font-size:13px;margin:4px 0;">Kindly acknowledge the receipt of the parent manual and provide your confirmation to adhere to it.
										Sign the Booking Confirmation document provided to you by your school. In case you haven’t received it,
										do request your centre to provide it to you at the earliest.</p><br>

										<p style="font-size:13px;margin:4px 0;">Attached herewith is the copy of the Parent Manual for your ready reference.</p><br>

										<p style="font-size:13px;margin:4px 0;"><b>Regards</b></p>
										<p style="font-size:13px;margin:4px 0;"><b>Team Aptech Montana International Preschool</b></p>';

										$message_body .=  '</div>';
										
										$message_body .=  '</body></html>';