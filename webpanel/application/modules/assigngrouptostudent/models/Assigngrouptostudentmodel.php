<?PHP
class Assigngrouptostudentmodel extends CI_Model
{
    
    function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	
    function getUserZones($condition){
	   
		$this -> db -> select('z.zone_id, z.zone_name');
		$this -> db -> from('tbl_user_zones as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> where("($condition)");
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
    }
    
    function getRecords($get=null){
		$table = "tbl_student_master";
		$table_id = 'sd.student_id';
		$default_sort_column = 'sd.student_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('concat(sd.student_first_name," ",sd.student_last_name)','gm.group_master_name');
		$searchArray = array('z.zone_id','ct.center_id','cr.course_id','concat(sd.student_first_name," ",sd.student_last_name)');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<4;$i++){
			if($i != 3){
				if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!=''){
					$condition .= " AND $searchArray[$i] = ".$_GET['sSearch_'.$i]." ";
				}
			}else{
			   	if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!=''){
    				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%' || sd.enrollment_no like '%".$_GET['sSearch_'.$i]."%' ";
    			}
			}		
		}
		
// 		echo "condition ".$condition;exit;
		
	    $this -> db -> select('concat(sd.student_first_name," ",sd.student_last_name) as student_name,GROUP_CONCAT(gm.group_master_name) as group_master_name,sg.student_id,sg.group_id,GROUP_CONCAT(tbnm.batch_name) as batch_name,z.zone_name,ct.center_name,cr.course_name,im.inquiry_date,ay.academic_year_master_name,sd.enrollment_no');
		$this -> db -> from('tbl_student_master sd');
		$this -> db -> join('tbl_student_group as sg', 'sd.student_id = sg.student_id', 'INNER');
        $this -> db -> join('tbl_group_master as gm', 'gm.group_master_id = sg.group_id', 'INNER');
        $this -> db -> join('tbl_zones as z', 'sd.zone_id  = z.zone_id', 'INNER');
		$this -> db -> join('tbl_centers as ct', 'sd.center_id  = ct.center_id', 'INNER');
		$this -> db -> join('tbl_inquiry_master as im', 'sd.inquiry_master_id  = im.inquiry_master_id', 'INNER');	
		$this -> db -> join('tbl_student_details as sd1', 'sd1.student_id  = sd.student_id AND sd.enrollment_no = sd1.enrollment_no', 'INNER');	
		$this -> db -> join('tbl_courses as cr', 'sd1.course_id  = cr.course_id', 'INNER');
		$this -> db -> join('tbl_academic_year_master as ay', 'sd1.academic_year_id  = ay.academic_year_master_id', 'INNER');
        
        $this -> db -> join('tbl_batch_master as tbm', 'tbm.batch_id = sg.batch_id', 'INNER');
        $this -> db -> join('tbl_batch_name_master as tbnm', 'tbnm.batch_name_master_id = tbm.batch_name_master_id', 'INNER');
        
//         $this -> db -> select('i.*,f.admission_fees_id,z.zone_name,ct.center_name,cr.course_name,im.inquiry_date,ay.academic_year_master_name');
// 		$this -> db -> from('tbl_student_master as i');
// 		$this -> db -> join('tbl_admission_fees as f', 'i.student_id  = f.student_id', 'left');
// 		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
// 		$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');
// 		$this -> db -> join('tbl_inquiry_master as im', 'i.inquiry_master_id  = im.inquiry_master_id', 'left');	
// 		$this -> db -> join('tbl_student_details as sd', 'i.student_id  = sd.student_id AND i.enrollment_no = sd.enrollment_no', 'left');	
// 		$this -> db -> join('tbl_courses as cr', 'sd.course_id  = cr.course_id', 'left');		
// 		$this -> db -> join('tbl_academic_year_master as ay', 'im.academic_year_master_id  = ay.academic_year_master_id', 'left');
        
        
        
        
        

		$this->db->where("($condition)");
		$this->db->group_by("sd.student_id");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
			
			
// 		echo $this->db->last_query(); 
//     	$this -> db -> select('concat(sd.student_first_name," ",sd.student_last_name) as student_name,GROUP_CONCAT(gm.group_master_name) as group_master_name,sg.student_id,sg.group_id,GROUP_CONCAT(tbnm.batch_name) as batch_name');
// 		$this -> db -> from('tbl_student_master sd');
// 		$this -> db -> join('tbl_student_group as sg', 'sd.student_id = sg.student_id', 'INNER');
//         $this -> db -> join('tbl_group_master as gm', 'gm.group_master_id = sg.group_id', 'INNER');
//         $this -> db -> join('tbl_batch_master as tbm', 'tbm.batch_id = sg.batch_id', 'INNER');
//         $this -> db -> join('tbl_batch_name_master as tbnm', 'tbnm.batch_name_master_id = tbm.batch_name_master_id', 'INNER');
        
         $this -> db -> select('concat(sd.student_first_name," ",sd.student_last_name) as student_name,GROUP_CONCAT(gm.group_master_name) as group_master_name,sg.student_id,sg.group_id,GROUP_CONCAT(tbnm.batch_name) as batch_name,z.zone_name,ct.center_name,cr.course_name,im.inquiry_date,ay.academic_year_master_name,sd.enrollment_no');
		$this -> db -> from('tbl_student_master sd');
		$this -> db -> join('tbl_student_group as sg', 'sd.student_id = sg.student_id', 'INNER');
        $this -> db -> join('tbl_group_master as gm', 'gm.group_master_id = sg.group_id', 'INNER');
        $this -> db -> join('tbl_zones as z', 'sd.zone_id  = z.zone_id', 'INNER');
		$this -> db -> join('tbl_centers as ct', 'sd.center_id  = ct.center_id', 'INNER');
		$this -> db -> join('tbl_inquiry_master as im', 'sd.inquiry_master_id  = im.inquiry_master_id', 'INNER');	
		$this -> db -> join('tbl_student_details as sd1', 'sd1.student_id  = sd.student_id AND sd.enrollment_no = sd1.enrollment_no', 'INNER');	
		$this -> db -> join('tbl_courses as cr', 'sd1.course_id  = cr.course_id', 'INNER');
		$this -> db -> join('tbl_academic_year_master as ay', 'sd1.academic_year_id  = ay.academic_year_master_id', 'INNER');
        
        $this -> db -> join('tbl_batch_master as tbm', 'tbm.batch_id = sg.batch_id', 'INNER');
        $this -> db -> join('tbl_batch_name_master as tbnm', 'tbnm.batch_name_master_id = tbm.batch_name_master_id', 'INNER');
        
        
        $this->db->where("($condition)");
		$this->db->group_by("sd.student_id");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
    }
    
    function getDropdown($tbl_name,$tble_flieds,$condition=NULL){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		if(!empty($condition)){
		$this -> db -> where($condition);
// 		$status="Active";
		    
		}
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
    }
    function getdata($student_id)
    {
        $this -> db -> select('concat(sd.student_first_name," ",sd.student_last_name) as student_name, gm.group_master_name,sg.student_id,sg.group_id');
		$this -> db -> from('tbl_student_master sd');
		$this -> db -> join('tbl_student_group as sg', 'sd.student_id = sg.student_id', 'INNER');
        $this -> db -> join('tbl_group_master as gm', 'gm.group_master_id = sg.group_id', 'INNER');
        
		$this-> db -> where("('sd.student_id',$student_id)");
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
    }
    
    
    	function getCenterMappedCourses($center_id){
		$this -> db -> select('tc.course_id,tc.course_name');
		$this -> db -> from('tbl_center_courses as cc');
		$this -> db -> join('tbl_courses as tc', 'tc.course_id  = cc.course_id', 'left');
		$condition = array("cc.center_id"=>$center_id);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	

    	/* Fetch records from multiple tables [Join Queries] with multiple condition, Sorting, Limit, Group By*/
	function JoinFetch($main_table = array(), $join_tables = array(), $condition = null, $sort_by = null, $group_by = null, $limit = null) {
		$columns = isset($main_table[1]) ? $main_table[1] : array();
		$main_table = $main_table[0];

		$join_str = "";
		foreach ($join_tables as $join_table) {
			$join_str .= $join_table[0] . " JOIN " . $join_table[1] . " ON (" . $join_table[2] . ") ";
			if (isset($join_table[3])) {
				$columns = array_merge($columns, $join_table[3]);
			}
		}
												
		$columns = (sizeof($columns) > 0) ? implode(", ", $columns) : "*";

		if (is_null($condition) || $condition == "") {
			$condition = "1=1";
		}
										
		$sort_order = "";
		if (is_array($sort_by) && $sort_by != null) {
			foreach ($sort_by as $key => $val) {
				$sort_order .= ($sort_order == "") ? "ORDER BY $key $val" : ", $key $val";
			}
		}
														
		if ($group_by != null) {
			$group_by = " GROUP BY " . $group_by;
		}
								
		if ($limit != null) {
			$limit = " LIMIT " . $limit;
		}
				
		//$this->db->query($this->set_timezone_query);
		$sql = trim("SELECT $columns FROM $main_table $join_str WHERE $condition $group_by $sort_order $limit");
		
		// echo $sql.'<br/><br/><br/>';
		// exit;
		$query = $this->db->query($sql);
		return $query;
	}

	/*
		 * Fetch as per the request like [Array,Object,Asscociative Array]
	*/
	function MySqlFetchRow($result, $type = 'assoc') {

		$row = false;
		if ($result != false) {

			switch ($type) {
			case 'array':
				$row = @$result->result_array();
				break;
			case 'object':
				$row = @$result->result();
				break;
			default:
			case 'assoc':
				$row = @$result->row_array();
				break;
			}
		}
		return $row;
	}
	
	function delete_records($student_id,$group_id,$batch_id)
	{
		$this->db->where('student_id', $student_id);
		$this->db->where('group_id', $group_id);
		$this->db->where('batch_id', $batch_id);
		$this->db->delete('tbl_student_group');
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function getStdDatas($table, $select = "*", $condition = '') 
	{
		$this->db->select($select);
		$this->db->from($table);
		if(!empty($condition)){
			$this->db->where("($condition)");
		}
		$query = $this->db->get();
		// echo $this->db->last_query();
		// exit;
		if($query->num_rows() >= 1) 
		{
			return $query->result_array();
		} 
		else 
		{
			
			return false;
		}
	}

	function getCenterGroup($center_id){
		$this -> db -> select('gm.group_master_id as group_id,gm.group_master_name as group_name');
		$this -> db -> from('tbl_group_master gm');
		$this -> db -> join('tbl_batch_master as bm', 'bm.group_id = gm.group_master_id', 'RIGHT');
		$this-> db -> where("bm.center_id",$center_id);
		$this-> db -> where("gm.status","Active");
		$this-> db -> where("bm.status","Active");
		$this-> db -> group_by("gm.group_master_id");
        $query = $this -> db -> get();
        if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
}