<?PHP
class Communicationmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	
	function get_fcm_data($user_id){
		$query = $this->db->query('Select * from tbl_student_master where student_id = '.$this->db->escape($user_id).' ');
	    
		if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
	}
	
	function get_email_data($eid){
		$query = $this->db->query('Select * from tbl_emailcontents where eid = '.$this->db->escape($eid));
	    
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getRecords($get=null){
		$table = "tbl_student_notices";
		$table_id = 'i.student_notice_id';
		$default_sort_column = 'i.student_notice_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('s.student_first_name','s.student_last_name','c.communication_categoy_name','b.batch_name','i.noticetitle','i.noticedescription','a.academic_year_master_name','i.notice_file');
		$searchArray = array('s.student_first_name','s.student_last_name','c.communication_categoy_name','b.batch_name','i.noticetitle','i.noticedescription','a.academic_year_master_name','i.notice_file');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<6;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		$this -> db -> select('i.*, s.student_first_name,s.student_last_name,c.communication_categoy_name,b.batch_name,a.academic_year_master_name');
		$this -> db -> from('tbl_student_notices as i');
		$this -> db -> join('tbl_notice_students as sn', 'i.student_notice_id  = sn.student_notice_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'sn.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_communication_categories as c', 'i.communication_category_id  = c.communication_category_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');	
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.*, s.student_first_name,s.student_last_name,c.communication_categoy_name,b.batch_name,a.academic_year_master_name');
		$this -> db -> from('tbl_student_notices as i');
		$this -> db -> join('tbl_notice_students as sn', 'i.student_notice_id  = sn.student_notice_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'sn.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_communication_categories as c', 'i.communication_category_id  = c.communication_category_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');	
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
	}

	function getStudents($tbl_id,$comp_id,$tbl_id1,$comp_id1,$tbl_id2,$comp_id2,$tbl_id3,$comp_id3,$tbl_id4,$comp_id4){
		$this -> db -> select('s.student_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name');
		$this -> db -> from('tbl_student_details as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this->db->where_in($comp_id,$tbl_id);
		$this->db->where_in($comp_id1,$tbl_id1);
		$this->db->where($comp_id2,$tbl_id2);
		$this->db->where_in('batch_id',$tbl_id3);
		$this->db->where($comp_id4,$tbl_id4);
		$status="Active";
		$this -> db -> where("d.status",$status);
		// $this -> db -> group_by("s.student_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	function getGroupStudents($tbl_id,$comp_id,$tbl_id1,$comp_id1,$tbl_id2,$comp_id2,$tbl_id3,$comp_id3){
		$this -> db -> select('d.*,s.student_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name');
		$this -> db -> from('tbl_student_group as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this->db->where_in($comp_id,$tbl_id);
		$this->db->where_in($comp_id1,$tbl_id1);
		$this->db->where_in($comp_id2,$tbl_id2);
		$this->db->where($comp_id3,$tbl_id3);
		$status="Active";
		$this -> db -> where("d.status",$status);
		// $this -> db -> group_by("s.student_id");
		$query = $this -> db -> get();
		// print_r($query->result_array());exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	function getOptionsGroup1($tbl_id,$comp_id){
		$this -> db -> select('g.group_master_id, g.group_master_name');
		$this -> db -> from('tbl_group_master as g');
		$this -> db -> join('tbl_batch_master as b', 'b.group_id  = g.group_master_id', 'left');
		$this -> db -> where_in($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("b.status",$status);
		$this -> db -> group_by("g.group_master_id");
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	// function getAllStudents($tbl_id,$comp_id,$tbl_id1,$comp_id1){

	// 	$this -> db -> select('s.student_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name');
	// 	$this -> db -> from('tbl_student_details as d');
	// 	$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
	// 	$this->db->where($comp_id,$tbl_id);
	// 	$this->db->where($comp_id1,$tbl_id1);
	// 	$status="Active";
	// 	$this -> db -> where("s.status",$status);
	// 	$query = $this -> db -> get();
		
	// 	if($query -> num_rows() >= 1)
	// 	{
	// 		return $query->result_array();
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }
	
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getDropdownCategory($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$category_id = 1;
		$this -> db -> where("status",$status);
		$this -> db -> where("category_id",$category_id);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	// function getFormdata($ID){
		
	// 	$this -> db -> select('i.*');
	// 	$this -> db -> from('tbl_assign_album as i');
	// 	$this->db->where('i.assign_album_id', $ID);
	
	// 	$query = $this -> db -> get();
	   
	// 	if($query -> num_rows() >= 1)
	// 	{
	// 		return $query->result();
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
		
	// }
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions3($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where_in($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	// function getCenterGroups($tbl_id){
	// 	$condition = "i.user_id='".$tbl_id."'  ";
	// 	$this -> db -> select('i.*,g.group_master_name');
	// 	$this -> db -> from('tbl_center_user_groups as i');
	// 	$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		
	// 	$this->db->where("($condition)");
	// 	$query = $this -> db -> get();
	// 	// print_r($query->result());
	// 	// exit();
	// 	if($query -> num_rows() >= 1)
	// 	{
	// 		return $query->result();
	// 	}
	// }

	function getBatchCourseData($tbl_id,$comp_id){
		$this -> db -> select('d.*, b.center_id');
		$this -> db -> from('tbl_student_details as d');
		$this -> db -> join('tbl_batch_master as b', 'b.batch_id  = d.batch_id', 'left');
		$this -> db -> where_in($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("b.status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getBatchGroupData($tbl_id,$comp_id){
		$this -> db -> select('g.*, b.center_id');
		$this -> db -> from('tbl_student_group as g');
		$this -> db -> join('tbl_batch_master as b', 'b.batch_id  = g.batch_id', 'left');
		$this -> db -> where_in($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("b.status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getOptions1($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where_in($comp_id,$tbl_id);
		$this -> db -> where_in($comp_id1,$tbl_id1);
		$status="Active";
		$this -> db -> where("status",$status);
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions2($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1,$tbl_id2,$comp_id2){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where_in($comp_id1,$tbl_id1);
		$this -> db -> where_in($comp_id2,$tbl_id2);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionsClass($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where_in($comp_id,$tbl_id);
		$status="Active";
		$category_id = 1;
		$batchSelectionType = 'Class';
		$this -> db -> where("status",$status);
		$this -> db -> where("category_id",$category_id);
		$this -> db -> where("batch_selection_type",$batchSelectionType);
	
		$query = $this -> db -> get();
		// print_r($query);
		// exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionsGroup($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where_in($comp_id,$tbl_id);
		$status="Active";
		$batchSelectionType = 'Group';
		$this -> db -> where("status",$status);
		$this -> db -> where("batch_selection_type",$batchSelectionType);
	
		$query = $this -> db -> get();
		// print_r($query);
		// exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionsGroup2($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where_in($comp_id,$tbl_id);
		$this -> db -> where_in($comp_id1,$tbl_id1);
		$status="Active";
		$batchSelectionType = 'Group';
		$this -> db -> where("status",$status);
		$this -> db -> where("batch_selection_type",$batchSelectionType);
	
		$query = $this -> db -> get();
		// print_r($query);
		// exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}


	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }

    function getdata1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

    function getdataCondition($condition = '1=1',$val1,$col1,$val2,$col2){
        $this -> db -> select('*');
		$this -> db -> from('tbl_student_notices as s');
		$this -> db -> join('tbl_notice_students as n', 's.student_notice_id  = n.student_notice_id', 'left');
		$this -> db -> where_in($col1,$val1);
		$this -> db -> where_in($col2,$val2);
		$this -> db -> where($condition);
        $query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
    }
	
	
	// function updateRecord($tableName, $data, $column, $value)
	// {
	// 	$this->db->where("$column", $value);
	// 	$this->db->update($tableName, $data);
	// 	if ($this->db->affected_rows() > 0) {
	// 		return true;
	// 	}
	// 	else {
	// 		return true;
	// 	}
	// }  
	
	// function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	// {
	// 	$data = array('status' => $status);
		 
	// 	$this->db->where($tbl_id, $record_id);
	// 	$this->db->update($tbl_name,$data); 
		 
	// 	if($this->db->affected_rows() >= 1)
	// 	{
	// 		return true;
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }	
}
?>
