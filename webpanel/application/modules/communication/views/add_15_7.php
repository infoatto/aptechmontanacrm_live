<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Add Communication</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>communication">Communication</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
				<div class="control-group form-group">
						<label class="control-label">Academic Year*</label>
						<div class="controls">
							<select id="academic_year_id" name="academic_year_id" class="form-control">
								<option value="">Select Academic Year</option>
								<?php 
									if(isset($academicyear) && !empty($academicyear)){
										foreach($academicyear as $cdrow){
								?>
									<option value="<?php echo $cdrow->academic_year_master_id;?>"><?php echo $cdrow->academic_year_master_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label" for="zone_id">Zone*</label>
						<div class="controls">
							<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);getGroup(this.value)" >
								<option value="">Select Zone</option>
								<?php 
									if(isset($zones) && !empty($zones)){
										foreach($zones as $cdrow){
								?>
									<option value="<?php echo $cdrow->zone_id;?>"><?php echo $cdrow->zone_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label"><span>Center*</span></label> 
						<div class="controls">
							<select id="center_id" name="center_id" class="form-control"  onchange="getBatch(this.value);getGroup(this.value)" >
								<option value="">Select Center</option>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label"><span>Selection Type*</span></label>
						<div class="controls">
							<input type="radio" class="form-control selection_type" name="selection_type" value="Class" checked="checked">Class
							<input type="radio" class="form-control selection_type" name="selection_type" value="Group">Group
						</div>
					</div>

					<div class="control-group form-group class_div">
						<label class="control-label" for="category_id">Category*</label>
						<div class="controls">
							<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);" >
								<option value="">Select Category</option>
								<?php 
									if(isset($categories) && !empty($categories)){
										foreach($categories as $cdrow){
								?>
									<option value="<?php echo $cdrow->category_id;?>"><?php echo $cdrow->categoy_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
				
					<div class="control-group form-group class_div">
						<label class="control-label"><span>Course*</span></label> 
						<br>
						<input type="checkbox" id="assigntoallcourse" name="assigntoallcourse" onchange="getBatch(this.value);getStudent(this.value)">Assign To All
						<br>
						<div class="controls course_div">
							<select id="course_id" name="course_id" class="form-control" onchange="getBatch(this.value);getStudent(this.value)">
								<option value="">Select Course</option>
							</select>
						</div>
					</div>

					<div class="control-group form-group group_div">
						<label class="control-label" for="group_id">Group*</label>
						<br>
						<input type="checkbox" id="assigntoallgroup" name="assigntoallgroup" onchange="getGroupBatch(this.value);getGroupStudent(this.value)">Assign To All
						<br>
						<div class="controls grp_div">
							<select id="group_id" name="group_id" class="form-control" onchange="getGroupBatch(this.value);getGroupStudent(this.value)">
								<option value="">Select Group</option>
								<!-- <?php 
									if(isset($groups) && !empty($groups)){
										foreach($groups as $cdrow){
								?>
									<option value="<?php echo $cdrow->group_id;?>"><?php echo $cdrow->group_master_name;?></option>
								<?php }}?> -->
							</select>
						</div>
					</div>

					<div class="control-group form-group group_div">
						<label class="control-label" for="group_batch_id">Batch*</label>
						<br>
						<input type="checkbox" id="assigntoallgroupbatch" name="assigntoallgroupbatch" onchange="getGroupStudent(this.value);">Assign To All
						<br>
						<div class="controls batchgroup_div">
							<select id="group_batch_id" name="group_batch_id" class="form-control" onchange="getGroupStudent(this.value);">
								<option value="">Select Batch</option>
							</select>
						</div>
					</div>

					<div class="control-group form-group class_div">
						<label class="control-label" for="batch_id">Batch*</label>
						<br>
						<input type="checkbox" id="assigntoallbatch" name="assigntoallbatch" onchange="getStudent(this.value);">Assign To All
						<br>
						<div class="controls batch_div">
							<select id="batch_id" name="batch_id" class="form-control" onchange="getStudent(this.value);">
								<option value="">Select Batch</option>
							</select>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label">Student*</label>
						<div class="controls">
							<select id="student_id" name="student_id[]" class="form-control select2" multiple>
								<option value="">Select Student</option>
							</select>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label" for="communication_category_id">Communication Category*</label>
						<div class="controls">
							<select id="communication_category_id" name="communication_category_id" class="form-control">
								<option value="">Select Communication Category</option>
								<?php 
									if(isset($communication_categories) && !empty($communication_categories)){
										foreach($communication_categories as $cdrow){
								?>
									<option value="<?php echo $cdrow->communication_category_id;?>"><?php echo $cdrow->communication_categoy_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label"><span>Communication Title</span></label>
						<div class="controls">
							<input type="text" class="form-control required" placeholder="Enter Title" id="noticetitle" name="noticetitle" value="" maxlength="50" >
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label"><span>Communication Description*</span></label>
						<div class="controls">
							<textarea class="form-control" name="noticedescription" value="" id="noticedescription"></textarea>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label">File Type</label>
						<div class="controls">
							<select id="notice_file_type" name="notice_file_type" class="form-control" onchange="changeFile()">
								<option value="">Select File Type</option>
								<option value="Image">Image</option>
								<option value="PDF">Pdf</option>
							</select>
						</div>
					</div>

					<div class="control-group form-group pdf">
						<label class="control-label"><span>File</span></label> 
						<div class="controls">
							<input type="file" name="notice_file" id="notice_file" class="form-control" accept="application/pdf"/>
						</div>
					</div>

					<div class="control-group form-group image">
						<label class="control-label"><span>File</span></label> 
						<div class="controls">
							<input type="file" name="notice_file" id="notice_file" class="form-control" accept="image/jpg,image/jpeg,image/png"/>
						</div>
					</div>
					
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>communication" class="btn btn-primary">Cancel</a>
					</div>
				</form>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$('.group_div').hide();
	$('.image').hide()
	$('.pdf').hide()
	$("#assigntoallcourse").click(function(){
		if($("#assigntoallcourse").is(':checked') ){
			$('.course_div').hide();
		}else{
			$('.course_div').show();
		}
	});
	$("#assigntoallbatch").click(function(){
		if($("#assigntoallbatch").is(':checked') ){
			$('.batch_div').hide();
		}else{
			$('.batch_div').show();
		}
	});
	$("#assigntoallgroup").click(function(){
		if($("#assigntoallgroup").is(':checked') ){
			$('.grp_div').hide();
		}else{
			$('.grp_div').show();
		}
	});
	$("#assigntoallgroupbatch").click(function(){
		if($("#assigntoallgroupbatch").is(':checked') ){
			$('.batchgroup_div').hide();
		}else{
			$('.batchgroup_div').show();
		}
	});
	$('.selection_type').change(function(){
		var selection_type = $(this).val();
		if(selection_type == 'Class'){
			$('.class_div').show();
			$('.group_div').hide();
		}
		else{
			$('.class_div').hide();
			$('.group_div').show();
		}
	})
});

function getCourses(category_id,course_id = null)
{
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>communication/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}
function getBatch(course_id,center_id,batch_id = null)
{
	var category_id = $('#category_id').val();//for all course
	var center_id = $('#center_id').val();
	if($('#assigntoallcourse').is(':checked')){
		$('#batch_id').val('')
	}
	else{
		var course_id = $('#course_id').val();
		$('#assigntoallbatch').prop('checked',false)
		$('.batch_div').show()
		$("#batch_id").html("<option value=''>Select</option>");
		$("#student_id").html("<option value=''>Select</option>");
	}
	if(course_id != "")
	{
		$.ajax({
			url:"<?php echo base_url();?>communication/getBatch",
			data:{category_id:category_id,center_id:center_id, course_id:course_id,batch_id:batch_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#batch_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#batch_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#batch_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getGroup(center_id,group_id = null){
	$("#group_batch_id").html("<option value=''>Select</option>");
	if(center_id != null && center_id != ''){
		$.ajax({
			url:"<?php echo base_url();?>communication/getGroup",
			data:{center_id:center_id,group_id:group_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#group_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#group_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#group_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getGroupBatch(group_id,batch_id = null)
{
	var center_id = $('#center_id').val();
	if($('#assigntoallgroup').is(':checked')){
		$('#group_id').val('')
	}
	else{
		var group_id = $('#group_id').val();
		$('#assigntoallgroupbatch').prop('checked',false)
		$('.batchgroup_div').show()
		$("#group_batch_id").html("<option value=''>Select</option>");
		$("#student_id").html("<option value=''>Select</option>");
	}
	if(group_id != "")
	{
		$.ajax({
			url:"<?php echo base_url();?>communication/getGroupbatch",
			data:{group_id:group_id,center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#group_batch_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#group_batch_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#group_batch_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>communication/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function changeFile(){
	if($('#notice_file_type').val() == 'Image'){
		$('.image').show()
		$('.pdf').hide()
	}
	else{
		$('.image').hide()
		$('.pdf').show()
	}
}

function getStudent(categoryid,course_id,center_id,batch_id = null)
{
	var category_id = $('#category_id').val();
	var academic_year_id = $('#academic_year_id').val();
	if($('#assigntoallcourse').is(':checked')){
		var course_id = 'on'
	}
	else{
		var course_id = $('#course_id').val();
	}
	if($('#assigntoallbatch').is(':checked')){
		var batch_id = 'on'
	}
	else{
		var batch_id = $('#batch_id').val();
	}
	var center_id = $('#center_id').val();
	console.log(batch_id)
	if(batch_id != "" && batch_id != null)
	{
		$.ajax({
			url:"<?php echo base_url();?>communication/getStudent",
			data:{category_id:category_id,center_id:center_id, course_id:course_id,batch_id:batch_id,academic_year_id:academic_year_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#student_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#student_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#student_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getGroupStudent(group_id,center_id,batch_id = null)
{
	var academic_year_id = $('#academic_year_id').val();
	if($('#assigntoallgroup').is(':checked')){
		var group_id = 'on'
	}
	else{
		var group_id = $('#group_id').val();
	}
	if($('#assigntoallgroupbatch').is(':checked')){
		var batch_id = 'on'
	}
	else{
		var batch_id = $('#group_batch_id').val();
	}
	var center_id = $('#center_id').val();
	if(batch_id != "" && batch_id != null)
	{
		$.ajax({
			url:"<?php echo base_url();?>communication/getGroupStudent",
			data:{center_id:center_id, group_id:group_id,batch_id:batch_id,academic_year_id:academic_year_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#student_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#student_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#student_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	category_id :{required:true},
	academic_year_id :{required:true},
	zone_id:{required:true},
	center_id:{required:true},
	student_id :{required:true},
	communication_category_id :{required:true},
	noticedescription :{required:true},
	
};
var vMessages = {
	category_id:{required:"Please select category."},
	academic_year_id:{required:"Please select academic year."},
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	student_id:{required:"Please select student."},
	communication_category_id:{required:"Please select communication category."},
	noticedescription:{required:"Please enter communication description."},
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>communication/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>communication";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "Add - Communication";

 
</script>					
