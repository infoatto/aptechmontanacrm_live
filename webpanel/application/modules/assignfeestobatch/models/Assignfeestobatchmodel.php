<?PHP
class Assignfeestobatchmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL){
		$this->db->insert($tbl_name,$data_array);
		$result_id = $this->db->insert_id();
		if($sendid == 1){
	 		return $result_id;
	 	}
	}
	 
	function getRecords($get=null){
		$table = "tbl_batch_master";
		$table_id = 'i.fees_batch_mapping_id';
		$default_sort_column = 'i.fees_batch_mapping_id';
		$default_sort_order = 'desc';
		$condition = "  1=1 ";
		$searchArray = array('z.zone_name','ce.center_name','g.group_master_name','bm.batch_name','fm.fees_name');
		$colArray = array('z.zone_name','ce.center_name','g.group_master_name','bm.batch_name','fm.fees_name');
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<4;$i++){
			if(isset($get['Searchkey_'.$i]) && $get['Searchkey_'.$i]!=''){
				$condition .= " && ".$searchArray[$i]." LIKE '%".$_GET['Searchkey_'.$i]."%' ";
			}
		}
		
		$this -> db -> select('i.*,fm.fees_name,z.zone_name,ce.center_name,g.group_master_name,bm.batch_name');
		$this -> db -> from('tbl_fees_batch_mapping as i');
		$this -> db -> join('tbl_fees_master as fm', 'fm.fees_id  = i.fees_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ce', 'i.center_id  = ce.center_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this -> db -> join('tbl_batch_master as bm', 'i.group_id  = bm.group_id AND i.batch_id = bm.batch_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->group_by("i.fees_batch_mapping_id");
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.*,fm.fees_name,z.zone_name,ce.center_name,g.group_master_name,bm.batch_name');
		$this -> db -> from('tbl_fees_batch_mapping as i');
		$this -> db -> join('tbl_fees_master as fm', 'fm.fees_id  = i.fees_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ce', 'i.center_id  = ce.center_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this -> db -> join('tbl_batch_master as bm', 'i.batch_id = bm.batch_id AND i.batch_id = bm.batch_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$this->db->group_by("i.fees_batch_mapping_id");
		$query1 = $this -> db -> get();

		if($query -> num_rows() >= 1){
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}else{
			return array("totalRecords"=>0);
		}
		
		exit;
	}
	
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
	
	function getFormdata($ID){
		
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_fees_batch_mapping as i');
		$this->db->where('i.fees_batch_mapping_id', $ID);
	
		$query = $this -> db -> get();
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	
	function delrecord($tbl_name,$condition){ 
		$this->db->where($condition);
		$this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1){
			return true;
		}else{
			return false;
		}
	}

	function getCenterGroup($condition){
		$this->db->select("gm.group_master_id,gm.group_master_name");
		$this->db->from("tbl_group_master as gm");
		$this->db->join("tbl_assign_fees_group_mapping as afgm","gm.group_master_id = afgm.group_master_id","LEFT");
		$this->db->join("tbl_assign_fees_center_mapping as afcm","afgm.assign_fees_id = afcm.assign_fees_id","LEFT");
		$this->db-> where($condition);
		$this->db->group_by("gm.group_master_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getGroupFees($condition){
		$this->db->select("fm.fees_id,fm.fees_name");
		$this->db->from("tbl_fees_master as fm");
		$this->db->join("tbl_assign_fees as af","fm.fees_id = af.fees_id","LEFT");
		$this->db->join("tbl_assign_fees_group_mapping as afgm","af.assign_fees_id = afgm.assign_fees_id","LEFT");
		$this->db->join("tbl_assign_fees_center_mapping as afcm","afgm.assign_fees_id = afcm.assign_fees_id","LEFT");
		$this->db-> where($condition);
		$this->db->group_by("fm.fees_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getGroupBatch($condition){
		$this -> db -> select('bm.batch_id,bm.batch_name');
		$this -> db -> from("tbl_batch_master as bm");
		$this -> db -> where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
}
?>
