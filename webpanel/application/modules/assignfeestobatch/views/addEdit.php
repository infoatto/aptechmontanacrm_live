<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Assign Fees To Batch</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>assignfeestobatch">Assign Fees To Batch</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-6 col-md-6">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="fees_batch_mapping_id" name="fees_batch_mapping_id" value="<?php echo(!empty($details[0]->fees_batch_mapping_id))?$details[0]->fees_batch_mapping_id:"";?>" />
						<div class="control-group form-group">
							<label class="control-label"><span>Batch Selection Type*</span></label>
							<div class="controls">
								<!-- <input type="radio" class="form-control batch_selection_type" name="batch_selection_type" value="Class" <?php echo (!empty($details[0]->batch_selection_type) &&  ($details[0]->batch_selection_type== 'Class')) ? "checked" : ""?> checked="checked">Class -->
								<label for="batch_selection_type">
									<input type="radio" id="batch_selection_type" class="batch_selection_type" name="batch_selection_type" value="Group" <?php echo (!empty($details[0]->batch_selection_type) &&  ($details[0]->batch_selection_type== 'Group')) ? "checked" : "checked"?> >Group
								</label>
							</div>
						</div>
						<!-- <div class="class_div">
							<div class="control-group form-group ">
								<label class="control-label" for="category_id">Category*</label>
								<div class="controls">
									<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
										<option value="">Select Category</option>
										<?php 
											if(isset($categories) && !empty($categories)){
												foreach($categories as $cdrow){
													$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
										?>
											<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
										<?php }}?>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
						
							<div class="control-group form-group">
								<label class="control-label"><span>Course*</span></label> 
								<div class="controls">
									<select id="course_id" name="course_id" class="form-control">
										<option value="">Select Course</option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
						</div> -->

						<div class="control-group form-group">
							<label class="control-label"><span>Zone*</span></label> 
							<div class="controls">
								<select id="zone_id" name="zone_id" class="select2"  onchange="getCenters();" selected_value = "<?php echo(!empty($details[0]->zone_id))?$details[0]->zone_id:""?>">
									<option value="">Select Zone</option>
									<?php 
										if(isset($zones) && !empty($zones)){
											foreach($zones as $cdrow){
												$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label"><span>Center*</span></label> 
							<div class="controls">
								<select id="center_id" name="center_id" class="select2" onchange="getCenterGroup();" selected_value = "<?php echo(!empty($details[0]->center_id))?$details[0]->center_id:""?>">
									<option value="">Select Center</option>
								</select>
							</div>
						</div>
						<div class="control-group form-group group_div">
							<label class="control-label" for="category_id">Group*</label>
							<div class="controls">
								<select id="group_id" name="group_id" class="select2"  onchange="getFeesandBatch();" selected_value = "<?php echo(!empty($details[0]->group_id))?$details[0]->group_id:""?>">
									<option value="">Select Group</option>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label" for="batch_id">Batch Name Selection*</label>
							<div class="controls">
								<select id="batch_id" name="batch_id" class="select2" style="width:98%" selected_value = "<?php echo(!empty($details[0]->batch_id))?$details[0]->batch_id:""?>">
									<option value="">Select Batch Name</option>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label"><span>Fees*</span></label> 
							<div class="controls">
							<?php if(!empty($details[0]->fees_batch_mapping_id)){ ?>
								<!-- <select id="fees_id" name="fees_id" class="form-control" selected_value = "<?php echo(!empty($details[0]->fees_id))?$details[0]->fees_id:""?>"> -->
								<select class="form-control selectpicker required" data-width="98%"   data-style="btn-primary" data-actions-box="false" data-live-search="true" data-live-search-placeholder="Search ..." id="fees_id" name="fees_id" selected_value = "<?php echo(!empty($details[0]->fees_id))?$details[0]->fees_id:""?>">
									<option value="">Select Fees</option>
								</select>
							<?php }else{ ?>
								<select class="form-control selectpicker required" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..." id="fees_id" name="fees_id[]">
									<option value="">Select Fees</option>
								</select>
							<?php } ?>
							</div>
						</div>
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>assignfeestobatch" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

$( document ).ready(function() {
	$('.class_div').hide();
	getCenters();
});

/* function getCourses(category_id,course_id = null){
	if(category_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>assignfeestobatch/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}else{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}else{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
} */


function getCenters(){
	$("#center_id").html("<option value=''>Select</option>");
	$("#course_id").html("<option value=''>Select Group</option>");
	$("#fees_id").html("<option value=''>Select Fees</option>");
	$("#batch_id").html("<option value=''>Select Batch</option>");
	var selected_center = $("#center_id").attr("selected_value");
	var zone_id = $("#zone_id").val();
	if(zone_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>assignfeestobatch/getCenters",
			data:{zone_id:zone_id,selected_center:selected_center},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res.status =="success"){
					if(res.option != ""){
						var added = $("#center_id").html("<option value=''>Select</option>"+res.option);
						if(selected_center != ""){
							getCenterGroup();
						}
					}else{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}else{	
					$("#center_id").html("<option value=''>Select</option>");
				}
				$("#center_id").select2()
			}
		});
	}
}

function getCenterGroup(){
	$("#course_id").html("<option value=''>Select Group</option>");
	$("#fees_id").html("<option value=''>Select Fees</option>");
	$("#batch_id").html("<option value=''>Select Batch</option>");
	var zone_id = $("#zone_id").val();
	var center_id = ($("#center_id").val()!="")?$("#center_id").val():$("#center_id").attr("selected_value");
	var selected_value = $("#group_id").attr("selected_value");
	if(zone_id != "" && center_id != ""){
		$.ajax({
			url:"<?php echo base_url();?>assignfeestobatch/getCenterGroup",
			data:{zone_id:zone_id, center_id:center_id,selected_value:selected_value},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#group_id").html("<option value=''>Select Group</option>"+res['option']);
						if(selected_value != ""){
							getFeesandBatch()
						}
					}else{
						$("#group_id").html("<option value=''>Select Group</option>");
					}
				}else{	
					$("#group_id").html("<option value=''>Select Group</option>");
				}
				$("#group_id").select2()
			}
		});
	}else{
		alert("Please select Zone / Center");
	}
}

function getFeesandBatch(){
	$("#fees_id").html("");
	$("#batch_id").html("<option value=''>Select Batch</option>");
	var center_id = $("#center_id").val();
	var group_id = $("#group_id").val();
	var fees_selected_value = $("#fees_id").attr("selected_value");
	var batch_selected_value = $("#batch_id").attr("selected_value");
	if(center_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>assignfeestobatch/getFeesandBatch",
			data:{center_id:center_id,group_id:group_id,batch_selected_value:batch_selected_value,fees_selected_value:fees_selected_value},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['fees_option'] != ""){
						$("#fees_id").html(res['fees_option']);
					}else{
						$("#fees_id").html("");
					}
					if(res['batch_option'] != ""){
						$("#batch_id").html("<option value=''>Select Batch</option>"+res['batch_option']);
					}else{
						$("#batch_id").html("<option value=''>Select Batch</option>");
					}
				}else{	
					$("#fees_id").html("");
					$("#batch_id").html("<option value=''>Select Batch</option>");
				}
				$("#fees_id").selectpicker("refresh");
				$("#batch_id").select2();
			}
		});
	}
}

var vRules = {
	zone_id:{required:true},
	center_id:{required:true},
	group_id:{required:true},
	batch_id:{required:true},
	fees_id:{required:true},
};
var vMessages = {
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	group_id:{required:"Please select group."},
	batch_id:{required:"Please select batch ."},
	fees_id:{required:"Please select Fees ."},
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form){
		var act = "<?php echo base_url();?>assignfeestobatch/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			dataType: 'json',
			method:'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				if(response.success == "1"){
					displayMsg("success",response.msg);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>assignfeestobatch";
					},2000);

				}else{	
					displayMsg("error",response.msg);
					return false;
				}
			},error:function(){
				displayMsg("error",response.msg);
			}
		});
	}
});

document.title = "Assign Fees To Batch";
</script>					
<style>
.select2{
	width:98% !important;
}
</style>