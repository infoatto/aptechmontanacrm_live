<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Feeslevelmaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('feeslevelmastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('feeslevelmaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			$result = [];
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				$result['details'] = $this->feeslevelmastermodel->getFormdata($record_id);
				//get mapped centers
				$selected_centers = $this->feeslevelmastermodel->getdata("tbl_fees_level_center_mapping","fees_level_id='".$record_id."' ");
				$result['selected_centers'] = (!empty($selected_centers))?array_column($selected_centers,"center_id"):array();

			}
			$result['centers'] = $this->feeslevelmastermodel->getDropdown("tbl_centers","center_id,center_name");
			
			
			$this->load->view('template/header.php');
			$this->load->view('feeslevelmaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
		


	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			$condition = "fees_level_name='".$_POST['fees_level_name']."' ";
			if(isset($_POST['fees_level_id']) && $_POST['fees_level_id'] > 0){
				$condition .= " &&  fees_level_id != ".$_POST['fees_level_id'];
			}
			$check_name = $this->feeslevelmastermodel->getdata("tbl_fees_level_master",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			$fees_level_id= "";
			if (!empty($_POST['fees_level_id'])) {
				$data_array = array();			
				$fees_level_id = $_POST['fees_level_id'];
				$data_array['fees_level_name'] = (!empty($_POST['fees_level_name'])) ? $_POST['fees_level_name'] : '';
				$data_array['status'] = (!empty($_POST['status'])) ? $_POST['status'] : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->feeslevelmastermodel->updateRecord('tbl_fees_level_master', $data_array,'fees_level_id',$fees_level_id);
				if(!empty($_POST['center_id'])){
					//delete existing data of mapping'
					$this->feeslevelmastermodel->deletedata("tbl_fees_level_center_mapping","fees_level_id",$fees_level_id);
				}
			}else {
				$data_array = array();
				$data_array['fees_level_name'] = (!empty($_POST['fees_level_name'])) ? $_POST['fees_level_name'] : '';
				$data_array['status'] = (!empty($_POST['status'])) ? $_POST['status'] : '';
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				$result = $this->feeslevelmastermodel->insertData('tbl_fees_level_master', $data_array, '1');
				$fees_level_id = $result;
			}


			// mapping multiple courses
			if(!empty($_POST['center_id']) && $fees_level_id != ""){
				foreach($_POST['center_id'] as $key=>$val){
					$center_mapping_data = array();
					$center_mapping_data['fees_level_id'] = $fees_level_id;
					$center_mapping_data['center_id'] = $val;
					$this->feeslevelmastermodel->insertData('tbl_fees_level_center_mapping', $center_mapping_data, '1');
				}
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->feeslevelmastermodel->getRecords($_GET);

		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->fees_level_name);
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("FeesLevelAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->fees_level_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("FeesLevelAddEdit")){
					$actionCol1.= '<a href="feeslevelmaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->fees_level_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord12()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->feeslevelmastermodel->delrecord12("tbl_fees_level_master","fees_level_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
}

?>
