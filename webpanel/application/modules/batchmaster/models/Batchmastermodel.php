<?PHP
class Batchmastermodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	 
	function getRecords($get=null){
		$table = "tbl_batch_master";
		$table_id = 'i.batch_id';
		$default_sort_column = 'i.batch_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('i.batch_selection_type','z.zone_name','ce.center_name','c.categoy_name','cn.course_name','g.group_master_name','i.batch_name','i.status');
		$searchArray = array('i.batch_selection_type','z.zone_name','ce.center_name','c.categoy_name','cn.course_name','g.group_master_name','i.batch_name','i.status');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<7;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		
		$this -> db -> select('i.*,c.categoy_name, cn.course_name, z.zone_name,ce.center_name,g.group_master_name');
		$this -> db -> from('tbl_batch_master as i');
		$this -> db -> join('tbl_categories as c', 'i.category_id  = c.category_id', 'left');
		$this -> db -> join('tbl_courses as cn', 'i.course_id  = cn.course_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ce', 'i.center_id  = ce.center_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.*,c.categoy_name, cn.course_name, z.zone_name,ce.center_name,g.group_master_name');
		$this -> db -> from('tbl_batch_master as i');
		$this -> db -> join('tbl_categories as c', 'i.category_id  = c.category_id', 'left');
		$this -> db -> join('tbl_courses as cn', 'i.course_id  = cn.course_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ce', 'i.center_id  = ce.center_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();

		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			return array("totalRecords"=>0);
		}
		
		exit;
	}
	
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
	
	function getFormdata($ID){
		
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_batch_master as i');
		$this->db->where('i.batch_id', $ID);
	
		$query = $this -> db -> get();
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}
?>
