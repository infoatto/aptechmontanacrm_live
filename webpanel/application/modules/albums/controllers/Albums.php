<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Albums extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('albumsmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->albumsmodel->getFormdata($record_id);
			
			if(!empty($result['details'])){
				//$result['productImages'] = $this->albumsmodel->getProductImages($record_id);
			}
			
			$zone_data = array('action'=>'FetchZone', 'BrandID'=>'112');
			$getZones = curlFunction(SERVICE_URL, $zone_data);
			//echo "<pre>";print_r($getZones);exit;
			$getZones = json_decode($getZones, true);
			$result['zones'] = $getZones;
			
			/*for($i=0; $i < sizeof($getZones);$i++){
				echo $getZones[$i]['Zone'];
			}
			exit;
			*/
            
			$this->load->view('template/header.php');
			$this->load->view('albums/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$album_id = "";
			
			$condition = "album_name= '".$_POST['album_name']."' ";
			if(isset($_POST['album_id']) && $_POST['album_id'] > 0)
			{
				$condition .= " &&  album_id != ".$_POST['album_id'];
			}
			
			$check_name = $this->albumsmodel->getdata("tbl_albums",$condition);
			//echo "<pre>";print_r($check_name);exit;
			//echo $_SESSION["webadmin"][0]->user_name;exit;
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			//exit;
			if (!empty($_POST['album_id'])) {
				$data_array=array();			
				$album_id = $_POST['album_id'];
		 		
				$data_array['teacher_id'] = (!empty($_SESSION["webadmin"][0]->user_name)) ? $_SESSION["webadmin"][0]->user_name : '';
				$data_array['center_id'] = (!empty($_SESSION["webadmin"][0]->center_id)) ? $_SESSION["webadmin"][0]->center_id : '';
				$data_array['album_name'] = (!empty($_POST['album_name'])) ? $_POST['album_name'] : '';
				
				$data_array['updated_by']=$_SESSION["webadmin"][0]->user_id;
				
				$result = $this->albumsmodel->updateRecord('tbl_albums', $data_array,'album_id',$album_id);
				
			}else {
				
				$data_array = array();
				$data_array['teacher_id'] = (!empty($_SESSION["webadmin"][0]->user_name)) ? $_SESSION["webadmin"][0]->user_name : '';
				$data_array['center_id'] = (!empty($_SESSION["webadmin"][0]->center_id)) ? $_SESSION["webadmin"][0]->center_id : '';
				$data_array['album_name'] = (!empty($_POST['album_name'])) ? $_POST['album_name'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->albumsmodel->insertData('tbl_albums', $data_array, '1');
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->albumsmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->album_name);
				
				$actionCol21="";
				$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->album_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				
				$actionCol1 = "";
				$actionCol1.= '<a href="albums/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->album_id) , '+/', '-_') , '=') . '" title="Edit">Edit</i></a> | ';
				$actionCol1.= '<a href="albumimages/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->album_id) , '+/', '-_') , '=') . '" title="View">View/Add Images</a>';
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && album_id='".$_GET['id']."' ";
		$result['details'] = $this->albumsmodel->getdata('tbl_albums',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('albums/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->albumsmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('newsletters/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		//$config['file_name']     = md5(uniqid("100_ID", true));
		$config['upload_path'] = DOC_ROOT_FRONT."/images/newsletters_images";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}

	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->albumsmodel->delrecord("tbl_albums","album_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->albumsmodel->delrecord12("tbl_albums","album_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	
	

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
