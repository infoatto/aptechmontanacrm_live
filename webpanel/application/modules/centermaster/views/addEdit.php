<?php 
//error_reporting(0);

$selcourses1 = array();
if(!empty($selcourses)){
	for($i=0; $i < sizeof($selcourses); $i++){
		$selcourses1[] = $selcourses[$i]->course_id;
	}
}
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Center Master</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>centermaster">Center Master</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="center_id" name="center_id" value="<?php if(!empty($details[0]->center_id)){echo $details[0]->center_id;}?>" />
								
								<div class="control-group form-group">
									<label class="control-label" for="zone_id">Zone*</label>
									<div class="controls">
										<select id="zone_id" name="zone_id" class="form-control" >
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
														$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<!--
								<div class="control-group form-group">
									<label class="control-label" for="category_id">Category*</label>
									<div class="controls">
										<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
											<option value="">Select Category</option>
											<?php 
												if(isset($categories) && !empty($categories)){
													foreach($categories as $cdrow){
														$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="course_id">Courses*</label>
									<div class="controls">
										<select id="course_id" name="course_id[]" class="form-control select2" multiple >
											<option value="">Select Courses</option>
										</select>
									</div>
								</div>
							-->
								<div class="control-group form-group">
									<label class="control-label"><span>Center Name*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Center Name" id="center_name" name="center_name" value="<?php if(!empty($details[0]->center_name)){echo $details[0]->center_name;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Name for printing receipt/confirmation*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Name for printing receipt/confirmation" id="receipt_print_name" name="receipt_print_name" value="<?php if(!empty($details[0]->receipt_print_name)){echo $details[0]->receipt_print_name;}?>" >
									</div>
								</div>

								<div class="control-group form-group">
									<label class="control-label"><span>Center Code*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Center Code" id="center_code" name="center_code" value="<?php if(!empty($details[0]->center_code)){echo $details[0]->center_code;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center Description</span></label>
									<br/>(Editor is for text contens only kindly don't put any image)
									<div class="controls">
										<textarea name="center_description" id="center_description" placeholder="Enter Description" class="form-control editor"><?php if(!empty($details[0]->center_description)){echo $details[0]->center_description;}?></textarea>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center Address*</span></label>
									<div class="controls">
										<textarea name="center_address" id="center_address" placeholder="Enter Address" class="form-control"><?php if(!empty($details[0]->center_address)){echo $details[0]->center_address;}?></textarea>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center Spoc*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Spoc Name" id="center_spoc" name="center_spoc" value="<?php if(!empty($details[0]->center_spoc)){echo $details[0]->center_spoc;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center Contact No.*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Center Contact No." id="center_contact_no" name="center_contact_no" value="<?php if(!empty($details[0]->center_contact_no)){echo $details[0]->center_contact_no;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Center Eamil ID*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Center Email ID" id="center_email_id" name="center_email_id" value="<?php if(!empty($details[0]->center_email_id)){echo $details[0]->center_email_id;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Can See Timetable Before(days)*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Can See Timetable Before Value" id="can_see_timetable_before" name="can_see_timetable_before" value="<?php if(!empty($details[0]->can_see_timetable_before)){echo $details[0]->can_see_timetable_before;}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Can See Timetable After(days)*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Can See Timetable After Value" id="can_see_timetable_after" name="can_see_timetable_after" value="<?php if(!empty($details[0]->can_see_timetable_after)){echo $details[0]->can_see_timetable_after;}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>centermaster" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>
$( document ).ready(function() {
	
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

function getCourses(category_id)
{
	//alert("center_id: "+center_id);return false;
	if(category_id != "" )
	{
		<?php 
			if(!empty($details[0]->center_id)){
		?>
			var center_id = '<?php echo $details[0]->center_id; ?>';
		<?php }else{?>
			var center_id = '';
		<?php }?>
		
		//alert();
		$.ajax({
			url:"<?php echo base_url();?>centermaster/getCourses",
			data:{category_id:category_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
						$("#course_id").select2();
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
						$("#course_id").select2();
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
					$("#course_id").select2();
				}
			}
		});
	}
}

var vRules = {
	zone_id:{required:true},
	center_name:{required:true},
	receipt_print_name:{required:true},
	center_code:{required:true},
	center_description:{required:true},
	center_address:{required:true},
	center_spoc:{required:true},
	center_contact_no:{required:true},
	center_email_id:{required:true, email:true},
	can_see_timetable_before:{required:true, digits: true},
	can_see_timetable_after:{required:true, digits: true}
	
};
var vMessages = {
	zone_id:{required:"Please select zone."},
	center_name:{required:"Please enter center name."},
	receipt_print_name:{required:"Please enter receipt print name."},
	center_code:{required:"Please enter center code."},
	center_description:{required:"Please enter center description."},
	center_address:{required:"Please enter center address."},
	center_spoc:{required:"Please enter center spoc name."},
	center_contact_no:{required:"Please enter center contact no."},
	center_email_id:{required:"Please enter valid email id."},
	can_see_timetable_before:{required:"Please enter value."},
	can_see_timetable_after:{required:"Please enter value."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>centermaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>centermaster";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Center";

 
</script>					
