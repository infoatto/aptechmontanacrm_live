<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Centermaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('centermastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('centermaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['zones'] = $this->centermastermodel->getDropdown("tbl_zones","zone_id,zone_name");
			//$result['courses'] = $this->centermastermodel->getDropdown("tbl_courses","course_id,course_name");
			$result['categories'] = $this->centermastermodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['details'] = $this->centermastermodel->getFormdata($record_id);
			
			
			if(!empty($result['details'])){
				$result['selcourses'] = $this->centermastermodel->getDropdownSelval('tbl_center_courses', 'center_id', 'course_id',$record_id);
			}
			
			//echo "<pre>";print_r($result['selcourses']);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('centermaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->centermastermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$selcourses1 = array();
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$selcourses = $this->centermastermodel->getDropdownSelval('tbl_center_courses', 'center_id', 'course_id',$_REQUEST['center_id']);
			if(!empty($selcourses)){
				for($i=0; $i < sizeof($selcourses); $i++){
					$selcourses1[] = $selcourses[$i]->course_id;
				}
			}
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				//$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$sel = (in_array($result[$i]->course_id, $selcourses1)) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			$condition = "zone_id='".$_POST['zone_id']."' && center_name= '".$_POST['center_name']."' ";
			if(isset($_POST['center_id']) && $_POST['center_id'] > 0)
			{
				$condition .= " &&  center_id != ".$_POST['center_id'];
			}
			
			$check_name = $this->centermastermodel->getdata("tbl_centers",$condition);
			//echo "<pre>";print_r($check_name);exit;
			//echo $_SESSION["webadmin"][0]->user_name;exit;
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			//exit;
			if (!empty($_POST['center_id'])) {
				$data_array = array();			
				$center_id = $_POST['center_id'];
		 		
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				//$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['center_name'] = (!empty($_POST['center_name'])) ? $_POST['center_name'] : '';
				$data_array['receipt_print_name'] = (!empty($_POST['receipt_print_name'])) ? $_POST['receipt_print_name'] : '';
				$data_array['center_code'] = (!empty($_POST['center_code'])) ? $_POST['center_code'] : '';
				$data_array['center_description'] = (!empty($_POST['center_description'])) ? htmlentities($_POST['center_description']) : '';
				$data_array['center_address'] = (!empty($_POST['center_address'])) ? $_POST['center_address'] : '';
				$data_array['center_spoc'] = (!empty($_POST['center_spoc'])) ? $_POST['center_spoc'] : '';
				$data_array['center_contact_no'] = (!empty($_POST['center_contact_no'])) ? $_POST['center_contact_no'] : '';
				$data_array['center_email_id'] = (!empty($_POST['center_email_id'])) ? $_POST['center_email_id'] : '';
				$data_array['can_see_timetable_before'] = (!empty($_POST['can_see_timetable_before'])) ? $_POST['can_see_timetable_before'] : '';
				$data_array['can_see_timetable_after'] = (!empty($_POST['can_see_timetable_after'])) ? $_POST['can_see_timetable_after'] : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->centermastermodel->updateRecord('tbl_centers', $data_array,'center_id',$center_id);
				
				if(!empty($_POST['course_id'])){
					$this->centermastermodel->delrecord1('tbl_center_courses', 'center_id',$center_id);
					for($i=0; $i < sizeof($_POST['course_id']); $i++){
						$data = array();
						$data['center_id'] = $center_id;
						$data['course_id'] = $_POST['course_id'][$i];
						$this->centermastermodel->insertData('tbl_center_courses', $data, '1');
					}
				}
				
			}else {
		
				$data_array = array();
				
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				//$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['center_name'] = (!empty($_POST['center_name'])) ? $_POST['center_name'] : '';
				$data_array['receipt_print_name'] = (!empty($_POST['receipt_print_name'])) ? $_POST['receipt_print_name'] : '';
				$data_array['center_code'] = (!empty($_POST['center_code'])) ? $_POST['center_code'] : '';
				$data_array['center_description'] = (!empty($_POST['center_description'])) ? htmlentities($_POST['center_description']) : '';
				$data_array['center_address'] = (!empty($_POST['center_address'])) ? $_POST['center_address'] : '';
				$data_array['center_spoc'] = (!empty($_POST['center_spoc'])) ? $_POST['center_spoc'] : '';
				$data_array['center_contact_no'] = (!empty($_POST['center_contact_no'])) ? $_POST['center_contact_no'] : '';
				$data_array['center_email_id'] = (!empty($_POST['center_email_id'])) ? $_POST['center_email_id'] : '';
				$data_array['can_see_timetable_before'] = (!empty($_POST['can_see_timetable_before'])) ? $_POST['can_see_timetable_before'] : '';
				$data_array['can_see_timetable_after'] = (!empty($_POST['can_see_timetable_after'])) ? $_POST['can_see_timetable_after'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->centermastermodel->insertData('tbl_centers', $data_array, '1');
				
				if(!empty($_POST['course_id'])){
					for($i=0; $i < sizeof($_POST['course_id']); $i++){
						$data = array();
						$data['center_id'] = $result;
						$data['course_id'] = $_POST['course_id'][$i];
						$this->centermastermodel->insertData('tbl_center_courses', $data, '1');
					}
				}
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->centermastermodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->center_address);
				array_push($temp, $get_result['query_result'][$i]->center_spoc);
				array_push($temp, $get_result['query_result'][$i]->center_contact_no);
				array_push($temp, $get_result['query_result'][$i]->center_email_id);
				array_push($temp, $get_result['query_result'][$i]->can_see_timetable_before);
				array_push($temp, $get_result['query_result'][$i]->can_see_timetable_after);
				
				$viewCourses="";
				$viewCourses.= '<a href="centercourses/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->center_id) , '+/', '-_') , '=') . '" title="Courses Mapping">View Courses Mapping</a>';
				
				array_push($temp, $viewCourses);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CenterAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->center_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("CenterCategoryCoursesList")){
					$actionCol1.= '<a href="centermaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->center_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && course_id='".$_GET['id']."' ";
		$result['details'] = $this->centermastermodel->getdata('tbl_courses',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->centermastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('centermaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->centermastermodel->delrecord("tbl_centers","center_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->centermastermodel->delrecord12("tbl_centers","center_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
