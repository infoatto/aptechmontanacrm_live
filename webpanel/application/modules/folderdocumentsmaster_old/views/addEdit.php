<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Folder Documents</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="javascript:history.go(-1)">Folder Documents</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="folder_document_id" name="folder_document_id" value="<?php if(!empty($details[0]->folder_document_id)){echo $details[0]->folder_document_id;}?>" />
								
								<?php if(!empty($details[0]->folder_document_id)){?>
									<div class="control-group form-group">
										<label class="control-label" for="document_title">Document Title*</label>
										<div class="controls">
											<input type="text" name="document_title" id="document_title" class="required form-control" title="Please enter title." value="<?php if(!empty($details[0]->document_title)){echo $details[0]->document_title;}?>" />
											
											<input type="hidden" name="doc_type" id="doc_type" class="required form-control" title="" value="<?php if(!empty($details[0]->doc_type)){echo $details[0]->doc_type;}?>" />
											
										</div>
									</div>
									
									<div class="control-group form-group">
										<label class="control-label" for="document_folder_id">Document / Video*</label>
										<div class="controls">
										<?php if(!empty($details[0]->doc_type) && $details[0]->doc_type =='video' ){?>
											<textarea name="video_url" id="video_url" class="required form-control" title="Please enter video url." ><?php if(!empty($details[0]->document_field_value)){ echo $details[0]->document_field_value;}?></textarea>
										<?php }else if(!empty($details[0]->doc_type) && $details[0]->doc_type =='doc' ){?>	
											<input type="file" name="doc_file" id="doc_file" class="form-control" accept="application/pdf" />
											<input type="hidden" name="prev_value" id="prev_value" value="<?php if(!empty($details[0]->document_field_value)){echo $details[0]->document_field_value;}?>" />
											
											<a href="<?php echo FRONT_URL; ?>/images/folder_documents/<?php echo $details[0]->document_field_value; ?>" target="_blank" >View File</a>
										<?php }else{?>	
											<input type="file" name="doc_excel_file" id="doc_excel_file" class="form-control" accept=".xlsx,.xls,.doc,.docx,.ppt,.pptx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation" />
											
											<input type="hidden" name="prev_value" id="prev_value" value="<?php if(!empty($details[0]->document_field_value)){echo $details[0]->document_field_value;}?>" />
											
											<a href="<?php echo FRONT_URL; ?>/images/folder_documents/<?php echo $details[0]->document_field_value; ?>" target="_blank" >View File</a>
											
										<?php }?>	
										</div>
									</div>
									
								<?php }else{?>
									<div class="control-group form-group">
										<label class="control-label" for="document_folder_id">Folder*</label>
										<div class="controls">
											<select id="document_folder_id" name="document_folder_id" class="form-control">
												<option value="">Select Folder</option>
												<?php 
													if(isset($document_folders) && !empty($document_folders)){
														foreach($document_folders as $cdrow){
															$sel = ($cdrow->document_folder_id == $details[0]->document_folder_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->document_folder_id;?>" <?php echo $sel; ?>><?php echo $cdrow->folder_name;?></option>
												<?php }}?>
											</select>
										</div>
									</div>
								
									<a class="minia-icon-file-add btn btn-primary tip" style="float: none; margin-right: 0%; margin-top: 3%; margin-bottom: 2%;" onclick="addFiles();" title="" aria-describedby="ui-tooltip-1"> Add Document </a>
									<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
										<table class="display table table-bordered non-bootstrap">
											<thead>
												<tr>
													<th>Document Type</th>
													<th>Document Title</th>
													<th>Video / Document</th>
													<th>Is Downloadable?</th>
													<!--<th>Start Date</th>
													<th>End Date</th>-->
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="tableBody"></tbody>
										</table>					
									</div>
									<div style="clear:both; margin-bottom: 2%;"></div>
								<?php }?>
								
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->folder_document_id)){
	?>
		count = '1';
	<?php }else{?>
		addFiles();
	<?php }?>
	
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});
});

var count = 1;
function addFiles()
{
	var fileCount = count - 1;
	var text = '<tr id="divTD_'+count+'">'+
					'<td>'+
						'<select name="doc_type['+count+']" id="doc_type_'+count+'" class="required form-control" onchange="showDocumentType('+count+')" title="Please select document type." >'+
							'<option value=""> Select Type</option>'+
							'<option value="video"> Video</option>'+
							'<option value="doc"> PDF Document</option>'+
							'<option value="doc_excel"> Doc/Excel/PPT Document</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<input type="text" name="document_title['+count+']" id="document_title_'+count+'" class="required form-control" title="Please enter title." />'+
					'</td>'+
					'<td>'+
						'<textarea name="video_url['+count+']" id="video_url_'+count+'" class="required form-control" title="Please enter video url." style="display:none;" ></textarea>'+
						'<input type="file" name="doc_file['+count+']" id="doc_file_'+count+'" class="required form-control" accept="application/pdf" style="display:none;" />'+
						'<input type="file" name="doc_excel_file['+count+']" id="doc_excel_file_'+count+'" class="required form-control" accept=".xlsx,.xls,.doc,.docx,.ppt,.pptx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation" style="display:none;" />'+
					'</td>'+
					'<td>'+
						'<select name="is_downloadable['+count+']" id="is_downloadable_'+count+'" class="required form-control" title="Please select option.">'+
							'<option value="Yes"> Yes</option>'+
							'<option value="No"> No</option>'+
						'</select>'+
					'</td>'+
					/*'<td>'+
						'<input type="text" name="start_date['+count+']" id="start_date_'+count+'" class="required form-control datepicker" title="Please select start date." />'+
					'</td>'+
					'<td>'+
						'<input type="text" name="end_date['+count+']" id="end_date_'+count+'" class="required form-control datepicker" title="Please select end date." />'+
					'</td>'+*/
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles(\'divTD_'+count+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody").append(text);
	
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});
	
	count++;
}

function removeFiles(divId,record_id)
{
    //alert(record_id);
	//return false;
	var r=confirm("Are you sure you want to delete this record?");
	if (r==true)
   	{
		if(record_id == '0'){
			//alert("in");
			$("#"+divId).remove();
		}
	}
}

function showDocumentType(record_id){
	var doc_type_val = $("#doc_type_"+record_id).val();
	//alert(doc_type_val);
	if(doc_type_val == 'video'){
		$("#video_url_"+record_id).show();
		$("#doc_file_"+record_id).hide();
		$("#doc_excel_file_"+record_id).hide();
	}else if(doc_type_val == 'doc'){
		$("#doc_file_"+record_id).show();
		$("#video_url_"+record_id).hide();
		$("#doc_excel_file_"+record_id).hide();
	}else if(doc_type_val == 'doc_excel'){
		$("#doc_excel_file_"+record_id).show();
		$("#doc_file_"+record_id).hide();
		$("#video_url_"+record_id).hide();
	}
}

var vRules = {
	document_folder_id:{required:true}
};
var vMessages = {
	document_folder_id:{required:"Please select folder."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>folderdocumentsmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						//window.location = "<?php echo base_url();?>centermaster";
						//window.location.reload();
						history.go(-1);
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Folder Documents";

 
</script>					
