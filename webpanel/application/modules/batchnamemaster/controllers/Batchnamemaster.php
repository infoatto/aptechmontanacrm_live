<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Batchnamemaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('batchnamemastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->batchnamemastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('batchnamemaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code*/
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->batchnamemastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('batchnamemaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code end*/
	function fetch()
	{
		$get_result = $this->batchnamemastermodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->batch_name);
				
				/*$actionCol21="";
				if($this->privilegeduser->hasPrivilege("ZoneAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->batch_name_master_id . '\',\'' . $get_result['query_result'][$i]->status. '\');" title="Status">'.$get_result['query_result'][$i]->status .'</a>';
				}*/	
				
				$actionCol = "";
				if($this->privilegeduser->hasPrivilege("ZoneAddEdit")){
					$actionCol.= '<a href="batchnamemaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->batch_name_master_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
				}
				
				//array_push($temp, $actionCol21);
				array_push($temp, $actionCol);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			$condition = "batch_name='".$_POST['batch_name']."' ";
			if(isset($_POST['batch_name_master_id']) && $_POST['batch_name_master_id'] > 0)
			{
				$condition .= " &&  batch_name_master_id != ".$_POST['batch_name_master_id'];
			}
			
			$check_name = $this->batchnamemastermodel->getdata("tbl_batch_name_master",$condition);
			//echo "<pre>";print_r($check_name);exit;
			//echo $_SESSION["webadmin"][0]->user_name;exit;
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			if (!empty($_POST['batch_name_master_id'])) {
				$data_array=array();			
				$batch_name_master_id = $_POST['batch_name_master_id'];
				$data_array['batch_name'] = (!empty($_POST['batch_name'])) ? $_POST['batch_name'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by']=$_SESSION["webadmin"][0]->user_id;
				
				//get batch with selected name
				$result = $this->batchnamemastermodel->updateRecord('tbl_batch_name_master', $data_array,'batch_name_master_id',$batch_name_master_id);
				
				$data = array();
				$data['batch_name'] = (!empty($_POST['batch_name'])) ? $_POST['batch_name'] : '';
				$this->batchnamemastermodel->updateRecord('tbl_batch_master', $data,'batch_name_master_id',$batch_name_master_id);
				
			}else {
				$data_array=array();
				$data_array['batch_name'] = (!empty($_POST['batch_name'])) ? $_POST['batch_name'] : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->batchnamemastermodel->insertData('tbl_batch_name_master', $data_array, '1');
				$result = 1;
				//echo "result: ";$result;exit;
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && batch_name_master_id='".$_GET['id']."' ";
		$result['details'] = $this->batchnamemastermodel->getdata('tbl_batch_name_master',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('batchnamemaster/changeapproval',$result,true);
		echo $view;
	}
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->batchnamemastermodel->delrecord("tbl_batch_name_master","batch_name_master_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->batchnamemastermodel->delrecord12("tbl_batch_name_master","batch_name_master_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
