<?PHP
class Usergroupsmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	
	function getRecords($get){
		$table = "tbl_center_user_groups";
		$table_id = 'i.center_user_group_id';
		$default_sort_column = 'i.center_user_group_id';
		$default_sort_order = 'desc';
		
		$condition = "1=1 ";
		
		$colArray = array('u.first_name','u.last_name','g.group_master_name');
		$searchArray = array('u.first_name','u.last_name','g.group_master_name');
		
		$page = $get['iDisplayStart'];						
		$rows = $get['iDisplayLength'];					
		
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<3;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		$this -> db -> select('i.*,u.first_name, u.last_name,g.group_master_name');
		$this -> db -> from('tbl_center_user_groups as i');
		$this -> db -> join('tbl_admin_users as u', 'i.user_id  = u.user_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this-> db -> where('i.user_id',$get['user_id']);
		$this->db->where("($condition)");
		
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();

		$this -> db -> select('i.*,u.first_name, u.last_name,g.group_master_name');
		$this -> db -> from('tbl_center_user_groups as i');
		$this -> db -> join('tbl_admin_users as u', 'i.user_id  = u.user_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this-> db -> where('i.user_id',$get['user_id']);
		$this->db->where("($condition)");

		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			return array("totalRecords"=>0);
		}
	}
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function delrecord1($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}

	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
}
?>
