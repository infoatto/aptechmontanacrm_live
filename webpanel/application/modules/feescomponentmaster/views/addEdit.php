<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Fees Component Master</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>feescomponentmaster">Fees Component Master</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="fees_component_master_id" name="fees_component_master_id" value="<?php if(!empty($details[0]->fees_component_master_id)){echo $details[0]->fees_component_master_id;}?>" />
						
						<div class="control-group form-group">
							<label class="control-label"><span>Mandatory</span></label>
							<div class="controls">
								<label for="no_mandatory1" style="margin-right:30px;">
									<input type="radio" class=" is_mandatory" name="is_mandatory" value="Yes" <?php echo (!empty($details[0]->is_mandatory) &&  ($details[0]->is_mandatory== 'Yes')) ? "checked" : ""?> id="no_mandatory1" >Yes
								</label>
								<?php 
									if(isset($details[0]->is_mandatory) && !empty($details[0]->is_mandatory)){
								?>
								<label for="no_mandatory2" style="margin-right:30px;">
									<input type="radio" class=" is_mandatory" name="is_mandatory"  id="no_mandatory2"  value="No" <?php echo (!empty($details[0]->is_mandatory) &&  ($details[0]->is_mandatory== 'No')) ? "checked" : ""?>>No
								<label for="is_mandatory">
								<?php }else{
									?>
								<label for="no_mandatory">
									<input type="radio" class=" is_mandatory" name="is_mandatory" id="no_mandatory" value="No" <?php echo (!empty($details[0]->is_mandatory) &&  ($details[0]->is_mandatory== 'No')) ? "checked" : ""?> checked="checked" >No
								</label>
								<?php
									}
								?>
							</div>
						</div>

						<div class="control-group form-group class_div">
							<label class="control-label" for="category_id">Component Category*</label>
							<div class="controls">
								<select id="fees_type" name="fees_type" class="form-control">
									<option value="">Select Component Category</option>
									<?php 
										if(isset($component_category) && !empty($component_category)){
											foreach($component_category as $key=>$val){
												$sel = ($val == $details[0]->fees_type) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $val;?>" <?php echo $sel; ?>><?php echo $val;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						
						<div class="control-group form-group">
							<label class="control-label"><span>Fees Component Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Name" id="fees_component_master_name" name="fees_component_master_name" value="<?php if(!empty($details[0]->fees_component_master_name)){echo $details[0]->fees_component_master_name;}?>"  maxlength="150" >
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Printed On Receipt Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Receipt Nmae" id="receipt_name" name="receipt_name" value="<?php if(!empty($details[0]->receipt_name)){echo $details[0]->receipt_name;}?>"  maxlength="50" >
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label" for="status">Status*</label>
							<div class="controls">
								<select name="status" id="status" class="form-control">
									<option value="Active" <?php if(!empty($details[0]->status) && $details[0]->status == "Active"){?> selected <?php }?>>Active</option>
									<option value="In-active" <?php if(!empty($details[0]->status) && $details[0]->status == "In-active"){?> selected <?php }?>>In-active</option>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label" for="status">Description</label>
							<div class="controls">
								<textarea name="component_description" id="component_description" class="form-control"><?php echo(!empty($details[0]->component_description))?$details[0]->component_description:""; ?></textarea>
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>feescomponentmaster" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

var vRules = {
	fees_component_master_name:{required:true, alphanumericwithspace:true},
	receipt_name:{required:true},
	fees_type:{required:true}
	
};
var vMessages = {
	fees_component_master_name:{required:"Please enter fees component name."},
	receipt_name:{required:"Please enter receipt name."},
	fees_type:{required:"Please select fees type."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>feescomponentmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feescomponentmaster";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Fees Component Master";

 
</script>					
