<?PHP
class Studentchangepasswordmodel extends CI_Model
{
	function updateRecord($table,$data,$condition){
		$this -> db -> where($condition);
		$this -> db -> update($table,$data);
		if ($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function getData($table,$fields,$condition){
		$this -> db -> select($fields);
	    $this -> db -> from($table);
	    $this->db->where($condition);
	    $query = $this -> db -> get();
	    if($query -> num_rows() >= 1){
			return $query->result_array();
	    }else{
			return false;
	    }
	}
	 
	
}
?>