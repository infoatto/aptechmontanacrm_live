<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Studentchangepassword extends CI_Controller {

 	function __construct(){
		parent::__construct();
		$this->load->model('studentchangepasswordmodel','',TRUE);
 	}
 
	function index(){
		if(!empty($_SESSION["webadmin"])){
			//get student list
			$result_data["student_details"]= $this->studentchangepasswordmodel->getData("tbl_student_master","*",array());
			$this->load->view('template/header.php');
			$this->load->view('studentchangepassword/addEdit',$result_data);
			$this->load->view('template/footer.php');
		}else{
			redirect('login', 'refresh');
		}
	}
 
	function submitForm(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			if(!empty($_POST['student_id'])){
				//check old password
				$student_details = $this->studentchangepasswordmodel->getData("tbl_student_master","password",array("password"=>md5($_POST['newpassword']),"student_id"=>$_POST['student_id']));
				if(empty($student_details)){
					$data = array();
					$data['password'] = md5($_POST['newpassword']);
					$condition = array("student_id"=>$_POST['student_id']);
					$result = $this->studentchangepasswordmodel->updateRecord("tbl_student_master",$data,$condition);
					if(!empty($result)){
						echo json_encode(array('success'=>'1','msg'=>'Password Changed Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>'0','msg'=>'Problem in password update.'));
						exit;
					}
				}else{
					echo json_encode(array('success'=>'0','msg'=>'The new password is same as old password.'));
					exit;
				}
			}
		}else{
			return false;
		}
	}
}

?>