<!-- start: Content -->
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1><!--<i class="fa fa-dashboard"></i>--> Change Password</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>studentchangepassword">Change Password</a></li>
        </ul>
      </div>
    </div>    
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
            	<div class="col-sm-8 col-md-4">
               		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                       <div class="control-group form-group">
                            <label for="">Student* </label>
                            <select name="student_id" id="student_id" class="form-control select2">
                                <option value="">Select Student</option>
                                <?php if(!empty($student_details)){ 
                                    foreach($student_details as $key=>$val){   
                                ?>
                                    <option value="<?php echo $val['student_id']; ?>"><?php echo $val['enrollment_no']."-".$val['student_first_name']." ".$val['student_last_name']; ?></option>
                                <?php }
                                } ?>
                            </select>				
                       </div>
                        <div class="control-group form-group">
                            <label class="control-label" for="newpassword">New Password*</label>
                            <div class="controls">
                                <input class="input-xlarge form-control" id="newpassword" name="newpassword" type="password">
                            </div>
                        </div>     
                        <div class="form-actions form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?php echo base_url();?>home"><button class="btn" type="button">Cancel</button></a>
                        </div>
                    
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
         </div>              
    </div>
</div><!-- end: Content -->	
			
<script>
var vRules = {
	student_id:{required:true}, 
	newpassword:{required:true}, 
};
var vMessages = {
	student_id:{required:"Please select student."},
	newpassword:{required:"Please enter New Password."},
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form){
		var act = "<?php echo base_url();?>studentchangepassword/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1"){
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location.reload();
					},2000);
				}else{	
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});
</script>